﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ProfileGlassHome
{
    public partial class Quotation : Form
    {

        private QuotationViewModel viewModel = new QuotationViewModel();
        
        private int tab_count = 1;
        private int selected_tab_count = 0;
        private int tabIndex = 0;
        private bool delete_item_flag = false;

        //public static Quote quote;
        private TabPage tab;
        //private Item item;

        //Note: available in Profile project
        //bool quickAccessFlag = false;

        //Note: the form name will actually be 'Quotation'
        //Note: May need to pass in a DataTable so that the information can then be parsed out into multiple items
        public Quotation(string company, string name, string quoteNum, bool quickAccessFlag)
        {
            InitializeComponent();

            //Manage parameters

            if (company == "")
            {
                
            } else
            {
                txtCompanyName.Text = company;
            }

            if (name == "")
            {
                
            } else
            {
                txtName.Text = name;
            }

            //If quoteNumber is empty create a new 
            if (quoteNum == "")
            {
                txtQuoteNumber.Text = viewModel.retrieveQuoteNumber;
            } else
            {
                txtQuoteNumber.Text = quoteNum;
            }

            //If the generate quote is triggered, then enable these options - note: available in Profile project
            if (quickAccessFlag)
            {
                //Disable button functionality
                //btn_quotation_delete_attach.Enabled = false;
                //btn_quotation_attach.Enabled = false;
                //btn_quotation_save.Enabled = false;
                //btn_quotation_note.Enabled = false;
                btnQuotationConfirm.Enabled = false;
            }

            //Loads
            Load_All_ComboBoxes();

            //Assign handler to RichTextBox Description
            richtxtSlumpCustomDescription.TextChanged += new EventHandler(All_RichTextBox_Changed);
            richtxtPaintColorCode.TextChanged += new EventHandler(All_RichTextBox_Changed);

            //Assign handler to button
            //All_Button_Changed
            btnQuotationDeleteAttach.Click += new EventHandler(All_Button_Changed);
            btnQuotationAttach.Click += new EventHandler(All_Button_Changed);
            btnQuotationSave.Click += new EventHandler(All_Button_Changed);
            btnQuotationNewItem.Click += new EventHandler(All_Button_Changed);
            btnQuotationDeleteItem.Click += new EventHandler(All_Button_Changed);
            btnQuotationClose.Click += new EventHandler(All_Button_Changed);
            btnQuotationNote.Click += new EventHandler(All_Button_Changed);
            btnQuotationConfirm.Click += new EventHandler(All_Button_Changed);
            btnWaterjetAdd.Click += new EventHandler(All_Button_Changed);
            btnWaterjetDelete.Click += new EventHandler(All_Button_Changed);
            btnCustomAdd.Click += new EventHandler(All_Button_Changed);
            btnCustomDelete.Click += new EventHandler(All_Button_Changed);
            btnBrowseTemplate.Click += new EventHandler(All_Button_Changed);

            //Assign handler to combobox
            cmbMaterialThickness.SelectedValueChanged += new EventHandler(All_ComboBox_Changed);
            cmbMaterialType.SelectedValueChanged += new EventHandler(All_ComboBox_Changed);
            cmbSlumpTexture.SelectedValueChanged += new EventHandler(All_ComboBox_Changed);
            cmbPaintType.SelectedValueChanged += new EventHandler(All_ComboBox_Changed);
            cmbPriceFactor.SelectedValueChanged += new EventHandler(All_ComboBox_Changed);
            cmbItemOverrideDollar.SelectedValueChanged += new EventHandler(All_ComboBox_Changed);
            cmbItemOverridePercent.SelectedValueChanged += new EventHandler(All_ComboBox_Changed);
            cmbQuoteOverrideDollar.SelectedValueChanged += new EventHandler(All_ComboBox_Changed);
            cmbQuoteOverridePercent.SelectedValueChanged += new EventHandler(All_ComboBox_Changed);
            cmbGpoCutout.SelectedValueChanged += new EventHandler(All_ComboBox_Changed);
            cmbCutoutLength.SelectedValueChanged += new EventHandler(All_ComboBox_Changed);

            //Assign handler to textbox
            txtCompanyName.TextChanged += new EventHandler(All_TextBox_Changed);
            txtName.TextChanged += new EventHandler(All_TextBox_Changed);
            txtAddress.TextChanged += new EventHandler(All_TextBox_Changed);

            txtDimensionQuantity.TextChanged += new EventHandler(All_TextBox_Changed);
            txtDimensionHeight.TextChanged += new EventHandler(All_TextBox_Changed);
            txtDimensionWidth.TextChanged += new EventHandler(All_TextBox_Changed);
            txtDimensionRadius.TextChanged += new EventHandler(All_TextBox_Changed);
            txtMaterialCustomDescription.TextChanged += new EventHandler(All_TextBox_Changed);
            txtDimensionArea.TextChanged += new EventHandler(All_TextBox_Changed);
            txtDeliveryDistance.TextChanged += new EventHandler(All_TextBox_Changed);
            txtInstallationHrs.TextChanged += new EventHandler(All_TextBox_Changed);
            txtItemOverrideDollar.TextChanged += new EventHandler(All_TextBox_Changed);
            txtItemOverridePercent.TextChanged += new EventHandler(All_TextBox_Changed);
            txtQuoteOverrideDollar.TextChanged += new EventHandler(All_TextBox_Changed);
            txtQuoteOverridePercent.TextChanged += new EventHandler(All_TextBox_Changed);
            txtCustomQuantity.TextChanged += new EventHandler(All_TextBox_Changed);
            //txt_custom_material.TextChanged += new EventHandler(All_TextBox_Changed);
            txtCustomPrice.TextChanged += new EventHandler(All_TextBox_Changed);
            txtCustomHours.TextChanged += new EventHandler(All_TextBox_Changed);

            //Assign handler to checkbox
            chkWaterJet.CheckedChanged += new EventHandler(All_CheckBox_Changed);
            chkWaterJet.Checked = false;
            ManageCheckGroupBox(chkWaterJet, grpWaterJet);

            chkHoleDiameter.CheckedChanged += new EventHandler(All_CheckBox_Changed);
            chkHoleDiameter.Checked = false;

            chkCustomDesign.CheckedChanged += new EventHandler(All_CheckBox_Changed);
            chkCustomDesign.Checked = false;
            ManageCheckGroupBox(chkCustomDesign, grpCustomDesign);

            chkSlump.CheckedChanged += new EventHandler(All_CheckBox_Changed);
            chkSlump.Checked = false;
            ManageCheckGroupBox(chkSlump, grpSlump);

            chkLaminate.CheckedChanged += new EventHandler(All_CheckBox_Changed);
            chkLaminate.Checked = false;
            ManageCheckGroupBox(chkLaminate, grpLaminate);

            chkBend.CheckedChanged += new EventHandler(All_CheckBox_Changed);
            chkBend.Checked = false;
            ManageCheckGroupBox(chkBend, grpBend);

            chkToughen.CheckedChanged += new EventHandler(All_CheckBox_Changed);
            chkToughen.Checked = false;
            ManageCheckGroupBox(chkToughen, grpToughen);

            chkLaser.CheckedChanged += new EventHandler(All_CheckBox_Changed);
            chkLaser.Checked = false;
            ManageCheckGroupBox(chkLaser, grpLaser);

            chkSandblast.CheckedChanged += new EventHandler(All_CheckBox_Changed);
            chkSandblast.Checked = false;
            ManageCheckGroupBox(chkSandblast, grpSandblast);

            chkPolishing.CheckedChanged += new EventHandler(All_CheckBox_Changed);
            chkPolishing.Checked = false;
            ManageCheckGroupBox(chkPolishing, grpPolishing);

            chkPrint.CheckedChanged += new EventHandler(All_CheckBox_Changed);
            chkPrint.Checked = false;
            ManageCheckGroupBox(chkPrint, grpPrint);

            chkPaint.CheckedChanged += new EventHandler(All_CheckBox_Changed);
            chkPaint.Checked = false;
            ManageCheckGroupBox(chkPaint, grpPaint);

            //Assign handler to radiobutton
            rbToughenInsource.CheckedChanged += new EventHandler(All_RadioButton_Changed);
            rbToughenOutsource.CheckedChanged += new EventHandler(All_RadioButton_Changed);

            rbLaminateInsource.CheckedChanged += new EventHandler(All_RadioButton_Changed);
            rbLaminateOutsource.CheckedChanged += new EventHandler(All_RadioButton_Changed);

            rbBendInsource.CheckedChanged += new EventHandler(All_RadioButton_Changed);
            rbBendOutsource.CheckedChanged += new EventHandler(All_RadioButton_Changed);

            rbSandblastInsource.CheckedChanged += new EventHandler(All_RadioButton_Changed);
            rbSandblastOutsource.CheckedChanged += new EventHandler(All_RadioButton_Changed);

            rbPolishInsource.CheckedChanged += new EventHandler(All_RadioButton_Changed);
            rbPolishOutsource.CheckedChanged += new EventHandler(All_RadioButton_Changed);

            rbLaserSimple.CheckedChanged += new EventHandler(All_RadioButton_Changed);
            rbLaserComplex.CheckedChanged += new EventHandler(All_RadioButton_Changed);

            rbPrintSmall.CheckedChanged += new EventHandler(All_RadioButton_Changed);
            rbPrintLarge.CheckedChanged += new EventHandler(All_RadioButton_Changed);

        }



        private void Load_All_ComboBoxes()
        {   
            LoadGpoCutoutComboBox();
            LoadCutoutLengthComboBox();
            LoadPaintComboBox();
            LoadPriceFactorComboBox();
            LoadOverrideComboBox();
            
            LoadMaterialsComboBox();
            LoadSlumpComboBox();
        }

        private void LoadGpoCutoutComboBox()
        {
            cmbGpoCutout.Items.Add("Vertical");
            cmbGpoCutout.Items.Add("Horizontal");
            cmbGpoCutout.SelectedItem = null;
        }
        private void LoadCutoutLengthComboBox()
        {
            cmbCutoutLength.Items.Add("< 500mm");
            cmbCutoutLength.Items.Add("501 - 1000mm");
            cmbCutoutLength.Items.Add("1001 - 2000mm");
            cmbCutoutLength.Items.Add("2001 - 3000mm");
            cmbCutoutLength.Items.Add("3001 - 4000mm");
            cmbCutoutLength.SelectedItem = null;
        }
        private void LoadPriceFactorComboBox()
        {
            cmbPriceFactor.Items.Add("1.7");
            cmbPriceFactor.Items.Add("2.7");
            cmbPriceFactor.Items.Add("3.7");
            cmbPriceFactor.Items.Add("4.7");
            cmbPriceFactor.Items.Add("5.7");
            cmbPriceFactor.Items.Add("6.7");
            cmbPriceFactor.Items.Add("7.7");
            cmbPriceFactor.Items.Add("8.7");
            cmbPriceFactor.Items.Add("9.7");

            cmbPriceFactor.SelectedItem = "4.7";
            //quote.getItem(tabIndex).PriceFactor = Convert.ToDouble(cmb_price_factor.SelectedItem);
            viewModel.setItemPriceFactor(tabIndex, Convert.ToDouble(cmbPriceFactor.SelectedItem));
        }
        private void LoadOverrideComboBox()
        {
            cmbItemOverrideDollar.Items.Add("+");
            cmbItemOverrideDollar.Items.Add("-");

            cmbItemOverridePercent.Items.Add("+");
            cmbItemOverridePercent.Items.Add("-");

            cmbQuoteOverrideDollar.Items.Add("+");
            cmbQuoteOverrideDollar.Items.Add("-");

            cmbQuoteOverridePercent.Items.Add("+");
            cmbQuoteOverridePercent.Items.Add("-");

        }

        // Load all available materials into combobox from the database
        private void LoadMaterialsComboBox()
        {
            DataTable temp = viewModel.retrieveMaterialData;
            for (int i = 0; i < temp.Rows.Count; i++)
            {
                string content = temp.Rows[i][0].ToString();
                cmbMaterialType.Items.Add(content);
            }
        }
        private void LoadSlumpComboBox()
        {
            DataTable temp = viewModel.retrieveSlumpData;
            for (int i = 0; i < temp.Rows.Count; i++)
            {
                string content = temp.Rows[i][0].ToString();
                cmbSlumpTexture.Items.Add(content);
            }
        }
        private void LoadThicknessComboBox(string material)
        {
            //Clears all the items in the Thickness combobox
            cmbMaterialThickness.Items.Clear();

            //Clears the combobox text
            cmbMaterialThickness.Text = " ";

            DataTable temp = viewModel.retrieveGlassThicknessData(material);

            for (int i = 0; i < temp.Rows.Count; i++)
            {
                string thickness = temp.Rows[i][0].ToString();
                cmbMaterialThickness.Items.Add(thickness);
            }
        }
        private void LoadPaintComboBox()
        {
            for (int i = 0; i < viewModel.retrievePaintData.Rows.Count; i++)
            {
                string content = viewModel.retrievePaintData.Rows[i][0].ToString();
                cmbPaintType.Items.Add(content);
            }
        }

        //Handles the item calculations and sets the item price tests 
        private void calculateAndUpdatePrices()
        {
            //Calculate the cost price from the item price list
            lblItemCostPrice.Text = String.Format("{0:0.00}", viewModel.getItemCostPrice(tabIndex));
            
            //Calculate the job price (based on cost price with multipler involved)
            lblItemJobPrice.Text = String.Format("{0:0.00}", viewModel.getItemJobPrice(tabIndex));

            //Calculate the customer price (based on job price with the addition of overrides)
            lblItemCustomerPrice.Text = String.Format("{0:0.00}", viewModel.getItemCustomerPrice(tabIndex));

            //MessageBox.Show("Before Quote total: " + quote.getTotalPrice);
            lblQuoteTotal.Text = String.Format("{0:0.00}", viewModel.getQuoteTotalPrice);

            lblGstAmount.Text = String.Format("{0:0.00}", viewModel.getQuoteGstAmount);

            lblQuoteGstTotal.Text = String.Format("{0:0.00}", viewModel.getQuoteTotalAndGstPrice);

            //MessageBox.Show(" |calculate and update price| " + " Cost Price: " + item.GetCostPrice + " Job Price: " + item.GetJobPrice + " Item total: " + quote.getItemCount + " Quote total: " + quote.getTotalPrice);
        }
        

        //Used to disable the datagridview
        private void DataGridViewIsEnabled(DataGridView dgv,bool isEnabled)
        {
            if(!isEnabled)
            {
                dgv.DefaultCellStyle.BackColor = SystemColors.Control;
                dgv.DefaultCellStyle.ForeColor = SystemColors.GrayText;
                dgv.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.Control;
                dgv.ColumnHeadersDefaultCellStyle.ForeColor = SystemColors.GrayText;
                dgv.ReadOnly = true;
                dgv.EnableHeadersVisualStyles = false;
                dgv.CurrentCell = null;
            } else
            {
                dgv.DefaultCellStyle.BackColor = SystemColors.Window;
                dgv.DefaultCellStyle.ForeColor = SystemColors.ControlText;
                dgv.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.Window;
                dgv.ColumnHeadersDefaultCellStyle.ForeColor = SystemColors.ControlText;
                dgv.ReadOnly = false;
                dgv.EnableHeadersVisualStyles = false;
            }
        }

        //Manages which RichTextbox has been changed
        private void All_RichTextBox_Changed(object sender, EventArgs e)
        {
            System.Windows.Forms.RichTextBox richtxt = (sender as System.Windows.Forms.RichTextBox);
            //MessageBox.Show("Textbox name: " + txt.Name + " | Textbox text:" + txt.Text);

            switch (richtxt.Name)
            {
                case "richtxtSlumpCustomDescription":
                    viewModel.setItemSlumpCustomDescrition(tabIndex, richtxtSlumpCustomDescription.Text);
                    break;
                case "richtxtPaintColorCode":
                    viewModel.setItemPaintColorCode(tabIndex, richtxtPaintColorCode.Text);
                    break;
                default:
                    break;
            }
        }

        //This function is called from the Notes form when the user presses the Save button
        public void saveToNotes(string note)
        {
            viewModel.getNotes = note;
            Console.WriteLine("Save to notes: " + note);
        }

        //Manages which button has been changed
        private void All_Button_Changed(object sender, EventArgs e)
        {
            System.Windows.Forms.Button btn = (sender as System.Windows.Forms.Button);
            
            string error = "";
            switch (btn.Name)
            {
                case"btnQuotationNote":
                    Notes note = new Notes(this, viewModel.getQuoteNumber, viewModel.getNotes);
                    note.Show();  
                break;

                case "btnQuotationAttach":
                    OpenFileDialog attach = new OpenFileDialog();
                    attach.Title = "Attach File";

                    //Allow multi-selecting of files
                    attach.Multiselect = true;

                    //Do not restore the last opened directory
                    attach.RestoreDirectory = false;

                    if (attach.ShowDialog() == DialogResult.OK)
                    {
                        for (int i = 0; i < attach.SafeFileNames.Count(); i++)
                        {
                            //MessageBox.Show("File {" + i + "} : " + " File Name: " + attach.SafeFileNames[i] + " File Path: " + attach.FileNames[i]);
                            ltbQuotationListFiles.Items.Add(attach.SafeFileNames[i]);     //Add the safefilename to the listbox

                            viewModel.getFileAttachmentList.Add(attach.SafeFileNames[i], attach.FileNames[i].ToString());   //Add the fileName and filePath to the dictionary list
                        }
                    }
                break;

                case "btnQuotationDeleteAttach":
                    //Check if a selected item has been selected in the listbox
                    if (ltbQuotationListFiles.SelectedItem != null)
                    {
                        bool findFlag = false;
                        string selection = ltbQuotationListFiles.SelectedItem.ToString();
                        //MessageBox.Show("Item selected: " + selection);
                        //Delete the selected item from the listbox
                        foreach (var file in viewModel.getFileAttachmentList)
                        {
                            //MessageBox.Show("Item: " + file);
                            if (file.Key.Contains(selection))
                            {
                                //MessageBox.Show("Delete Selected Item ");
                                findFlag = true;
                            }
                        }
                        if (findFlag)
                        {
                            //Remove file from the dictionary list
                            viewModel.getFileAttachmentList.Remove(selection);
                        
                            //Remove file name from the listbox
                            ltbQuotationListFiles.Items.Remove(selection);
                        }
                    }
                break;
                case "btnBrowseTemplate":
                    OpenFileDialog fileDialog = new OpenFileDialog();
                    fileDialog.RestoreDirectory = true;
                    
                    //Allow the user to pick the Quote template
                    if (fileDialog.ShowDialog() == DialogResult.OK)
                    {
                        Console.WriteLine("file name: " + fileDialog.FileName);
                        lblTemplateFileName.Text = fileDialog.SafeFileName;

                        //Set as target path to template document
                        viewModel.getTemplateFilePath = fileDialog.FileName;
                    }
                    break;

                case "btnQuotationSave":

                    //Get the folder path directory
                    FolderBrowserDialog browser = new FolderBrowserDialog();

                    string folderPath = "";
                    //Check if a template has been chosen
                    if (viewModel.getTemplateFilePath != "")
                    {
                        if (browser.ShowDialog() == DialogResult.OK)
                        {
                            folderPath = browser.SelectedPath;
                            //Console.WriteLine("FolderBrowserDialog: " + folderPath);
                            string company;
                            string name;
                            //Check if the company name is empty, if so then don't add it to the folder name
                            if (!String.IsNullOrEmpty(txtCompanyName.Text))
                            {
                                company = "-" + viewModel.getQuoteCompanyName;
                            } else
                            {
                                company = "";
                            }

                            if(!String.IsNullOrEmpty(txtName.Text))
                            {
                                name = "-" + viewModel.getQuoteFirstLastName;
                            } else
                            {
                                name = "";
                            }

                            string quote = viewModel.getQuoteNumber;

                            //Get quote number, company and first last name, combine with the folder path
                            string quotePath = Path.Combine(folderPath, quote + company + name);

                            //Check if a the Quote directory already exists
                            if (!Directory.Exists(quotePath))
                            {
                                //Console.WriteLine("Create a new Quote folder:" + quotePath);
                                Directory.CreateDirectory(quotePath);
                            }
                            else
                            {
                                // Console.WriteLine("Quote folder already exists:" + quotePath);
                            }

                            //Check if any notes exist
                            if (viewModel.checkNotesExist)
                            {
                                //Save notes
                                viewModel.saveNotes(quotePath);
                            }
                            else
                            {
                                //Console.WriteLine("No notes found");
                            }

                            //Check if any attachments exist
                            if (viewModel.checkAttachmentsExist)
                            {
                                //Save attachments to a new/existing folder location
                                viewModel.saveAttachments(quotePath);
                            }
                            else
                            {
                                //Console.WriteLine("No attachments found");
                            }

                            viewModel.CreateQuote(quotePath);
                        }
                        else
                        {
                            Console.WriteLine("Dialog has been closed");
                        }
                    } else
                    {
                        MessageBox.Show("Make sure that a template has been chosen");
                    }
                    //viewModel.CreateQuote("");


                    /* Code for inserting data into the database 
                    //string company_name = "glass market";
                    //string first_name = "kayla";
                    //string last_name = "Jones";
                    string customer_id = "";
                    //string quotation_num = "Q0001";
                    //string quotation_amount = "0";
                    string quotation_id = "";

                    //Note: if the customer is an existing or new customer, their customer_id should be retrieved from the database
                    //db.Select
                    db.Select("SELECT c.contact_id FROM [contacts] c WHERE c.[company_name] = 'Glass Market' AND c.[first_name] = 'Kayla' AND c.[last_name] = 'Jones'");

                    //Assign the data as a result of the select command to the datatable
                    db.get_SqlDataAdapter.Fill(table);

                    customer_id = Convert.ToString(table.Rows[0][0]);

                    //MessageBox.Show("Customer id for " + first_name + " : " + customer_id);

                    //Acquire customer_id
                    //Acquire quotation_num
                    //Acquire quotation_amount

                    // *****************************************************
                    // 

                    //Insert into the quotation_headers table
                    db.Insert("INSERT INTO quotation_headers (CUSTOMER_ID , QUOTATION_NUM, VERSION, QUOTATION_AMOUNT, QUOTATION_DATE, DESCRIPTION, CREATED_BY, CREATED_DATE, MODIFIED_BY, MODIFIED_DATE) VALUES ()");

                    //Select the quotation_id from the quotation_headers table
                    db.Select("SELECT q.quotation_id FROM [quotation_headers] q WHERE q.[quotation_num] = Q0001");

                    //Insert quote data into quotation_lines table
                    */
                    break;

                case "btnQuotationNewItem":
                    /*
                        * Creates a new item object, assigns a new number to the tab pages, 
                        * adds the current panel to a new tab page, change the 'SelectTab' to the new tab page
                        */

                    //Create a new item
                    viewModel.addNewItem();
                    //Adds a  number to represent the number of items on the tab control
                    tab_count++;
                    tab = new TabPage("Item " + tab_count);

                    //Adds the panel (contains the controls) to a new tab page
                    tab.Controls.Add(pnl_quotation);

                    //Creates a new tab page in the tab control
                    this.tabcontrol_quotation.TabPages.Add(tab);

                    //When a tab is added, select the new tab
                    selected_tab_count++;

                    //selected_tab_count = this.tabcontrol_quotation.SelectedIndex++;
                    //MessageBox.Show("Selected_tab_count: " + selected_tab_count + "Previous tab: " + this.tabcontrol_quotation.SelectedIndex);
                    tabcontrol_quotation.SelectTab(selected_tab_count);
                    break;
                case "btnQuotationDeleteItem":
                    /*
                        Need to remove the tab page and the item associate with that tab page
                    */

                    //If there is one tab page remaining, prevent the user from deleting it
                    if (this.tabcontrol_quotation.TabPages.Count > 1)
                    {
                        //Remove the Item associated with the currently selected tab page
                        viewModel.removeItem(this.tabcontrol_quotation.SelectedIndex);
                        
                        //Clears tab pages, resets variables used to track tab pages
                        delete_item_flag = true;
                        tab_count = 0;
                        selected_tab_count = 0;
                        this.tabcontrol_quotation.TabPages.Clear();

                        //Re-arranges items in the item list and reset the tab pages
                        if (viewModel.rearrangeItems)
                        {
                            //Add number of tabs according to the number of items
                            for (int i = 0; i < viewModel.getItemCount; i++)
                            {
                                //Adds a  number to represent the number of items on the tab control
                                tab_count++;
                                tab = new TabPage("Item " + tab_count);

                                //Creates a new tab page in the tab control
                                this.tabcontrol_quotation.TabPages.Add(tab);

                                //Increment selected_tab_count
                                selected_tab_count++;
                                //selected_tab_count = this.tabcontrol_quotation.SelectedIndex++;
                            }
                            //Decrement the select_tab_count to not affect the 'add item' button click 
                            selected_tab_count--;
                        }

                        //Adds the panel to the selected tab
                        tabcontrol_quotation.SelectedTab.Controls.Add(pnl_quotation);

                        //Set tab index to a new selected index
                        tabIndex = tabcontrol_quotation.SelectedIndex;

                        //Reintialise the flag
                        delete_item_flag = false;

                        //Retrieves the values for the item based on which tab item has been selected 
                        ManageItemControls();
                    }
                    break;

                case "btnQuotationClose":
                    this.Close();
                    break;

                case "btnWaterjetAdd":
                    //MessageBox.Show("getWaterjetCutSelection: " + item.getWaterjetCutSelection);
                    if ( viewModel.getItemWaterJetSelection(tabIndex) == "" )
                    {
                        MessageBox.Show(" Option has not been chosen, Choose one of the following options ");
                        return;
                    }
 
                    //Add to the item list so that the price can be calculated
                    error = viewModel.addItemWaterJetCut(tabIndex);
                    if (error == "")
                    {
                        //Add to the listbox
                        lstWaterjetCutList.Items.Add(viewModel.getItemWaterJetSelection(tabIndex));

                        calculateAndUpdatePrices();
                    } else
                    {
                        chkHoleDiameter.Checked = false;
                        cmbCutoutLength.SelectedItem = null;
                        cmbGpoCutout.SelectedItem = null;
                        MessageBox.Show("Error: " + error);
                    }

                    break;

                case "btnWaterjetDelete":
                    //Check if a selected item has been selected in the listbox
                    if (lstWaterjetCutList.SelectedItem != null)
                    {
                        string selection = lstWaterjetCutList.SelectedItem.ToString();
                        //MessageBox.Show("Item selected to delete: " + selection);
                        lstWaterjetCutList.Items.Remove(selection);

                        //Delete from the item list
                        error = viewModel.deleteItemWaterJetCut(tabIndex);
                        if (error == "")
                        {
                            calculateAndUpdatePrices();
                        } else
                        {
                            MessageBox.Show("Error: " + error);
                        }
                    } else
                    {
                        MessageBox.Show("Pick a cut from the list of cuts to delete");
                    }
                    break;
                case "btnCustomAdd":
                    try
                    {
                        int quantity = 0;
                        string material = ""; 
                        double price = 0.0;
                        double hours = 0;

                        //Check if the user is adding an empty row
                        if (!(String.IsNullOrEmpty(txtCustomQuantity.Text)) && !(String.IsNullOrEmpty(txtCustomMaterial.Text)) && !(String.IsNullOrEmpty(txtCustomPrice.Text)) && !(String.IsNullOrEmpty(txtCustomHours.Text)) )
                        {
                            ListViewItem newItem;
                            //Get text from the txt boxes and assign to the subitems
                            newItem = new ListViewItem(txtCustomQuantity.Text, 0);
                            newItem.SubItems.Add(txtCustomMaterial.Text);
                            newItem.SubItems.Add(txtCustomPrice.Text);
                            newItem.SubItems.Add(txtCustomHours.Text);

                            //Add items to the listView
                            lstCustomDesign.Items.Add(newItem);

                            material = txtCustomMaterial.Text;

                            //Check and assign values where blank
                            if (txtCustomQuantity.Text != "")
                            {
                                quantity = Convert.ToInt16(txtCustomQuantity.Text);
                            }
                            if (txtCustomPrice.Text != "")
                            {
                                price = Convert.ToDouble(txtCustomPrice.Text);
                            }
                            if (txtCustomHours.Text != "")
                            {
                                hours = Convert.ToDouble(txtCustomHours.Text);
                            }
                            //Add to the item dictionary list of custom design items
                            viewModel.addItemCustomDesignItem(tabIndex, quantity, material, price, hours);
                            //MessageBox.Show("Add getCustomDesignPrice: " + item.getCustomDesignPrice);
                            calculateAndUpdatePrices();

                            //Clear the text
                            txtCustomQuantity.Text = "";
                            txtCustomMaterial.Text = "";
                            txtCustomPrice.Text = "";
                            txtCustomHours.Text = "";
                        }
                        else
                        {
                            MessageBox.Show(" Row is empty, Add a Name name to add a row ");
                            txtCustomQuantity.Text = "";
                            txtCustomMaterial.Text = "";
                            txtCustomPrice.Text = "";
                            txtCustomHours.Text = "";
                        }
                    } catch
                    {
                        MessageBox.Show(" Error has occurred: Check the recent item added");
                        txtCustomQuantity.Text = "";
                        txtCustomMaterial.Text = "";
                        txtCustomPrice.Text = "";
                        txtCustomHours.Text = "";
                    }
                    break;
                case "btnCustomDelete":
                    try
                    {
                        if (lstCustomDesign.FocusedItem != null)
                        {
                            if (lstCustomDesign.FocusedItem.Selected)
                            {
                                //MessageBox.Show("Selected Item to delete: " + " Index: " + lst_custom_design.FocusedItem.Index + " Item: " + lst_custom_design.FocusedItem.SubItems[1].Text);
                                Console.WriteLine("btn_custom_delete index: " + lstCustomDesign.FocusedItem.Index);
                                //Delete item from dictionary list of custom design items
                                viewModel.deleteItemCustomDesignItem(tabIndex, lstCustomDesign.FocusedItem.Index);
                                //Remove item from the listView
                                lstCustomDesign.Items.Remove(lstCustomDesign.SelectedItems[0]);
                                //MessageBox.Show("Remove getCustomDesignPrice: " + item.getCustomDesignPrice);
                                calculateAndUpdatePrices();
                            }
                            else
                            {
                                MessageBox.Show("Item has not been selected" + lstCustomDesign.FocusedItem.Selected);
                            }
                        }
                    } catch
                    {
                        MessageBox.Show(" Unknown error ");
                    }
                    break;
            }
        } 

        //Manages which Combobox has been changed
        private void All_ComboBox_Changed(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.ComboBox cmb = (sender as System.Windows.Forms.ComboBox);

                switch (cmb.Name)
                {
                    case "cmbMaterialType":
                        try {
                            if (cmb.SelectedItem != null)
                            {
                                //MessageBox.Show("cmb_material_type: " + cmb.SelectedItem.ToString());

                                //If the previous value is not the same as the selected value, load data to the Thickness combobox
                                if (viewModel.getItemMaterialName(tabIndex) != cmb.SelectedItem.ToString())
                                {
                                    //Stores the new selected Name
                                    viewModel.setItemMaterialName(tabIndex, cmb.SelectedItem.ToString());

                                    LoadThicknessComboBox(cmb.SelectedItem.ToString());

                                    if (!String.IsNullOrEmpty(txtDimensionArea.Text))
                                    {

                                        //Remove the price
                                        viewModel.clearItemGlassPrice(tabIndex, 0.0);

                                        calculateAndUpdatePrices();
                                    }
                                }
                            }
                        } catch (Exception ab)
                        {
                            MessageBox.Show("cmb_material_type - " + ab.ToString());
                        }
                    break;

                    case "cmbMaterialThickness":
                        try
                        {
                            if (cmb.SelectedItem != null)
                            {
                                //MessageBox.Show("Name: " + cmb_material_type.Text + "Thickness: " + cmb_material_thickness.Text);
                                viewModel.setItemMaterialThickness(tabIndex, cmb.SelectedItem.ToString());
                                //If the prices have already been calculated, then re-calculate them with the new price factor
                                if (!String.IsNullOrEmpty(txtDimensionArea.Text))
                                {
                                    viewModel.setItemGlassPrice(tabIndex);
                                    calculateAndUpdatePrices();
                                }
                            }
                        } catch (Exception ab)
                        {
                            MessageBox.Show("cmb_material_thickness - " + ab.ToString());
                        }
                        break;

                    case "cmbSlumpTexture":
                        if (cmb.SelectedItem != null)
                        {
                            viewModel.setItemSlumpTexture(tabIndex, cmb.SelectedItem.ToString());
                        }
                        break;

                    case "cmbPaintType":
                        try
                        {
                            if (cmb.SelectedItem != null)
                            {
                                //If the price list already contains the price then remove it
                                if (!String.IsNullOrEmpty(viewModel.findItemName(tabIndex, viewModel.getItemPaintType(tabIndex) )))
                                {
                                    //MessageBox.Show(" |Remove Paint type: |" + item.Type);
                                    //Remove the price and clear the table
                                    viewModel.removeItemPrice(tabIndex, viewModel.getItemPaintType(tabIndex) );
                                }

                                viewModel.setItemPaintType(tabIndex, cmb.SelectedItem.ToString() );
                                //MessageBox.Show(" |Paint: |" + paint_price_table.Rows[0][0] + Environment.NewLine + " |Paint type: | " + item.Type);

                                Double price = viewModel.retrievePaintPrice(viewModel.getItemPaintType(tabIndex) );
                                
                                //Check if the table is not empty
                                if (price != 0.0)
                                {
                                    //MessageBox.Show(" Price ($): " + val + Environment.NewLine);
                                    viewModel.addItemPrice(tabIndex, viewModel.getItemPaintType(tabIndex), price);

                                    calculateAndUpdatePrices();
                                }
                            }
                        } catch (Exception ab)
                        {
                            MessageBox.Show("cmb_paint_type - " + ab.ToString());
                        }
                        break;

                    case "cmbPriceFactor":
                        try
                        {
                            if (cmb.SelectedItem != null)
                            {
                                viewModel.setItemPriceFactor(tabIndex, Convert.ToDouble(cmb.SelectedItem.ToString()));
                                //If the prices have already been calculated, then re-calculate them with the new price factor
                                if (!String.IsNullOrEmpty(txtDimensionArea.Text))
                                {
                                    viewModel.setItemGlassPrice(tabIndex);
                                    calculateAndUpdatePrices();
                                }
                            }
                        } catch (Exception ab)
                        {
                            MessageBox.Show("cmb_price_factor - " + ab.ToString());
                        }
                        break;

                    case "cmbQuoteOverrideDollar":
                        try
                        {
                            //Get the current sign
                            viewModel.setQuoteOverrideDollarSign(cmb.SelectedItem.ToString());
                            //If a value is present in the txt, update the prices
                            if (!String.IsNullOrEmpty(txtQuoteOverrideDollar.Text))
                            {
                                //Update price
                                calculateAndUpdatePrices();
                            }
                        } catch (Exception ab)
                        {
                            MessageBox.Show("cmb_quote_override_dollar - " + ab.ToString());
                        }
                        break;

                    case "cmbQuoteOverridePercent":
                        try
                        {
                            viewModel.setQuoteOverridePercentSign(cmb.SelectedItem.ToString());
                            //If a value is present in the txt, update the prices
                            if (!String.IsNullOrEmpty(txtQuoteOverridePercent.Text))
                            {
                                //Update price
                                calculateAndUpdatePrices();
                            }
                        } catch (Exception ab)
                        {
                            MessageBox.Show("cmb_quote_override_percent - " + ab.ToString());
                        }
                        break;

                    case "cmbItemOverrideDollar":
                        try
                        {
                            //MessageBox.Show("OverrideDollarSign: " + item.OverrideDollarSign);
                            viewModel.setItemOverrideDollarSign(tabIndex, cmb.SelectedItem.ToString());
                            //If a value is present in the txt, update the prices
                            if (!String.IsNullOrEmpty(txtItemOverrideDollar.Text))
                            {
                                //Update price
                                calculateAndUpdatePrices();
                            }
                        } catch (Exception ab)
                        {
                            MessageBox.Show("cmb_item_override_dollar - " + ab.ToString());
                        }
                        break;

                    case "cmbItemOverridePercent":
                        try
                        {
                            viewModel.setItemOverridePercentSign(tabIndex, cmb.SelectedItem.ToString());
                            if (!String.IsNullOrEmpty(txtItemOverridePercent.Text))
                            {
                                //Update price
                                calculateAndUpdatePrices();
                            }
                        } catch (Exception ab)
                        {
                            MessageBox.Show("cmb_item_override_percent - " + ab.ToString());
                        }
                        break;

                    case "cmbCutoutLength":
                        if (cmb.SelectedItem != null)
                        {
                            //Remove other cut options
                            chkHoleDiameter.Checked = false;
                            cmbGpoCutout.SelectedItem = null;
                            //MessageBox.Show("cmb_cutout_length - Remove " + chkHoleDiameter.Checked + " and " + cmb_gpo_cutout.SelectedItem);
                            viewModel.setItemWaterJetCutSelection(tabIndex, "Cutout Length " + cmbCutoutLength.SelectedItem.ToString());
                        }
                        break;

                    case "cmbGpoCutout":
                        if (cmb.SelectedItem != null)
                        {
                            //Remove other cut options
                            chkHoleDiameter.Checked = false;
                            cmbCutoutLength.SelectedItem = null;
                            //MessageBox.Show("cmb_gpo_cutout - Remove " + chkHoleDiameter.Checked + " and " + cmb_cutout_length.SelectedItem);
                            viewModel.setItemWaterJetCutSelection(tabIndex, "GPO Cutout " + cmbGpoCutout.SelectedItem.ToString());
                        }
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ab)
            {
                MessageBox.Show(ab.ToString());
            }
        }

        //Manages which Textbox has been changed
        private void All_TextBox_Changed(object sender, EventArgs e)
        {
            System.Windows.Forms.TextBox txt = (sender as System.Windows.Forms.TextBox);


            switch (txt.Name)
            {
                case "txtQuoteNumber":
                    viewModel.getQuoteNumber = txtQuoteNumber.Text;
                    break;
                case "txtCompanyName":
                        viewModel.setQuoteCompanyName(txtCompanyName.Text);
                    break;
                case "txtName":
                        viewModel.setQuoteFirstLastName(txtName.Text);
                    break;
                case "txtAddress":
                        viewModel.setQuoteAddress(txtAddress.Text);
                    break;
                case "txtMaterialCustomDescription":
                    try
                    { 
                        viewModel.setItemMaterialDescription(tabIndex, txtMaterialCustomDescription.Text);
                    }
                    catch (Exception error)
                    {
                        //Console.WriteLine(error.Message);
                        MessageBox.Show("Error: Contains an unknown character - txt_material_custom_description: " + error);
                        txtMaterialCustomDescription.Text = "";
                    }
                    break;
                case "txtDimensionQuantity":
                    try
                    {
                        viewModel.setItemQuantity(tabIndex, txtDimensionQuantity.Text);
                        calculateAndUpdatePrices();
                    }
                    catch (Exception error)
                    {
                        //Console.WriteLine(error.Message);
                        MessageBox.Show("Error: Contains an unknown character - txt_dimension_quantity: " + error);
                        txtDimensionQuantity.Text = "";
                    }
                    break;
                case "txtDimensionHeight":
                    //MessageBox.Show("Focused: " + txt_dimension_height.Focused + "  txt_dimension_height    Height: " + item.Height + " Width: " + item.Width + " Radius: " + item.Radius);
                    try
                    {
                        if (viewModel.getItemDimensionHeight(tabIndex) != txtDimensionHeight.Text) //Check to see if the text value has changed &&  !String.IsNullOrEmpty(txt_quotation_height.Text) && String.IsNullOrEmpty(txt_quotation_radius.Text))
                        {

                            //Set the dimension height
                            viewModel.setItemDimensionHeight(tabIndex, txtDimensionHeight.Text);

                            //Check to see if the text input value is empty or not 
                            if (!String.IsNullOrEmpty(txtDimensionHeight.Text))
                            {
                                double resultTest = Calculations.Area(txtDimensionHeight.Text, txtDimensionWidth.Text, "");
                                string resultPerimeter = Convert.ToString(Calculations.Perimeter(txtDimensionHeight.Text, txtDimensionWidth.Text, ""));

                                //MessageBox.Show("result: " + resultTest);
                                if (resultTest < 0.3)
                                {
                                    if (resultTest == 0)
                                    {
                                        viewModel.setItemDimensionArea(tabIndex, "0.0");
                                        txtDimensionArea.Text = "0.0";
                                        lblDimensionPerimeter.Text = "";
                                        viewModel.setItemDimensionPerimeter(tabIndex, resultPerimeter);
                                    }
                                    else
                                    {
                                        //MessageBox.Show("Set 0.2 as fixed Area");
                                        viewModel.setItemDimensionArea(tabIndex, "0.3");
                                        txtDimensionArea.Text = "0.3";
                                        txtAreaOriginal.Text = Convert.ToString(resultTest);
                                        txtAreaOriginal.Visible = true;
                                        lblDimensionPerimeter.Text = resultPerimeter;
                                        viewModel.setItemDimensionPerimeter(tabIndex, resultPerimeter);
                                    }
                                }
                                else
                                {
                                    viewModel.setItemDimensionArea(tabIndex, Convert.ToString(resultTest));
                                    txtAreaOriginal.Text = Convert.ToString(resultTest);
                                    txtAreaOriginal.Visible = false;
                                    txtDimensionArea.Text = Convert.ToString(resultTest);
                                    lblDimensionPerimeter.Text = resultPerimeter;
                                    viewModel.setItemDimensionPerimeter(tabIndex, resultPerimeter);
                                }
                            }
                            else
                            {
                                //MessageBox.Show("Height is empty");
                                txtAreaOriginal.Visible = false;
                                txtAreaOriginal.Text = "";
                                viewModel.setItemDimensionArea(tabIndex, "0.0");

                                txtDimensionArea.Text = "0.0";
                                lblDimensionPerimeter.Text = "";
                                viewModel.setItemDimensionPerimeter(tabIndex, "");
                            }
                        }

                        //If a Height has been entered, disable the Radius texxtbox
                        if (txtDimensionHeight.Text != "")
                        {
                            txtDimensionRadius.Enabled = false;
                            //MessageBox.Show("Disable Radius text");
                        }
                        else
                        {
                            if (txtDimensionWidth.Text == "")
                            {
                                txtDimensionRadius.Enabled = true;
                            }
                        }
                    }
                    catch (Exception error)
                    {
                        MessageBox.Show("Error: Contains an unknown character - txt_dimension_height: " + error);
                        txtDimensionHeight.Text = "";
                    }
                    break;
                case "txtDimensionWidth":
                    //MessageBox.Show("txt_dimension_width    Height: " + txt_dimension_height.Text + " Width: " + txt_dimension_width.Text + " Radius: " + txt_dimension_radius.Text);
                    try
                    {
                        if (viewModel.getItemDimensionWidth(tabIndex) != txtDimensionWidth.Text)// && !String.IsNullOrEmpty(txt_quotation_height.Text)||(String.IsNullOrEmpty(txt_quotation_width.Text)) && String.IsNullOrEmpty(txt_quotation_radius.Text)))
                        {
                            //MessageBox.Show("Width text has changed");
                            viewModel.setItemDimensionWidth(tabIndex, txtDimensionWidth.Text);

                            //Check to see if the text input value is empty or not 
                            if (!String.IsNullOrEmpty(txtDimensionWidth.Text))
                            {
                                double resultTest = Calculations.Area(txtDimensionHeight.Text, txtDimensionWidth.Text, "");
                                string resultPerimeter = Convert.ToString(Calculations.Perimeter(txtDimensionHeight.Text, txtDimensionWidth.Text, ""));
                                //MessageBox.Show("result: " + resultTest);
                                if (resultTest < 0.3)
                                {
                                    if (resultTest == 0)
                                    {
                                        viewModel.setItemDimensionArea(tabIndex, "0.0");
                                        txtDimensionArea.Text = "0.0";
                                        lblDimensionPerimeter.Text = "";
                                        viewModel.setItemDimensionPerimeter(tabIndex, resultPerimeter);
                                    }
                                    else
                                    {
                                        //MessageBox.Show("Set 0.2 as fixed Area");
                                        viewModel.setItemDimensionArea(tabIndex, "0.3");
                                        txtDimensionArea.Text = "0.3";
                                        txtAreaOriginal.Text = Convert.ToString(resultTest);
                                        txtAreaOriginal.Visible = true;
                                        lblDimensionPerimeter.Text = resultPerimeter;
                                        viewModel.setItemDimensionPerimeter(tabIndex, resultPerimeter);
                                    }
                                }
                                else
                                {
                                    viewModel.setItemDimensionArea(tabIndex, Convert.ToString(resultTest));
                                    txtAreaOriginal.Text = Convert.ToString(resultTest);
                                    txtAreaOriginal.Visible = false;
                                    txtDimensionArea.Text = Convert.ToString(resultTest);
                                    lblDimensionPerimeter.Text = resultPerimeter;
                                    viewModel.setItemDimensionPerimeter(tabIndex, resultPerimeter);
                                }
                            }
                            else
                            {
                                //MessageBox.Show("Height is empty");
                                txtAreaOriginal.Visible = false;
                                txtAreaOriginal.Text = "";
                                viewModel.setItemDimensionArea(tabIndex, "0.0");

                                txtDimensionArea.Text = "0.0";
                                lblDimensionPerimeter.Text = "";
                                viewModel.setItemDimensionPerimeter(tabIndex, "");
                            }
                        }
                        //If a Width has been entered, disable the Radius texxtbox
                        if (txtDimensionWidth.Text != "")
                        {
                            txtDimensionRadius.Enabled = false;
                            //MessageBox.Show("Disable Radius text");
                        }
                        else
                        {
                            if (txtDimensionHeight.Text == "")
                            {
                                txtDimensionRadius.Enabled = true;
                            }
                        }
                    }
                    catch (Exception error)
                    {
                        MessageBox.Show("Error: Contains an unknown character - txt_dimension_width: " + error);
                        txtDimensionWidth.Text = "";
                    }
                    break;
                case "txtDimensionRadius":
                    //MessageBox.Show("txt_dimension_radius    Height: " + item.Height + " Width: " + item.Width + " Radius: " + item.Radius);
                    try
                    {
                        if (viewModel.getItemDimensionRadius(tabIndex) != txtDimensionRadius.Text)// && !String.IsNullOrEmpty(txt_quotation_height.Text)||(String.IsNullOrEmpty(txt_quotation_width.Text)) && String.IsNullOrEmpty(txt_quotation_radius.Text)))
                        {
                            //MessageBox.Show("Radius text has changed");
                            viewModel.setItemDimensionRadius(tabIndex, txtDimensionRadius.Text);
                            //Check to see if the text input value is empty or not 
                            if (!String.IsNullOrEmpty(txtDimensionRadius.Text))
                            {
                                //string result_area = Convert.ToString(item.getCalculations.Area("", "", remove_mm(item.Radius)));
                                string resultPerimeter = Convert.ToString(Calculations.Perimeter("", "", txtDimensionRadius.Text));
                                double resultTest = (Calculations.Area("", "", txtDimensionRadius.Text));
                                //MessageBox.Show("result: " + resultTest);
                                if (resultTest < 0.3)
                                {
                                    if (resultTest == 0)
                                    {
                                        viewModel.setItemDimensionArea(tabIndex, "0.0");
                                        txtDimensionArea.Text = "0.0";
                                        lblDimensionPerimeter.Text = "";
                                        viewModel.setItemDimensionPerimeter(tabIndex, resultPerimeter);
                                    }
                                    else
                                    {
                                        //MessageBox.Show("Set 0.2 as fixed Area");
                                        viewModel.setItemDimensionArea(tabIndex, "0.3");
                                        txtDimensionArea.Text = "0.3";
                                        txtAreaOriginal.Text = Convert.ToString(resultTest);
                                        txtAreaOriginal.Visible = true;
                                        lblDimensionPerimeter.Text = resultPerimeter;
                                        viewModel.setItemDimensionPerimeter(tabIndex, resultPerimeter);
                                    }
                                }
                                else
                                {
                                    viewModel.setItemDimensionArea(tabIndex, Convert.ToString(resultTest));
                                    txtAreaOriginal.Text = Convert.ToString(resultTest);
                                    txtAreaOriginal.Visible = false;
                                    txtDimensionArea.Text = Convert.ToString(resultTest);
                                    lblDimensionPerimeter.Text = resultPerimeter;
                                    viewModel.setItemDimensionPerimeter(tabIndex, resultPerimeter);
                                }
                            }
                            else
                            {
                                //MessageBox.Show("Height is empty");
                                txtAreaOriginal.Visible = false;
                                txtAreaOriginal.Text = "";
                                viewModel.setItemDimensionArea(tabIndex, "0.0");

                                txtDimensionArea.Text = "0.0";
                                lblDimensionPerimeter.Text = "";
                                viewModel.setItemDimensionPerimeter(tabIndex, "");
                            }
                        }
                        //If a Width has been entered, disable the Radius texxtbox
                        if (txtDimensionRadius.Text != "")
                        {
                            txtDimensionHeight.Enabled = false;
                            txtDimensionWidth.Enabled = false;
                            //MessageBox.Show("Disable Radius text");
                        }
                        else
                        {
                            txtDimensionHeight.Enabled = true;
                            txtDimensionWidth.Enabled = true;
                            //MessageBox.Show("Enable Radius text");
                        }
                    }
                    catch (Exception error)
                    {
                        //Console.WriteLine(error.Message);
                        MessageBox.Show("Error: Contains an unknown character - txt_dimension_radius: " + error);
                        txtDimensionRadius.Text = "";
                    }
                    break;              
                case "txtDimensionArea":
                    //Check if the materials, Thickness and Area is available
                    try
                    {
                        if (!String.IsNullOrEmpty(viewModel.getItemMaterialName(tabIndex)) && !String.IsNullOrEmpty(viewModel.getItemMaterialThickness(tabIndex)) && (viewModel.getItemDimensionArea(tabIndex) != "0.0"))
                        {
                            viewModel.setItemGlassPrice(tabIndex);
                        } 
                        calculateAndUpdatePrices();
                    }
                    catch (Exception error)
                    {
                        //Console.WriteLine(error.Message);
                        MessageBox.Show("Error: Contains an unknown character - txt_dimension_area: " + error);
                    }
                    break;
                
                case "txtItemOverrideDollar":
                    try
                    {
                        if ((txtItemOverrideDollar.Text != "") && (txtItemOverrideDollar.Text != "0") && (txtItemOverrideDollar.Text != "0.0") && (txtItemOverrideDollar.Text != "0."))
                        {
                            viewModel.setItemOverrideDollar(tabIndex, Convert.ToDouble(txtItemOverrideDollar.Text));
                        }
                        else
                        {
                            viewModel.setItemOverrideDollar(tabIndex, 0.0);
                            viewModel.setItemOverrideDollarSign(tabIndex, "");
                            cmbItemOverrideDollar.Text = "";
                        }
                        calculateAndUpdatePrices();
                    }
                    catch (Exception error)
                    {
                        //Console.WriteLine(error.Message);
                        MessageBox.Show("Error: Contains an unknown character - txt_item_override_dollar: " + error);
                        txtItemOverrideDollar.Text = "";
                    }
                    break;
                case "txtItemOverridePercent":
                    try
                    {
                        if ((txtItemOverridePercent.Text != "") && (txtItemOverrideDollar.Text != "0") && (txtItemOverrideDollar.Text != "0.0") && (txtItemOverrideDollar.Text != "0."))
                        {
                            viewModel.setItemOverridePercent(tabIndex, Convert.ToDouble(txtItemOverridePercent.Text));
                        }
                        else
                        {
                            viewModel.setItemOverridePercent(tabIndex, 0.0);
                            viewModel.setItemOverridePercentSign(tabIndex, "");
                            cmbItemOverridePercent.Text = "";
                        }

                        calculateAndUpdatePrices();
                    }
                    catch (Exception error)
                    {
                        //Console.WriteLine(error.Message);
                        MessageBox.Show("Error: Contains an unknown character - txt_item_override_percent: " + error);
                        txtItemOverridePercent.Text = "";
                    }
                    break;
                case "txtQuoteOverrideDollar":
                    try
                    {
                        if ((txtQuoteOverrideDollar.Text != "") && (txtItemOverrideDollar.Text != "0") && (txtItemOverrideDollar.Text != "0.0") && (txtItemOverrideDollar.Text != "0."))
                        {
                            viewModel.setQuoteOverrideDollar(Convert.ToDouble(txtQuoteOverrideDollar.Text));
                        }
                        else
                        {
                            viewModel.setQuoteOverrideDollar(0.0);
                            viewModel.setQuoteOverrideDollarSign("");
                            cmbQuoteOverrideDollar.Text = "";
                        }

                        calculateAndUpdatePrices();
                    }
                    catch (Exception error)
                    {
                        //Console.WriteLine(error.Message);
                        MessageBox.Show("Error: Contains an unknown character - txt_quote_override_dollar: " + error);
                        txtQuoteOverrideDollar.Text = "";
                    }
                    break;
                case "txtQuoteOverridePercent":
                    try
                    {
                        if ((txtQuoteOverridePercent.Text != "") && (txtItemOverrideDollar.Text != "0") && (txtItemOverrideDollar.Text != "0.0") && (txtItemOverrideDollar.Text != "0."))
                        {
                            viewModel.setQuoteOverridePercent(Convert.ToDouble(txtQuoteOverridePercent.Text));
                        }
                        else
                        {
                            viewModel.setQuoteOverridePercent(0.0);
                            viewModel.setQuoteOverridePercentSign("");
                            cmbQuoteOverridePercent.Text = "";
                        }

                        calculateAndUpdatePrices();
                    }
                    catch (Exception error)
                    {
                        //Console.WriteLine(error.Message);
                        MessageBox.Show("Error: Contains an unknown character - txt_quote_override_percent: " + error);
                        txtQuoteOverridePercent.Text = "";
                    }
                    break;
                case "txtInstallationHrs":
                    try
                    {
                        viewModel.setQuoteInstallationHours(txtInstallationHrs.Text);
                        lblInstallationCost.Text = viewModel.getQuoteInstallationCost;
                        //Update the prices
                        calculateAndUpdatePrices();
                    }
                    catch (Exception error)
                    {
                        //Console.WriteLine(error.Message);
                        MessageBox.Show("Error: Contains an unknown character - txtInstallationHrs: " + error);
                        txtInstallationHrs.Text = "";
                    }
                    break;
                case "txtDeliveryDistance":
                    try
                    {
                        viewModel.setQuoteDeliveryCost(txtDeliveryDistance.Text);
                        lblQuoteDelivery.Text = viewModel.getQuoteDeliveryCost;
                        //Update the prices
                        calculateAndUpdatePrices();
                    }
                    catch (Exception error)
                    {
                        //Console.WriteLine(error.Message);
                        MessageBox.Show("Error: Contains an unknown character - txtDeliveryDistance: " + error);
                        txtDeliveryDistance.Text = "";
                    }
                    break;
                case "txtCustomQuantity":
                    try
                    {
                        if (!String.IsNullOrEmpty(txtCustomQuantity.Text))
                        {
                            bool success = Int16.TryParse(txtCustomQuantity.Text, out short result);
                            if (!success)
                            {
                                MessageBox.Show("Invalid text input");
                                txtCustomQuantity.Text = "";
                            }
                        }
                    }
                    catch (Exception error)
                    {
                        //Console.WriteLine(error.Message);
                        MessageBox.Show("Error: Contains an unknown character - txtCustomQuantity: " + error);
                        txtCustomQuantity.Text = "";
                    }
                    break;
                case "txtCustomPrice":
                    try
                    {
                        if (!String.IsNullOrEmpty(txtCustomPrice.Text))
                        {
                            bool success = Double.TryParse(txtCustomPrice.Text, out double result);
                            if (!success)
                            {
                                MessageBox.Show("Invalid text input");
                                txtCustomPrice.Text = "";
                            }
                        }
                    }
                    catch (Exception error)
                    {
                        //Console.WriteLine(error.Message);
                        MessageBox.Show("Error: Contains an unknown character - txtCustomPrice: " + error);
                        txtCustomPrice.Text = "";
                    }
                    break;
                case "txtCustomHours":
                    try { 
                    if (!String.IsNullOrEmpty(txtCustomHours.Text))
                    {
                        bool success = Double.TryParse(txtCustomHours.Text, out double result);
                        if (!success)
                        {
                            MessageBox.Show("Invalid text input");
                            txtCustomHours.Text = "";
                        }
                    }
                    }
                    catch (Exception error)
                    {
                        //Console.WriteLine(error.Message);
                        MessageBox.Show("Error: Contains an unknown character - txtCustomHours: " + error);
                        txtCustomHours.Text = "";
                    }
                    break;
            }
        }

        //Manages which Checkbox has been changed
        private void All_CheckBox_Changed(object sender, EventArgs e)
        {
            System.Windows.Forms.CheckBox chk = (sender as System.Windows.Forms.CheckBox);
            //MessageBox.Show("Textbox name: " + txt.Name + " | Textbox text:" + txt.Text);

            switch (chk.Name)
            {
                case "chkWaterJet":
                    try
                    {
                        if ( (String.IsNullOrEmpty(txtDimensionArea.Text) && (chkWaterJet.Checked == true)) || ((txtDimensionArea.Text.Contains("0.0")) && (chkWaterJet.Checked == true)) )
                        {
                            MessageBox.Show("Area is empty - uncheck and fill in area");
                            break;
                        }

                        ManageCheckGroupBox(chkWaterJet, grpWaterJet);
                        if (chkWaterJet.Checked == true)
                        {
                            viewModel.setItemWaterJetName(tabIndex, "WATERJET");
                            viewModel.setItemWaterJetSetup(tabIndex, txtDimensionArea.Text);
                        }
                        else
                        {
                            //Remove the Waterjet vairable name
                            viewModel.setItemWaterJetName(tabIndex, "");

                            //Set up the waterjet setup
                            viewModel.setItemWaterJetSetup(tabIndex, "");

                            //Reset all controls within the groupbox
                            lstWaterjetCutList.Items.Clear();
                            chkHoleDiameter.Checked = false;
                            cmbCutoutLength.SelectedItem = null;
                            cmbGpoCutout.SelectedItem = null;
                        }
                        
                        calculateAndUpdatePrices();
                    }
                    catch (Exception error)
                    {
                        MessageBox.Show("Error - chkWaterJet : " + error);
                    }
                    break;

                case "chkCustomDesign":
                    try
                    {
                        /*if ( (String.IsNullOrEmpty(txtDimensionArea.Text) && (chkCustomDesign.Checked == true)) || ((txtDimensionArea.Text.Contains("0.0")) && (chkCustomDesign.Checked == true)) )
                        {
                            MessageBox.Show("Area is empty - uncheck and fill in area");
                            break;
                        }*/

                        ManageCheckGroupBox(chkCustomDesign, grpCustomDesign);
                        if (chkCustomDesign.Checked == true)
                        {
                            viewModel.setItemCustomDesign(tabIndex, "CUSTOM DESIGN");
                        }
                        else
                        {
                            txtCustomQuantity.Text = "";
                            txtCustomMaterial.Text = "";
                            txtCustomPrice.Text = "";
                            txtCustomHours.Text = "";

                            viewModel.setItemCustomDesign(tabIndex, "");

                            viewModel.clearItemCustomDesignItem(tabIndex);
                            lstCustomDesign.Items.Clear();

                            calculateAndUpdatePrices();
                        }

                    }
                    catch (Exception error)
                    {
                        MessageBox.Show("Error - chkCustomDesign : " + error);
                    }
                    break;

                case "chkSlump":
                    try
                    {
                        if ( (String.IsNullOrEmpty(txtDimensionArea.Text) && (chkSlump.Checked == true)) || ((txtDimensionArea.Text.Contains("0.0")) && (chkSlump.Checked == true)) )
                        {
                            MessageBox.Show("Area is empty - uncheck and fill in area");
                            break;
                        }
                        ManageCheckGroupBox(chkSlump, grpSlump);
                        if (chkSlump.Checked == true)
                        {
                            viewModel.setItemSlumpName(tabIndex, "SLUMP");
                        }
                        else
                        {
                            try
                            {
                                viewModel.setItemSlumpName(tabIndex, "");

                                cmbSlumpTexture.Text = "";

                                viewModel.setItemSlumpCustomDescrition(tabIndex, "");

                                richtxtSlumpCustomDescription.Text = "";
                            }
                            catch (DataException a)
                            {
                                MessageBox.Show(a.ToString());
                            }
                        }
                    }
                    catch (Exception error)
                    {
                        MessageBox.Show("Error - chkSlump : " + error);
                    }
                    break;

                case "chkLaminate":
                    try
                    {
                        if ( (String.IsNullOrEmpty(txtDimensionArea.Text) && (chkLaminate.Checked == true)) || ((txtDimensionArea.Text.Contains("0.0")) && (chkLaminate.Checked == true)) )
                        {
                            MessageBox.Show("Area is empty - uncheck and fill in area");
                            break;
                        }
                        ManageCheckGroupBox(chkLaminate, grpLaminate);
                        if (chkLaminate.Checked == true)
                        {
                            viewModel.setItemLaminateName(tabIndex, "LAMINATE");
                        }
                        else
                        {
                            //Remove the price if it exists in the price list
                            if (!String.IsNullOrEmpty(viewModel.findItemName(tabIndex, viewModel.getItemLaminateSource(tabIndex) )))
                            {
                                viewModel.removeItemPrice(tabIndex, viewModel.getItemLaminateSource(tabIndex) );
                                calculateAndUpdatePrices();
                            }

                            viewModel.setItemLaminateName(tabIndex, "");
                            rbLaminateOutsource.Checked = false;
                            rbLaminateInsource.Checked = false; 
                        }
                    }
                    catch (Exception error)
                    {
                        MessageBox.Show("Error - chkLaminate : " + error);
                    }
                    break;

                case "chkBend":
                    try
                    {
                        if ( (String.IsNullOrEmpty(txtDimensionArea.Text) && (chkBend.Checked == true)) || ((txtDimensionArea.Text.Contains("0.0")) && (chkBend.Checked == true)) )
                        {
                            MessageBox.Show("Area is empty - uncheck and fill in area");
                            break;
                        }
                        ManageCheckGroupBox(chkBend, grpBend);
                        if (chkBend.Checked == true)
                        {
                            viewModel.setItemBendName(tabIndex, "BEND");
                        }
                        else
                        {
                            //Remove the price if it exists in the price list
                            if (!String.IsNullOrEmpty(viewModel.findItemName(tabIndex, viewModel.getItemBendSource(tabIndex) )))
                            {
                                viewModel.removeItemPrice(tabIndex, viewModel.getItemBendSource(tabIndex) );
                                calculateAndUpdatePrices();
                            }

                            viewModel.setItemBendName(tabIndex, "");
                            rbBendOutsource.Checked = false;
                            rbBendInsource.Checked = false;
                        }
                    }
                    catch (Exception error)
                    {
                        MessageBox.Show("Error - chkBend : " + error);
                    }
                    break;

                case "chkToughen":
                    try
                    {
                        if ( (String.IsNullOrEmpty(txtDimensionArea.Text) && (chkToughen.Checked == true) ) || ( (txtDimensionArea.Text.Contains("0.0")) && (chkToughen.Checked == true)) )
                        {
                            MessageBox.Show("Area is empty - uncheck and fill in area");
                            break;
                        }
                        ManageCheckGroupBox(chkToughen, grpToughen);
                        if (chkToughen.Checked == true)
                        {
                            viewModel.setItemToughenName(tabIndex ,"TOUGHENED");
                        }
                        else
                        {
                            //Remove the price if it exists in the price list //Note: needs to add the cut to size price
                            viewModel.clearToughenPrice(tabIndex, 0.0);

                            viewModel.setItemToughenName(tabIndex, "");

                            calculateAndUpdatePrices();

                            rbToughenOutsource.Checked = false;
                            rbToughenInsource.Checked = false;
                        }
                    }
                    catch (Exception error)
                    {
                        MessageBox.Show("Error - chkToughen : " + error);
                    }
                    break;

                case "chkLaser":
                    try
                    {
                        if ( (String.IsNullOrEmpty(txtDimensionArea.Text) && (chkLaser.Checked == true)) || ((txtDimensionArea.Text.Contains("0.0")) && (chkLaser.Checked == true)) )
                        {
                            MessageBox.Show("Area is empty - uncheck and fill in area");
                            break;
                        }
                        ManageCheckGroupBox(chkLaser, grpLaser);
                        if (chkLaser.Checked == true)
                        {
                            viewModel.setItemLaserName(tabIndex, "LASER");
                        }
                        else
                        {
                            //Remove the price if it exists in the price list
                            if (!String.IsNullOrEmpty(viewModel.findItemName(tabIndex, viewModel.getItemLaserType(tabIndex))))
                            {
                                viewModel.removeItemPrice(tabIndex, viewModel.getItemLaserType(tabIndex) );
                                calculateAndUpdatePrices();
                            }
                            viewModel.setItemLaserName(tabIndex, "");

                            rbLaserSimple.Checked = false;
                            rbLaserComplex.Checked = false;
                        }
                    }
                    catch (Exception error)
                    {
                        MessageBox.Show("Error - chkLaser : " + error);
                    }
                    break;

                case "chkSandblast":
                    try
                    {
                        if ( (String.IsNullOrEmpty(txtDimensionArea.Text) && (chkSandblast.Checked == true)) || ((txtDimensionArea.Text.Contains("0.0")) && (chkSandblast.Checked == true)) )
                        {
                            MessageBox.Show("Area is empty - uncheck and fill in area");
                            break;
                        }
                        ManageCheckGroupBox(chkSandblast, grpSandblast);
                        if (chkSandblast.Checked == true)
                        {
                            viewModel.setItemSandblastName(tabIndex, "SANDBLAST");
                        }
                        else
                        {
                            //Remove the price if it exists in the price list
                            if (!String.IsNullOrEmpty(viewModel.findItemName(tabIndex, viewModel.getItemSandblastSource(tabIndex) )))
                            {
                                viewModel.removeItemPrice(tabIndex, viewModel.getItemSandblastSource(tabIndex) );
                                calculateAndUpdatePrices();
                            }

                            viewModel.setItemSandblastName(tabIndex, "");
                            rbSandblastOutsource.Checked = false;
                            rbSandblastInsource.Checked = false;
                        }
                    }
                    catch (Exception error)
                    {
                        MessageBox.Show("Error - chkSandblast : " + error);
                    }
                    break;

                case "chkPolishing":
                    try
                    {
                        if ( (String.IsNullOrEmpty(txtDimensionArea.Text) && (chkPolishing.Checked == true)) || ((txtDimensionArea.Text.Contains("0.0")) && (chkPolishing.Checked == true)) )
                        {
                            MessageBox.Show("Area is empty - uncheck and fill in area");
                            break;
                        }
                        ManageCheckGroupBox(chkPolishing, grpPolishing);
                        if (chkPolishing.Checked == true)
                        {
                            viewModel.setItemPolishingName(tabIndex, "POLISHING");
                        }
                        else
                        {
                            //Remove the price if it exists in the price list
                            if (!String.IsNullOrEmpty(viewModel.findItemName(tabIndex, viewModel.getItemPolishingSource(tabIndex) )))
                            {
                                viewModel.removeItemPrice(tabIndex, viewModel.getItemPolishingSource(tabIndex) );
                                calculateAndUpdatePrices();
                            }
                            viewModel.setItemPolishingName(tabIndex, "");
                            rbPolishOutsource.Checked = false;
                            rbPolishInsource.Checked = false;
                        }
                    }
                    catch (Exception error)
                    {
                        MessageBox.Show("Error - chkPolishing : " + error);
                    }
                    break;

                case "chkPrint":
                    try
                    {
                        if ( (String.IsNullOrEmpty(txtDimensionArea.Text) && (chkPrint.Checked == true)) || ((txtDimensionArea.Text.Contains("0.0")) && (chkPrint.Checked == true)) )
                        {
                            MessageBox.Show("Area is empty - uncheck and fill in area");
                            break;
                        }
                        ManageCheckGroupBox(chkPrint, grpPrint);
                        if (chkPrint.Checked == true)
                        {
                            viewModel.setItemPrintName(tabIndex, "PRINT");
                        }
                        else
                        {
                            //Remove the price if it exists in the price list
                            if (!String.IsNullOrEmpty(viewModel.findItemName(tabIndex, viewModel.getItemPrintSize(tabIndex) )))
                            {
                                viewModel.removeItemPrice(tabIndex, viewModel.getItemPrintSize(tabIndex));
                                calculateAndUpdatePrices();
                            }
                            viewModel.setItemPrintName(tabIndex, "");
                            rbPrintSmall.Checked = false;
                            rbPrintLarge.Checked = false;
                        }
                    }
                    catch (Exception error)
                    {
                        MessageBox.Show("Error - chkPrint : " + error);
                    }
                    break;

                case "chkPaint":
                    try
                    {
                        if ( (String.IsNullOrEmpty(txtDimensionArea.Text) && (chkPaint.Checked == true)) || ((txtDimensionArea.Text.Contains("0.0")) && (chkPaint.Checked == true)) )
                        {
                            MessageBox.Show("Area is empty - uncheck and fill in area");
                            break;
                        }
                        ManageCheckGroupBox(chkPaint, grpPaint);
                        if (chkPaint.Checked == true)
                        {
                            viewModel.setItemPaintName(tabIndex, "PAINT");
                        }
                        else
                        {
                            viewModel.setItemPaintName(tabIndex, "");

                            cmbPaintType.Text = "";

                            viewModel.setItemPaintColorCode(tabIndex, "");

                            richtxtPaintColorCode.Text = "";

                            //If the price list already contains the price then remove it
                            if (!String.IsNullOrEmpty(viewModel.findItemName(tabIndex, viewModel.getItemPaintType(tabIndex) )))
                            {
                                //Remove the price
                                viewModel.removeItemPrice(tabIndex, viewModel.getItemPaintType(tabIndex));
                                calculateAndUpdatePrices();
                            }
                        }
                    }
                    catch (Exception error)
                    {
                        MessageBox.Show("Error - chkPaint : " + error);
                    }
                    break;
                case "chkHoleDiameter":
                    try
                    {
                        //Check if the other cut options have been checked, if so remove them (only one cut option can be selected)
                        if (chkHoleDiameter.Checked == true)
                        {
                            //Clear the control inputs
                            cmbCutoutLength.SelectedItem = null;
                            cmbGpoCutout.SelectedItem = null;
                            viewModel.setItemWaterJetCutSelection(tabIndex, "Holes < 75mm Diameter");
                        }
                    }
                    catch (Exception error)
                    {
                        MessageBox.Show("Error - chkHoleDiameter : " + error);
                    }
                    break;
                default:

                    break;
            }
        }

        //Manages which Radiobutton has been changed
        private void All_RadioButton_Changed(object sender, EventArgs e)
        {
            System.Windows.Forms.RadioButton rb = (sender as System.Windows.Forms.RadioButton);

            if (rb.Checked)
            {
                switch (rb.Name)
                {
                    case "rbToughenOutsource": //Note: outsource and insource prices are the same for now
                        try
                        { 
                            viewModel.setItemToughenSource(tabIndex, "outsource");
                            viewModel.setItemToughenPrice(tabIndex, "outsource");
                        }
                        catch (Exception error)
                        {
                            MessageBox.Show("Error - rb_toughen_outsource : " + error);
                        }
                        break;
                    case "rbToughenInsource": //Note: outsource and insource prices are the same for now
                        try
                        {
                            viewModel.setItemToughenSource(tabIndex, "insource");
                            viewModel.setItemToughenPrice(tabIndex, "insource");
                        }
                        catch (Exception error)
                        {
                            MessageBox.Show("Error - rb_toughen_insource : " + error);
                        }
                        break;

                    case "rbLaminateOutsource":
                        try
                        { 
                            //If there was already a selection, remove the price
                            if (viewModel.getItemLaminateSource(tabIndex) == "insource")
                            {
                                viewModel.removeItemPrice(tabIndex, viewModel.getItemLaminateSource(tabIndex) );
                            }

                            viewModel.setItemLaminateSource(tabIndex, "outsource"); 
                            //MessageBox.Show("rb_laminate_outsource: " + laminate_price_table.Rows[0][1].ToString());

                            //If a price does not exist in the price list
                            if (String.IsNullOrEmpty(viewModel.findItemName(tabIndex, viewModel.getItemLaminateSource(tabIndex))))
                            {
                                viewModel.addItemPrice(tabIndex, viewModel.getItemLaminateSource(tabIndex), viewModel.retrieveLaminatePrice("outsource"));
                            }
                        }
                        catch (Exception error)
                        {
                            MessageBox.Show("Error - rb_laminate_outsource : " + error);
                        }
                        break;
                    case "rbLaminateInsource":
                        try
                        { 
                            //If there was already a selection, remove the price
                            if (viewModel.getItemLaminateSource(tabIndex) == "outsource")
                            {
                                viewModel.removeItemPrice(tabIndex, viewModel.getItemLaminateSource(tabIndex));
                            }
                            
                            viewModel.setItemLaminateSource(tabIndex, "insource");
                            //MessageBox.Show("rb_laminate_insource: " + laminate_price_table.Rows[0][0].ToString());

                            //If a price does not exist in the price list
                            if (String.IsNullOrEmpty(viewModel.findItemName(tabIndex, viewModel.getItemLaminateSource(tabIndex) )))
                            {
                                viewModel.addItemPrice(tabIndex, viewModel.getItemLaminateSource(tabIndex), viewModel.retrieveLaminatePrice("insource"));
                            }
                        }
                        catch (Exception error)
                        {
                            MessageBox.Show("Error - rb_laminate_insource : " + error);
                        }
                        break;

                    case "rbPolishOutsource":
                        try
                        { 
                            //If there was already a selection, remove the price
                            if(viewModel.getItemPolishingSource(tabIndex) == "insource")
                            {
                                viewModel.removeItemPrice(tabIndex, viewModel.getItemPolishingSource(tabIndex) );
                            }

                            viewModel.setItemPolishingSource(tabIndex, "outsource");
                            //MessageBox.Show("rb_polish_outsource: " + polish_price_table.Rows[0][1].ToString());

                            //If a price does not exist in the price list
                            if (String.IsNullOrEmpty(viewModel.findItemName(tabIndex, viewModel.getItemPolishingSource(tabIndex) )))
                            {
                                viewModel.addItemPrice(tabIndex, viewModel.getItemPolishingSource(tabIndex), viewModel.retrievePolishingPrice("outsource"));
                            }
                        }
                        catch (Exception error)
                        {
                            MessageBox.Show("Error - rb_polish_outsource : " + error);
                        }
                        break;
                    case "rbPolishInsource":
                        try
                        { 
                            //If there was already a selection, remove the price
                            if (viewModel.getItemPolishingSource(tabIndex) == "outsource")
                            {
                                viewModel.removeItemPrice(tabIndex, viewModel.getItemPolishingSource(tabIndex));
                            }

                            viewModel.setItemPolishingSource(tabIndex, "insource");
                            //MessageBox.Show("rb_polish_insource: " + polish_price_table.Rows[0][0].ToString());

                            //If a price does not exist in the price list
                            if (String.IsNullOrEmpty(viewModel.findItemName(tabIndex, viewModel.getItemPolishingSource(tabIndex) )))
                            {
                                viewModel.addItemPrice(tabIndex, viewModel.getItemPolishingSource(tabIndex), viewModel.retrievePolishingPrice("insource"));
                            }
                        }
                        catch (Exception error)
                        {
                            MessageBox.Show("Error - rb_polish_insource : " + error);
                        }
                        break;

                    case "rbBendOutsource":
                        try
                        { 
                            //If there was already a selection, remove the price
                            if (viewModel.getItemBendSource(tabIndex) == "insource")
                            {
                                viewModel.removeItemPrice(tabIndex, viewModel.getItemBendSource(tabIndex) );
                            }

                            viewModel.setItemBendSource(tabIndex, "outsource");
                            //MessageBox.Show("rb_bend_outsource: " + bend_price_table.Rows[0][1].ToString());

                            //If a price does not exist in the price list
                            if (String.IsNullOrEmpty(viewModel.findItemName(tabIndex, viewModel.getItemBendSource(tabIndex) )))
                            {
                                viewModel.addItemPrice(tabIndex, viewModel.getItemBendSource(tabIndex), viewModel.retrieveBendPrice("outsource"));
                            }
                        }
                        catch (Exception error)
                        {
                            MessageBox.Show("Error - rb_bend_outsource : " + error);
                        }
                        break;
                    case "rbBendInsource":
                        try
                        { 
                            //If there was already a selection, remove the price
                            if (viewModel.getItemBendSource(tabIndex) == "outsource")
                            {
                                viewModel.removeItemPrice(tabIndex, viewModel.getItemBendSource(tabIndex) );
                            }

                            viewModel.setItemBendSource(tabIndex, "insource");
                            //MessageBox.Show("rb_bend_insource: " + bend_price_table.Rows[0][0].ToString());

                            //If a price does not exist in the price list
                            if (String.IsNullOrEmpty(viewModel.findItemName(tabIndex, viewModel.getItemBendSource(tabIndex) )))
                            {
                                viewModel.addItemPrice(tabIndex, viewModel.getItemBendSource(tabIndex), viewModel.retrieveBendPrice("insource"));
                            }
                        }
                        catch (Exception error)
                        {
                            MessageBox.Show("Error - rb_bend_insource : " + error);
                        }
                        break;

                    case "rbSandblastOutsource":
                        try
                        { 
                            //If there was already a selection, remove the price
                            if (viewModel.getItemSandblastSource(tabIndex) == "insource")
                            {
                                viewModel.removeItemPrice(tabIndex, viewModel.getItemSandblastSource(tabIndex) );
                            }
                            Console.WriteLine("rbSandblastOutsource" + viewModel.getItemSandblastSource(tabIndex));
                            viewModel.setItemSandblastSource(tabIndex, "outsource");
                            //MessageBox.Show("rb_sandblast_outsource: " + sandblast_price_table.Rows[0][1].ToString());

                            //If a price does not exist in the price list
                            if (String.IsNullOrEmpty(viewModel.findItemName(tabIndex, viewModel.getItemSandblastSource(tabIndex) )))
                            {
                                viewModel.addItemPrice(tabIndex, viewModel.getItemSandblastSource(tabIndex), viewModel.retrieveSandblastPrice("outsource"));
                            }
                        }
                        catch (Exception error)
                        {
                            MessageBox.Show("Error - rb_sandblast_outsource : " + error);
                        }
                        break;
                    case "rbSandblastInsource":
                        try
                        { 
                            //If there was already a selection, remove the price
                            if (viewModel.getItemSandblastSource(tabIndex) == "outsource")
                            {
                                viewModel.removeItemPrice(tabIndex, viewModel.getItemSandblastSource(tabIndex) );
                            }
                            Console.WriteLine("rbSandblastInsource" + viewModel.getItemSandblastSource(tabIndex));
                            viewModel.setItemSandblastSource(tabIndex, "insource");
                            //MessageBox.Show("rb_sandblast_insource: " + sandblast_price_table.Rows[0][0].ToString());

                            //If a price does not exist in the price list
                            if (String.IsNullOrEmpty(viewModel.findItemName(tabIndex, viewModel.getItemSandblastSource(tabIndex) )))
                            {
                                viewModel.addItemPrice(tabIndex, viewModel.getItemSandblastSource(tabIndex), viewModel.retrieveSandblastPrice("insource"));
                            }
                        }
                        catch (Exception error)
                        {
                            MessageBox.Show("Error - rb_sandblast_insource : " + error);
                        }
                        break;

                    case "rbLaserSimple":
                        try
                        { 
                            //If there was already a selection, remove the price
                            if (viewModel.getItemLaserType(tabIndex) == "complex")
                            {
                                viewModel.removeItemPrice(tabIndex, viewModel.getItemLaserType(tabIndex) );
                            }

                            viewModel.setItemLaserType(tabIndex, "simple");
                            //MessageBox.Show("rb_laser_simple: " + laser_price_table.Rows[0][0].ToString());

                            //If a price does not exist in the price list
                            if (String.IsNullOrEmpty(viewModel.findItemName(tabIndex, viewModel.getItemLaserType(tabIndex) )))
                            {
                                viewModel.addItemPrice(tabIndex, viewModel.getItemLaserType(tabIndex), viewModel.retrieveLaserPrice("simple"));
                            }
                        }
                        catch (Exception error)
                        {
                            MessageBox.Show("Error - rb_laser_simple : " + error);
                        }
                        break;
                    case "rbLaserComplex":
                        try
                        { 
                            //If there was already a selection, remove the price
                            if (viewModel.getItemLaserType(tabIndex) == "simple")
                            {
                                viewModel.removeItemPrice(tabIndex, viewModel.getItemLaserType(tabIndex) );
                            }

                            viewModel.setItemLaserType(tabIndex, "complex");
                            //MessageBox.Show("rb_laser_complex: " + laser_price_table.Rows[0][1].ToString());

                            //If a price does not exist in the price list
                            if (String.IsNullOrEmpty(viewModel.findItemName(tabIndex, viewModel.getItemLaserType(tabIndex) )))
                            {
                                viewModel.addItemPrice(tabIndex, viewModel.getItemLaserType(tabIndex), viewModel.retrieveLaserPrice("complex"));
                            }
                        }
                        catch (Exception error)
                        {
                            MessageBox.Show("Error - rb_laser_complex : " + error);
                        }
                        break;

                    case "rbPrintSmall":
                        try
                        { 
                            //If there was already a selection, remove the price
                            if (viewModel.getItemPrintSize(tabIndex) == "large")
                            {
                                viewModel.removeItemPrice(tabIndex, viewModel.getItemPrintSize(tabIndex) );
                            }

                            viewModel.setItemPrintSize(tabIndex, "small");

                            //If a price does not exist in the price list
                            if (String.IsNullOrEmpty(viewModel.findItemName(tabIndex, viewModel.getItemPrintSize(tabIndex) )))
                            {
                                //55.0 is a fixed price for now -> no entry in the database yet
                                viewModel.addItemPrice(tabIndex, viewModel.getItemPrintSize(tabIndex), viewModel.retrievePrintPrice("small"));
                            }
                        }
                        catch (Exception error)
                        {
                            MessageBox.Show("Error - rb_print_small : " + error);
                        }
                        break;
                    case "rbPrintLarge":
                        try
                        { 
                            //If there was already a selection, remove the price
                            if (viewModel.getItemPrintSize(tabIndex) == "small")
                            {
                                viewModel.removeItemPrice(tabIndex, viewModel.getItemPrintSize(tabIndex) );
                            }

                            viewModel.setItemPrintSize(tabIndex, "large");
                            //MessageBox.Show("rb_print_small: " + "110");

                            //If a price does not exist in the price list
                            if (String.IsNullOrEmpty(viewModel.findItemName(tabIndex, viewModel.getItemPrintSize(tabIndex))))
                            {
                                //110.0 is a fixed price for now -> no entry in the database yet
                                viewModel.addItemPrice(tabIndex, viewModel.getItemPrintSize(tabIndex), viewModel.retrievePrintPrice("large"));
                            }
                        }
                        catch (Exception error)
                        {
                            MessageBox.Show("Error - rb_print_large : " + error);
                        }
                        break;
                }
                //Update price
                calculateAndUpdatePrices();
            }
        }

        /*
        * Saves all input values from the controls, sets the new tab index, adds the panel to the new selected tab
        * Look through the item object list and find the values that can be set to the controls
        */
        private void tabcontrol_quotation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (delete_item_flag != true)
            {
                tabcontrol_quotation.SuspendLayout();

                //Adds the panel to the selected tab
                tabcontrol_quotation.SelectedTab.Controls.Add(pnl_quotation);

                //Set tab index to a new selected index
                tabIndex = tabcontrol_quotation.SelectedIndex;

                //Retrieves the values for the item based on which tab item has been selected 
                ManageItemControls();

                tabcontrol_quotation.ResumeLayout();
                //MessageBox.Show(" Calculation price list: " + item.getCalculations.GetPriceListCount + " Tab index: " + tabIndex);
            }
        }

        /*
        * Manages how the controls display the values from the item object
        */
        private void ManageItemControls() 
        {

            if(viewModel.getItemOverrideDollar(tabIndex) != 0.0)
            {
                txtItemOverrideDollar.Text = Convert.ToString(viewModel.getItemOverrideDollar(tabIndex));
            } else
            {
                txtItemOverrideDollar.Text = "";
            }

            if(viewModel.getItemOverridePercent(tabIndex) != 0.0)
            {
                txtItemOverridePercent.Text = Convert.ToString(viewModel.getItemOverridePercent(tabIndex));
            } else
            {
                txtItemOverridePercent.Text = "";
            }

            if(viewModel.getItemOverrideDollarSign(tabIndex) != "")
            {
                cmbItemOverrideDollar.Text = viewModel.getItemOverrideDollarSign(tabIndex);
            } else
            {
                cmbItemOverrideDollar.SelectedItem = "none";
            }

            if(viewModel.getItemOverridePercentSign(tabIndex) != "")
            {
                cmbItemOverridePercent.Text = viewModel.getItemOverridePercentSign(tabIndex);
            } else
            {
                cmbItemOverridePercent.SelectedItem = "none";
            }

            if (viewModel.getItemPriceFactor(tabIndex) != 4.7)
            {
                cmbPriceFactor.Text = Convert.ToString(viewModel.getItemPriceFactor(tabIndex));
            }
            else
            {
                cmbPriceFactor.SelectedItem = "4.7";
            }

            if (viewModel.getItemMaterialName(tabIndex) != "")
            {
                cmbMaterialType.Text = viewModel.getItemMaterialName(tabIndex);
                //Refresh the Thickness combobox
                LoadThicknessComboBox(viewModel.getItemMaterialName(tabIndex));

            }
            else
            {
                cmbMaterialType.Text = viewModel.getItemMaterialName(tabIndex);
                //Clear the Thickness combobox
                cmbMaterialThickness.Items.Clear();
                //MessageBox.Show("Empty - Item Name: " + item.Name);
            }

            if (viewModel.getItemMaterialThickness(tabIndex) != "")
            {
                cmbMaterialThickness.Text = viewModel.getItemMaterialThickness(tabIndex);
            }
            else
            {
                cmbMaterialThickness.Text = viewModel.getItemMaterialThickness(tabIndex);
            }

            if(viewModel.getItemMaterialDescription(tabIndex) != "")
            {
                txtMaterialCustomDescription.Text = viewModel.getItemMaterialDescription(tabIndex);
            } else
            {
                txtMaterialCustomDescription.Text = "";
            }

            if (viewModel.getItemDimensionHeight(tabIndex) != "")
            {
                txtDimensionHeight.Text = viewModel.getItemDimensionHeight(tabIndex);
            } else
            {
                txtDimensionHeight.Text = "";
            }

            if(viewModel.getItemDimensionWidth(tabIndex) != "")
            {
                txtDimensionWidth.Text = viewModel.getItemDimensionWidth(tabIndex);
            } else
            {
                txtDimensionWidth.Text = "";
            }

            if (viewModel.getItemDimensionRadius(tabIndex) != "")
            {
                txtDimensionRadius.Text = viewModel.getItemDimensionRadius(tabIndex); //note: if Height and Width is set before this, the Radius text event will trigger
            }
            else
            {
                txtDimensionRadius.Text = "";
            }

            lblDimensionPerimeter.Text = viewModel.getItemDimensionPerimeter(tabIndex);
            txtDimensionQuantity.Text = viewModel.getItemQuantity(tabIndex);

            if(viewModel.getItemDimensionArea(tabIndex) != "0.0" )
            {
                //MessageBox.Show("Item Area not 0.0: " + item.Area);
                txtDimensionArea.Text = viewModel.getItemDimensionArea(tabIndex);
               
                double val = 0.0;

                if (!String.IsNullOrEmpty(viewModel.getItemDimensionRadius(tabIndex) ))
                {
                    val = (Calculations.Area("", "", viewModel.getItemDimensionRadius(tabIndex) ));
                }
                else if(!String.IsNullOrEmpty(viewModel.getItemDimensionHeight(tabIndex) ) && !String.IsNullOrEmpty(viewModel.getItemDimensionWidth(tabIndex) ))
                {
                    val = (Calculations.Area(viewModel.getItemDimensionHeight(tabIndex), viewModel.getItemDimensionWidth(tabIndex), ""));
                } else
                {
                    //MessageBox.Show(" Possible error ");
                }

                //MessageBox.Show("Result Area: " + val + "   Height: " + item.Height + "   Width: " + item.Width + " Radius: " + item.Radius);
                if (val < 0.3)
                {
                    if (val == 0.0 || val == 0)
                    {
                        txtDimensionArea.Text = "0.0";
                        lblDimensionPerimeter.Text = "";
                        txtAreaOriginal.Text = Convert.ToString(val);
                        txtAreaOriginal.Visible = false;
                    }
                    else
                    {
                        txtDimensionArea.Text = "0.3";
                        txtAreaOriginal.Text = Convert.ToString(val);
                        txtAreaOriginal.Visible = true;
                    }
                    //MessageBox.Show("ManageItemControl Item Area val<0.3: " + Convert.ToString(val));
                }
                else
                {
                    txtAreaOriginal.Text = Convert.ToString(val);
                    txtAreaOriginal.Visible = false;
                    //MessageBox.Show("ManageItemControl Item Area val>0.3: " + Convert.ToString(val));
                }
                
            }
            else
            {
                //MessageBox.Show("Item Area is 0.0: " + item.Area);
                txtDimensionArea.Text = "0.0";
                txtAreaOriginal.Visible = false;
                txtAreaOriginal.Text = "";
            }


            //Handle WaterJet option
            if (viewModel.getItemWaterJetName(tabIndex) != "")
            {
                chkWaterJet.CheckState = CheckState.Checked;

                //Clears the list of items in the listbox
                lstWaterjetCutList.Items.Clear();

                //Add the stored list of cuts in the Item class into the listbox
                for(int i = 0; i < viewModel.getItemWaterJetCutListCount(tabIndex); i++)
                {
                    lstWaterjetCutList.Items.Add(viewModel.getItemWaterJetCutType(tabIndex, i) );
                }
            }
            else
            {
                chkWaterJet.CheckState = CheckState.Unchecked;
            }

            //Handle Custom Design
            if(viewModel.getItemCustomDesign(tabIndex) != "")
            {
                chkCustomDesign.CheckState = CheckState.Checked;
                ListViewItem listViewItem;
                //Clears the list of items in the custom design listview
                lstCustomDesign.Items.Clear();

                //Add the stored list of custom design items from the Item class into the listview
                for (int i = 0; i < viewModel.getItemCustomDesignItemList(tabIndex); i++)
                {
                    //Get text from the txt boxes and assign to the subitems
                    listViewItem = new ListViewItem(viewModel.getItemCustomItemQuantity(tabIndex, i), 0);
                    listViewItem.SubItems.Add(viewModel.getItemCustomItemPrice(tabIndex, i) );
                    listViewItem.SubItems.Add(viewModel.getItemCustomItemPrice(tabIndex, i) );
                    listViewItem.SubItems.Add(viewModel.getItemCustomItemHoursWorked(tabIndex, i) );

                    //Add items to the listView
                    lstCustomDesign.Items.Add(listViewItem);
                }
            }
            else
            {
                chkCustomDesign.CheckState = CheckState.Unchecked;
            }

            //Handle Slump option
            if (viewModel.getItemSlumpName(tabIndex) != "")
            {
                chkSlump.CheckState = CheckState.Checked;
            }
            else
            {
                chkSlump.CheckState = CheckState.Unchecked;
            }
            cmbSlumpTexture.Text = viewModel.getItemSlumpTexture(tabIndex);
            richtxtSlumpCustomDescription.Text = viewModel.getItemSlumpCustomDescription(tabIndex);

            //Handle Paint option
            if (viewModel.getItemPaintName(tabIndex) != "")
            {
                chkPaint.CheckState = CheckState.Checked;
            }
            else
            {
                chkPaint.CheckState = CheckState.Unchecked;
            }
            cmbPaintType.Text = viewModel.getItemPaintType(tabIndex);
            richtxtPaintColorCode.Text = viewModel.getItemPaintColorCode(tabIndex);

            //Handle Toughened option
            if (viewModel.getItemToughenName(tabIndex) != "")
            {
                chkToughen.CheckState = CheckState.Checked;

                //Handles the Toughened Radiobutton
                if (viewModel.getItemToughenSource(tabIndex) == "insource")
                {
                    rbToughenInsource.Checked = true;
                } else
                {
                    rbToughenOutsource.Checked = true;
                }
            }
            else
            {
                chkToughen.CheckState = CheckState.Unchecked;
                rbToughenInsource.Checked = false;
                rbToughenOutsource.Checked = false;
            }

            //Handle Laminate option
            if (viewModel.getItemLaminateName(tabIndex) != "")
            {
                chkLaminate.CheckState = CheckState.Checked;

                //Handles the Toughened Radiobutton
                if (viewModel.getItemLaminateSource(tabIndex) == "insource")
                {
                    rbLaminateInsource.Checked = true;
                }
                else
                {
                    rbLaminateOutsource.Checked = true;
                }
            }
            else
            {
                chkLaminate.CheckState = CheckState.Unchecked;
                rbLaminateInsource.Checked = false;
                rbLaminateOutsource.Checked = false;
            }

            //Handle Bend option
            if (viewModel.getItemBendName(tabIndex) != "")
            {
                chkBend.CheckState = CheckState.Checked;

                //Handles the Toughened Radiobutton
                if (viewModel.getItemBendSource(tabIndex) == "insource")
                {
                    rbBendInsource.Checked = true;
                }
                else
                {
                    rbBendOutsource.Checked = true;
                }
            }
            else
            {
                chkBend.CheckState = CheckState.Unchecked;
                rbBendInsource.Checked = false;
                rbBendOutsource.Checked = false;
            }

            //Handle Sandblast option
            if (viewModel.getItemSandblastName(tabIndex) != "")
            {
                chkSandblast.CheckState = CheckState.Checked;

                //Handles the Toughened Radiobutton
                if (viewModel.getItemSandblastSource(tabIndex) == "insource")
                {
                    rbSandblastInsource.Checked = true;
                }
                else
                {
                    rbSandblastOutsource.Checked = true;
                }
            }
            else
            {
                chkSandblast.CheckState = CheckState.Unchecked;
                rbSandblastInsource.Checked = false;
                rbSandblastOutsource.Checked = false;
            }

            //Handle Laser option
            if (viewModel.getItemLaserName(tabIndex) != "")
            {
                chkLaser.CheckState = CheckState.Checked;

                //Handles the Toughened Radiobutton
                if (viewModel.getItemLaserType(tabIndex) == "simple")
                {
                    rbLaserSimple.Checked = true;
                }
                else
                {
                    rbLaserComplex.Checked = true;
                }
            }
            else
            {
                chkLaser.CheckState = CheckState.Unchecked;
                rbLaserSimple.Checked = false;
                rbLaserComplex.Checked = false;
            }

            //Handle Polishing option
            if (viewModel.getItemPolishingName(tabIndex) != "")
            {
                chkPolishing.CheckState = CheckState.Checked;

                //Handles the Toughened Radiobutton
                if (viewModel.getItemPolishingSource(tabIndex) == "insource")
                {
                    rbPolishInsource.Checked = true;
                }
                else
                {
                    rbPolishOutsource.Checked = true;
                }
            }
            else
            {
                chkPolishing.CheckState = CheckState.Unchecked;
                rbPolishInsource.Checked = false;
                rbPolishOutsource.Checked = false;
            }

            //Handle Print option
            if (viewModel.getItemPrintName(tabIndex) != "")
            {
                chkPrint.CheckState = CheckState.Checked;

                //Handles the Toughened Radiobutton
                if (viewModel.getItemPrintSize(tabIndex) == "small")
                {
                    rbPrintSmall.Checked = true;
                }
                else
                {
                    rbPrintLarge.Checked = true;
                }
            }
            else
            {
                chkPrint.CheckState = CheckState.Unchecked;
                rbPrintSmall.Checked = false;
                rbPrintLarge.Checked = false;
            }

            lblItemCostPrice.Text = Convert.ToString(viewModel.getItemCostPrice(tabIndex));
            lblItemCustomerPrice.Text = Convert.ToString(viewModel.getItemCustomerPrice(tabIndex));
            lblItemJobPrice.Text = Convert.ToString(viewModel.getItemJobPrice(tabIndex));
        }

        /*
        * This method checks whether the CheckBox is inside the GroupBox and, 
        * if it is, the method moves it into the GroupBox‘s parent. 
        * It then adjusts the CheckBox‘s location so it has the same position on the form. 
        * It also moves it to the front of the stacking order so it doesn’t end up behind the GroupBox.
        * Finally, depending on the CheckBox‘s state, the method enables or disables the GroupBox 
        * (which enables or disables all of the controls it contains).
        */
        private void ManageCheckGroupBox(CheckBox chk, GroupBox grp)
        {
            // Make sure the CheckBox isn't in the GroupBox.
            // This will only happen the first time.
            if (chk.Parent == grp)
            {
                // Reparent the CheckBox so it's not in the GroupBox.
                grp.Parent.Controls.Add(chk);

                // Adjust the CheckBox's location.
                chk.Location = new Point(chk.Left + grp.Left, chk.Top + grp.Top);

                // Move the CheckBox to the top of the stacking order.
                chk.BringToFront();
            }
            // Enable or disable the GroupBox.
            grp.Enabled = chk.Checked;
        }
 
    }
}
