﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProfileGlassHome
{
    public partial class Supplier : Form
    {
        public Supplier()
        {
            InitializeComponent();

            btn_save.Click += new EventHandler(All_Button_Changed);
            btn_close.Click += new EventHandler(All_Button_Changed);
        }


        private void All_Button_Changed(object sender, EventArgs e)
        {
            System.Windows.Forms.Button btn = (sender as System.Windows.Forms.Button);
            switch (btn.Name)
            {
                case "btn_save":
                    this.Close();
                    break;
                case "btn_close":
                    this.Close();
                    break;
            }
        }

    }
}
