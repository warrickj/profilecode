﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProfileGlassHome
{
    public partial class Supplier_List : Form
    {
        public Supplier_List()
        {
            InitializeComponent();

            dg_supplier_list.ReadOnly = true;
            dg_supplier_list.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            btn_new_supplier.Click += new EventHandler(All_Button_Changed);
            btn_edit_supplier.Click += new EventHandler(All_Button_Changed);
            btn_close.Click += new EventHandler(All_Button_Changed);
        }



        private void All_Button_Changed(object sender, EventArgs e)
        {
            System.Windows.Forms.Button btn = (sender as System.Windows.Forms.Button);
            switch (btn.Name)
            {
                case "btn_new_supplier":
                    Supplier supplier_1 = new Supplier();
                    supplier_1.ShowDialog();
                    break;
                case "btn_edit_supplier":
                    Supplier supplier_2 = new Supplier();
                    supplier_2.ShowDialog();
                    break;
                case "btn_close":
                    this.Close();
                    break;
            }
        }

        private void dg_supplier_list_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            Supplier_Profile supplier_profile = new Supplier_Profile();
            supplier_profile.ShowDialog();
        }
    }
}
