﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProfileGlassHome
{
    public class Calculations
    {

        public static double Area(string height, string width, string radius)
        {

            if (!String.IsNullOrEmpty(height) && !String.IsNullOrEmpty(width) && String.IsNullOrEmpty(radius))
            {
                //Calculate Area of rectangle
                double area = Convert.ToDouble(height) * Convert.ToDouble(width) / 1000000;
                return Math.Round(area, 5);
            }

            if (String.IsNullOrEmpty(height) && String.IsNullOrEmpty(width) && !String.IsNullOrEmpty(radius))
            {
                //Calculate Area of circle
                Double area = Math.PI * Convert.ToDouble(radius) * 2 / 1000000;
                return Math.Round(area, 5);
            }

            return 0;
        }

        public static double Perimeter(string height, string width, string radius)
        {

            if (!string.IsNullOrEmpty(height) && !string.IsNullOrEmpty(width) && string.IsNullOrEmpty(radius))
            {
                //calculate Perimeter of rectangle
                Double perim = (2 * Convert.ToInt16(height)) + (2 * Convert.ToInt16(width));
                return Math.Round(perim, 2);

            }

            if (string.IsNullOrEmpty(height) && string.IsNullOrEmpty(width) && !string.IsNullOrEmpty(radius))
            {
                // calculate Perimeter of circle
                Double perim = 2 * Math.PI * Convert.ToDouble(radius);
                return Math.Round(perim, 2);

            }
            return 0;
        }

        public static double distance(double lat1, double lat2, double lon1, double lon2, string unit)
        {

            /*
            A circle contains 360 degrees, which is the equivalent of 2π radians, so 1π radians = 180 degrees
            lat1, lon1: The Latitude and Longitude of point 1 (in decimal degrees)
            lat2, lon2: The Latitude and Longitude of point 2 (in decimal degrees)
            unit: The unit of measurement in which to calculate the results where:
            'M' is statute miles (default)
            'K' is kilometers
            'N' is nautical miles
            
            dlon = lon2 - lon1 
            dlat = lat2 - lat1
            a = (sin(dlat/2))^2 + cos(lat1) * cos(lat2) * (sin(dlon/2))^2
            c = 2 * atan2( sqrt(a), sqrt(1-a) )
            distance = R * c (where R is the Radius of the Earth)

            R = 6367 km OR 3956 mi

             */
            try
            {

                double dlat = lat2 - lat1;
                double dlon = lon2 - lon1;

                double radlat1 = Math.PI * lat1 / 180; //Convert Degrees to Radians
                double radlat2 = Math.PI * lat2 / 180; //Convert Degrees to Radians

                double theta = lon1 - lon2; //Get the difference in Longitude
                double radtheta = Math.PI * theta * 180;// Convert result to Radians


                double dist = Math.Pow(Math.Sin(dlat / 2), 2) +
                              Math.Cos(lat1) * Math.Cos(lat2) *
                              Math.Pow(Math.Sin(dlon / 2), 2);

                dist = 2 * Math.Asin(Math.Sqrt(dist));

                switch (unit)
                {
                    case "M":
                        // Result = Miles
                        dist = dist / 1.1515;
                        return dist;


                    case "K":
                        // Result = Kilometers
                        dist = dist * 1.609344;
                        return dist;

                    case "N":
                        // Result = Nautical Miles
                        dist = dist * 0.8684;
                        return dist;

                }


            }
            catch (Exception err)
            {
                Console.WriteLine("{0} Exception caught.", err);
            }
            return 0;

        }


    }
}
