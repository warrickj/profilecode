﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ProfileGlassHome
{
    public partial class Customer_Profile : Form
    {


        private BindingSource bindingSource1 = new BindingSource();
        private BindingSource bindingSource2 = new BindingSource();

        string _company_name;
        string _first_last_name;
        string _SysNo;


        public Customer_Profile(string contact_id, string detail_id, string company_name, string first_name, string last_name,
                                string position_title, string address, string address2, string suburb, string state,
                                string post_code, string phone, string mobile, string fax, string email)
        {
            InitializeComponent();

            _company_name = company_name;
            _first_last_name = first_name + " " + last_name;


            lbl_customer_profile_company.Text = company_name;
            lbl_customer_profile_firstname.Text = first_name;
            lbl_customer_profile_lastname.Text = last_name;
            lbl_customer_profile_position_title.Text = position_title;
            lbl_customer_profile_address.Text = address;
            lbl_customer_profile_address2.Text = address2;
            lbl_customer_profile_suburb.Text = suburb;
            lbl_customer_profile_state.Text = state;
            lbl_customer_profile_postcode.Text = post_code;
            lbl_customer_profile_phone.Text = phone;
            lbl_customer_profile_mobile.Text = mobile;
            lbl_customer_profile_fax.Text = fax;
            lbl_customer_profile_email.Text = email;

            int contact_id_int = Int32.Parse(contact_id);

            dg_quotation_list.ReadOnly = true;
            dg_quotation_list.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            dg_job_list.ReadOnly = true;
            dg_job_list.SelectionMode = DataGridViewSelectionMode.FullRowSelect;


            btn_profile_new_quote.Click += new EventHandler(All_Button_Changed);
            btn_profile_edit_quote.Click += new EventHandler(All_Button_Changed);
            btn_profile_close.Click += new EventHandler(All_Button_Changed);
            

        }


        //Manages which button has been changed
        private void All_Button_Changed(object sender, EventArgs e)
        {
            System.Windows.Forms.Button btn = (sender as System.Windows.Forms.Button);

            switch (btn.Name)
            {
                case "btn_profile_new_quote":
                    Quotation quotation_1 = new Quotation(_company_name, _first_last_name, _SysNo, false);
                    quotation_1.ShowDialog();
                    break;
                case "btn_profile_edit_quote":
                    Quotation quotation_2 = new Quotation(_company_name, _first_last_name, _SysNo, false);
                    quotation_2.ShowDialog();
                    break;
                case "btn_profile_close":
                        this.Close();
                    break;
                default:

                    break;
            }

        }


        private void populateGrid()
        {
            /*try
            {

            } 
            catch (SqlException)
            {
                MessageBox.Show("D'Oh... Something went wrong!");
            }
            */

        }


        /*
        private void getQuotes(int contact_id)
        {
            DataTable CurrentQuotesTable = new DataTable();
            string CurrentQuotes;

            db.Select("SELECT qh.quotation_num                                              " +
                             "      ,qh.version                                                    " +
                             "      ,qh.quotation_amount                                           " +
                             "      ,qh.quotation_date                                             " +
                             "      ,qh.description                                                " +
                             "      , l.meaning                                                    " +
                             "  FROM quotation_headers qh                                          " +
                             " INNER JOIN quotation_lines ql ON qh.quotation_id = ql.quotation_id  " +
                             " INNER JOIN lookups l ON l.lookup_id = qh.payment_status_flag        " +
                             " INNER JOIN contacts c ON c.contact_id = qh.contact_id               " +
                             " WHERE qh.[contact_id] = " + contact_id);

            db.get_SqlDataAdapter.Fill(CurrentQuotesTable);

            CurrentQuotes = Convert.ToString(CurrentQuotesTable.Rows[0][0]);

            CurrentQuotesTable.Clear();

        }*/


        /*
         *Opens up Quotation
            _SysNo = "";
        Quotation quotation = new Quotation(_company_name, _first_last_name, _SysNo, false);
        quotation.ShowDialog(); 
         * 
         */


        private void dg_quotation_list_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {

            //If the job list has a selection deselect it 


            //Check if a quote exists in the datagridview

            //Open Quote summary with details of the quote
            try
            {
                if (e.RowIndex >= 0)
                {
                    /*
                    DataGridViewRow row = this.dg_ViewPersonPersonList.Rows[e.RowIndex];
                    contact_id = row.Cells[(int)Details.ContactID].Value.ToString();
                    detail_id = row.Cells[(int)Details.DetailID].Value.ToString();
                    company_name = row.Cells[(int)Details.Company].Value.ToString();
                    first_name = row.Cells[(int)Details.FirstName].Value.ToString();
                    last_name = row.Cells[(int)Details.LastName].Value.ToString();
                    position_title = row.Cells[(int)Details.PositionTitle].Value.ToString();
                    address = row.Cells[(int)Details.Address].Value.ToString();
                    address2 = row.Cells[(int)Details.Address2].Value.ToString();
                    suburb = row.Cells[(int)Details.Suburb].Value.ToString();
                    state = row.Cells[(int)Details.State].Value.ToString();
                    post_code = row.Cells[(int)Details.PostCode].Value.ToString();
                    phone = row.Cells[(int)Details.Phone].Value.ToString();
                    mobile = row.Cells[(int)Details.Mobile].Value.ToString();
                    fax = row.Cells[(int)Details.Fax].Value.ToString();
                    email = row.Cells[(int)Details.Email].Value.ToString();

                    //Opens up the customer profile
                    Customer_Profile customer_profile = new Customer_Profile(contact_id,
                                                                            detail_id,
                                                                            company_name,
                                                                            first_name,
                                                                            last_name,
                                                                            position_title,
                                                                            address,
                                                                            address2,
                                                                            suburb,
                                                                            state,
                                                                            post_code,
                                                                            phone,
                                                                            mobile,
                                                                            fax,
                                                                            email);
                    customer_profile.ShowDialog();
                    */

                    Quote_Summary quote_summary = new Quote_Summary();
                    quote_summary.ShowDialog();

                }
            }
            catch (Exception me)
            {
                MessageBox.Show(me.ToString());
            }

        }

        private void dg_job_list_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            //If the job list has a selection deselect it 

            //Check if a job exists in the datagridview


            //Open Job summary with details of the job
            try
            {
                if (e.RowIndex >= 0)
                {
                    /*
                    DataGridViewRow row = this.dg_ViewPersonPersonList.Rows[e.RowIndex];
                    contact_id = row.Cells[(int)Details.ContactID].Value.ToString();
                    detail_id = row.Cells[(int)Details.DetailID].Value.ToString();
                    company_name = row.Cells[(int)Details.Company].Value.ToString();
                    first_name = row.Cells[(int)Details.FirstName].Value.ToString();
                    last_name = row.Cells[(int)Details.LastName].Value.ToString();
                    position_title = row.Cells[(int)Details.PositionTitle].Value.ToString();
                    address = row.Cells[(int)Details.Address].Value.ToString();
                    address2 = row.Cells[(int)Details.Address2].Value.ToString();
                    suburb = row.Cells[(int)Details.Suburb].Value.ToString();
                    state = row.Cells[(int)Details.State].Value.ToString();
                    post_code = row.Cells[(int)Details.PostCode].Value.ToString();
                    phone = row.Cells[(int)Details.Phone].Value.ToString();
                    mobile = row.Cells[(int)Details.Mobile].Value.ToString();
                    fax = row.Cells[(int)Details.Fax].Value.ToString();
                    email = row.Cells[(int)Details.Email].Value.ToString();

                    //Opens up the customer profile
                    Customer_Profile customer_profile = new Customer_Profile(contact_id,
                                                                            detail_id,
                                                                            company_name,
                                                                            first_name,
                                                                            last_name,
                                                                            position_title,
                                                                            address,
                                                                            address2,
                                                                            suburb,
                                                                            state,
                                                                            post_code,
                                                                            phone,
                                                                            mobile,
                                                                            fax,
                                                                            email);
                    customer_profile.ShowDialog();
                    */
                    Job_Summary job_summary = new Job_Summary();
                    job_summary.ShowDialog();


                }
            }
            catch (Exception me)
            {
                MessageBox.Show(me.ToString());
            }

        }



    }

}
