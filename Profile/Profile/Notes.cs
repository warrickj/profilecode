﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ProfileGlassHome
{
    public partial class Notes : Form
    {
        NotesViewModel viewModel = new NotesViewModel();
        Quotation quotationForm;

        public Notes(Quotation context, string sysNo, string notes)
        {
            InitializeComponent();
            quotationForm = context;
            ////MessageBox.Show(sysNo);
            lblSystemNumber.Text = sysNo;
            txtNote.Text = notes;
            /*string notesDirectory = checkNotesPath(SysNo);
            string file = Path.Combine(notesDirectory, "Notes.txt");
            lblLocation.Text = file;
            */
            DateTime current = DateTime.Now;
            lblDate.Text = current.ToLongDateString();
            lblTime.Text = current.ToLongTimeString();
            //

            //existingEntries(SysNo);
            //displayEntryFor(SysNo);
        }

        private bool checkNotes(string file)
        {
            if (!File.Exists(file))
            {
                var newFile = File.Create(file);
                newFile.Close();
                if (File.Exists(file))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            } else
            {
                return true;
            }
            /*else
            { //Error is occurring here --------
                //Change 'Add Note' button appearance to indicate a note exists
                //this.btn_quotation_note.Image = Image.FromFile("C:\\ProgramData\\ProfileData\\note.ico");
                // Align the image and text on the button.
                this.btn_quotation_note.ImageAlign = ContentAlignment.MiddleRight;
                this.btn_quotation_note.TextAlign = ContentAlignment.MiddleLeft;
                this.btn_quotation_note.Text = "Note  ";
                this.btn_quotation_note.BackColor = Color.FromArgb(192, 255, 192);

                return true;
            }*/
        }

        private void displayFile(string file)
        {
            if (File.Exists(file))
                txtNote.Text = File.ReadAllText(file);
            else
                txtNote.Text = "";
        }

        private void writeFile()
        {
            string file = lblLocation.Text;
            string lines = Environment.NewLine + DateTime.Now + Environment.NewLine + txtNoteEntry.Text + Environment.NewLine;

            try
            {
                File.AppendAllText(file, lines);
                txtNoteEntry.Clear();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
        }

        private void readFile()
        {
            string file = lblLocation.Text;
            txtNote.Text = File.ReadAllText(file);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            //writeFile();
            //readFile();
            //string file = lblLocation.Text;
            string note = Environment.NewLine + DateTime.Now + Environment.NewLine + txtNote.Text + Environment.NewLine;
            quotationForm.saveToNotes(note);
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
