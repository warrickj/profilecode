﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProfileGlassHome
{
    public partial class frm_Home : Form
    {
        public frm_Home()
        {
            InitializeComponent();

            //DateTime now = DateTime.Now;
            //MessageBox.Show("Date Time: " + now);

            linklbl_quote_calculation.LinkClicked += new LinkLabelLinkClickedEventHandler(LinkLabel_Clicked);
            linklbl_view_customers.LinkClicked += new LinkLabelLinkClickedEventHandler(LinkLabel_Clicked);
            linklbl_view_suppliers.LinkClicked += new LinkLabelLinkClickedEventHandler(LinkLabel_Clicked);

            btn_new_quotation.Click += new EventHandler(All_Button_Changed);
            btn_new_purchase_order.Click += new EventHandler(All_Button_Changed);

            dg_active_quotes.ReadOnly = true;
            dg_active_jobs.ReadOnly = true;
            dg_active_purchase_orders.ReadOnly = true;

            dg_active_quotes.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dg_active_jobs.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dg_active_purchase_orders.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        private void btn_HomeNewQuote_Click(object sender, EventArgs e)
        {
            frm_ViewCustomer f2 = new frm_ViewCustomer();
            this.Close();
            f2.ShowDialog();
        }

        private void All_Button_Changed(object sender, EventArgs e)
        {
            System.Windows.Forms.Button btn = (sender as System.Windows.Forms.Button);
            switch(btn.Name)
            {
                case "btn_new_quotation":
                    frm_ViewCustomer f2 = new frm_ViewCustomer();
                    this.Close();
                    f2.ShowDialog();
                    break;
                case "btn_new_purchase_order":
                    PurchaseOrder purchase_order = new PurchaseOrder();
                    this.Close();
                    purchase_order.ShowDialog();
                    break;
            }
        }


            private void LinkLabel_Clicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //Allow user to quickly access the quotation, boolean value triggers the form to disable button functionality
            
            
            System.Windows.Forms.LinkLabel lnk = (sender as System.Windows.Forms.LinkLabel);
            switch (lnk.Name)
            {
                case "linklbl_quote_calculation":
                    Quotation quotation = new Quotation(" ", " ", " ", true);
                    quotation.ShowDialog();
                    break;
                case "linklbl_view_customers":
                    frm_ViewCustomer view_customer = new frm_ViewCustomer();
                    view_customer.ShowDialog();
                    break;
                case "linklbl_view_suppliers":
                    Supplier_List supplier_list = new Supplier_List();
                    supplier_list.ShowDialog();
                    break;
            }
        }

        private void dg_active_purchase_orders_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            PurchaseOrder purchase_order = new PurchaseOrder();
            purchase_order.ShowDialog();
        }

        private void dg_active_jobs_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            Job_Summary job_summary = new Job_Summary();
            job_summary.ShowDialog();
        }

        private void dg_active_quotes_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            Quote_Summary quote_summary = new Quote_Summary();
            quote_summary.ShowDialog();
        }
    }
}
