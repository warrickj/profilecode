﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProfileGlassHome
{
    class QuotationModel
    {
        private Quote quote;
        public QuotationModel()
        {
            quote = new Quote();
        }

        public Quote getQuote
        {
            get
            {
                return quote;
            }
        }


        //-----------------------------------------------------------------------------------QUOTE------------------------------------------------------------------------------// 
        public class Quote
        {
            private List<Item> itemList = new List<Item>();
            private Item item;

            private string companyName = "";
            private string firstLastName = "";
            private string address = "";

            public string getCompanyName
            {
                get
                {
                    return companyName;
                }
                set
                {
                    companyName = value;
                }
            }

            public string getFirstLastName
            {
                get
                {
                    return firstLastName;
                }
                set
                {
                    firstLastName = value;
                }
            }

            public string getAddress
            {
                get
                {
                    return address;
                }
                set
                {
                    address = value;
                }
            }

            private string quote_id_number = "";
            private string quote_created_date = "";
            private string quote_version = "";

            private double installationCost = 0;
            private double deliveryCost = 0;

            private double totalPrice = 0.0;
            private double override_dollar = 0.0;
            private string override_dollar_sign = "";

            private double override_percent = 0.0;
            private string override_percent_sign = "";

            private double gstPercent = 10.0;
            private double gstAmount = 0.0;
            private double totalAndGstPrice = 0.0;

            private double fixedInstallationCost = 100.0;
            private double fixedDeliveryCost = 1.81;

            private string installationHours = "";
            private string deliveryDistance = "";

            public Quote(/*string company, string name,string quote_number, string created_date,string version*/)
            {
                /*companyName = company;
                firstLastName = name;


                quote_created_date = created_date;
                quote_version = version;
                */
                //Initialise a new item
                addItem();
            }

            public string OverrideDollarSign
            {
                get
                {
                    return override_dollar_sign;
                }
                set
                {
                    override_dollar_sign = value;
                }
            }

            public string OverridePercentSign
            {
                get
                {
                    return override_percent_sign;
                }
                set
                {
                    override_percent_sign = value;
                }
            }

            public double OverrideDollar
            {
                get
                {
                    return override_dollar;
                }
                set
                {
                    override_dollar = value;
                }
            }

            public double OverridePercent
            {
                get
                {
                    return override_percent;
                }
                set
                {
                    override_percent = value;
                }
            }

            public void setInstallationHours(string hours)
            {
                installationHours = hours;
            }

            public void setDeliveryDistance(string distance)
            {
                deliveryDistance = distance;
            }
 
            //Installation price
            public String getInstallationCost()
            {
                if (Double.TryParse(installationHours, out double result))
                {
                    //Console.WriteLine(result);
                    installationCost = fixedInstallationCost * result;
                    return Convert.ToString(installationCost);
                }
                else
                {
                    installationCost = 0;
                    return "";
                }
            }

            //Delivery price
            public String getDeliveryCost()
            {
                if (Double.TryParse(deliveryDistance, out double result))
                {
                    //Console.WriteLine(result);
                    deliveryCost = fixedDeliveryCost * result;
                    return Convert.ToString(deliveryCost);
                }
                else
                {
                    deliveryCost = 0;
                    return "";
                }
            }

            //Quote calculation total
            //Needs to combine the total item cost + installation cost + delivery cost + the override(% and $)
            public double getTotalPrice
            {
                get
                {
                    //Re-initialise the variable
                    totalPrice = 0.0;

                    //Calculate the total cost of items in the list
                    foreach (Item item in itemList)
                    {
                        totalPrice += item.GetCustomerPrice;
                    }

                    //Add the installation cost and delivery cost
                    totalPrice += installationCost + deliveryCost;

                    //Apply override (%)
                    if (override_dollar != 0.0)
                    {
                        //Check the dollar sign (+/-)
                        if (override_dollar_sign == "+")
                        {
                            totalPrice += override_dollar;
                        }
                        else
                        {
                            totalPrice -= override_dollar;
                        }
                    }

                    //Apply override (+/-)
                    if (override_percent != 0.0)
                    {
                        if (override_percent_sign == "+")
                        {
                            totalPrice += ((override_percent / 100) * totalPrice);
                        }
                        else
                        {
                            totalPrice -= ((override_percent / 100) * totalPrice);
                        }
                    }
                    return totalPrice;
                }
            }

            public double getGstAmount
            {
                get
                {
                    gstAmount = totalPrice * (gstPercent / 100);
                    return gstAmount;
                }
            }

            public double getQuoteTotalAndGstPrice
            {
                get
                {
                    totalAndGstPrice = totalPrice + gstAmount;
                    return totalAndGstPrice;
                }
            }

            //Add a new item
            public void addItem()
            {
                item = new Item();
                itemList.Add(item);
            }

            //Remove a specific item from the list
            public void removeItem(int index)
            {
                itemList.RemoveAt(index);
            }

            //Gets the item object based on the index passed as a parameter
            public Item getItem(int index)
            {
                Item _item = null;
                //Sets the tab page with the values associated with it through the item object 
                foreach (Item i in itemList)
                {
                    //c++;
                    if (i.Equals(itemList[index]))
                    {
                        _item = i;
                    }
                }
                return _item;
            }

            public bool rearrangeItems()
            {
                List<Item> listCopy = new List<Item>();
                bool flag = true;
                try
                {
                    //If an item is not null in the list, add it to a new list 
                    foreach (var i in itemList)
                    {
                        if (i != null)
                        {
                            listCopy.Add(i);
                        }
                    }
                    //Assign the old list the new list of items
                    itemList.Clear();
                    itemList = listCopy;

                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    flag = false;
                }
                return flag;
            }

            public int getItemCount
            {
                get
                {
                    return itemList.Count;
                }
            }

        }


        //-----------------------------------------------------------------------------------ITEM------------------------------------------------------------------------------//
        public class Item
        {
            //Variables used to store information about the item
            public Materials materials;
            public Dimensions dimensions;
            public Slump slump;
            public Toughen toughen;
            public Laminate laminate;
            public Bend bend;
            public Laser laser;
            public Sandblast sandblast;
            public Polishing polishing;
            public Paint paint;
            public Print print;
            public WaterJet waterJet;
            public CustomDesign customDesign;

            //Variables used for calculating prices
            private double costPrice = 0.0;
            private double jobPrice = 0.0;
            private double customerPrice = 0.0;

            private double _override_dollar = 0.0;
            private string _override_dollar_sign = "";
            private double _override_percent = 0.0;
            private string _override_percent_sign = "";

            private double _pricefactor = 4.7;
            private double _process_pricefactor = 1.5;

            private double _glass_price = 0.0;
            private double _toughen_price = 0.0;
            private double _process_price = 0.0;

            private int newLines = 2; //Note: this includes the newlines added in the function CreateQuote

            Dictionary<string, double> listOfPrices = new Dictionary<string, double>();

            public Item()
            {
                materials = new Materials();
                dimensions = new Dimensions("", "", "", "", "");
                slump = new Slump();
                toughen = new Toughen();
                laminate = new Laminate();
                bend = new Bend();
                laser = new Laser();
                sandblast = new Sandblast();
                polishing = new Polishing();
                paint = new Paint();
                print = new Print();
                waterJet = new WaterJet();
                customDesign = new CustomDesign();
            }

            //Used to track the number of newlines that have been added - will be used for spacing out the items in the quote
            public int getNewLineCount
            {
                get
                {
                    return newLines;
                }
            }

            //Appends all the strings on all processes that have been chosen by the user
            public string itemSummary
            {
                get
                {
                    newLines = 2; //re-initialise
                    string summary = "";
                    if(!String.IsNullOrEmpty(materials.Name) )
                    {
                        summary += materials.Thickness + "mm " + materials.Name + Environment.NewLine;
                        newLines += 1;
                        if (!String.IsNullOrEmpty(materials.Description))
                        {
                            summary += "Description: " + materials.Description + Environment.NewLine;
                            newLines += 1;
                        }
                    }
                    if(!String.IsNullOrEmpty(dimensions.Area) )
                    {
                        if (!String.IsNullOrEmpty(dimensions.Height))
                        {
                            summary += "Height & Width: " + dimensions.Height + "mm " + " x " + dimensions.Width + "mm " + Environment.NewLine;
                            newLines += 1;
                        }
                        if (!String.IsNullOrEmpty(dimensions.Radius))
                        {
                            summary += "Radius: " + dimensions.Radius + "mm " + Environment.NewLine;
                            newLines += 1;
                        }
                    }
                    if(!String.IsNullOrEmpty(slump.Name) )
                    {
                        summary += slump.Name + " - " + Environment.NewLine;
                        newLines += 1;
                        if (!String.IsNullOrEmpty(slump.Texture))
                        {
                            summary += "Texture: " + slump.Texture + Environment.NewLine;
                            newLines += 1;
                        }
                        if (!String.IsNullOrEmpty(slump.CustomDescription))
                        {
                            summary += "Custom description: " + slump.CustomDescription + Environment.NewLine;
                            newLines += 1;
                        }
                    }
                    if (!String.IsNullOrEmpty(toughen.Name))
                    {
                        summary += toughen.Name + " - " + Environment.NewLine;
                        newLines += 1;
                        if (!String.IsNullOrEmpty(toughen.Source))
                        {
                            summary += "Source: " + toughen.Source + Environment.NewLine;
                            newLines += 1;
                        }
                    }
                    if (!String.IsNullOrEmpty(laminate.Name))
                    {
                        summary += laminate.Name + " - " + Environment.NewLine;
                        newLines += 1;
                        if (!String.IsNullOrEmpty(laminate.Source))
                        {
                            summary += "Source: " + laminate.Source + Environment.NewLine;
                            newLines += 1;
                        }
                    }
                    if (!String.IsNullOrEmpty(bend.Name))
                    {
                        summary += bend.Name + " - " + Environment.NewLine;
                        newLines += 1;
                        if (!String.IsNullOrEmpty(bend.Source))
                        {
                            summary += "Source: " + bend.Source + Environment.NewLine;
                            newLines += 1;
                        }
                    }
                    if (!String.IsNullOrEmpty(laser.Name))
                    {
                        summary += laser.Name + " - " + Environment.NewLine;
                        newLines += 1;
                        if (!String.IsNullOrEmpty(laser.Type))
                        {
                            summary += "Type: " + laser.Type + Environment.NewLine;
                            newLines += 1;
                        }
                    }
                    if (!String.IsNullOrEmpty(sandblast.Name))
                    {
                        summary += sandblast.Name + " - " + Environment.NewLine;
                        newLines += 1;
                        if (!String.IsNullOrEmpty(sandblast.Source))
                        {
                            summary += "Source: " + sandblast.Source + Environment.NewLine;
                            newLines += 1;
                        }
                    }
                    if (!String.IsNullOrEmpty(polishing.Name))
                    {
                        summary += polishing.Name + " - " + Environment.NewLine;
                        newLines += 1;
                        if (!String.IsNullOrEmpty(polishing.Source))
                        {
                            summary += "Source: " + polishing.Source + Environment.NewLine;
                            newLines += 1;
                        }
                    }
                    if (!String.IsNullOrEmpty(paint.Name))
                    {
                        summary += paint.Name + " - " + Environment.NewLine;
                        newLines += 1;
                        if (!String.IsNullOrEmpty(paint.Type))
                        {
                            summary += "Type: " + paint.Type + Environment.NewLine;
                            newLines += 1;
                        }
                        if (!String.IsNullOrEmpty(paint.ColorCode))
                        {
                            summary += "ColorCode: " + paint.ColorCode + Environment.NewLine;
                            newLines += 1;
                        }
                    }
                    if (!String.IsNullOrEmpty(print.Name))
                    {
                        summary += print.Name + " - " + Environment.NewLine;
                        newLines += 1;
                        if (!String.IsNullOrEmpty(print.Size))
                        {
                            summary += "Size: " + print.Size + Environment.NewLine;
                            newLines += 1;
                        }
                    }
                    if (!String.IsNullOrEmpty(waterJet.Name))
                    {
                        summary += waterJet.Name + " - " + Environment.NewLine;
                        newLines += 1;
                        for (int i = 0;i < waterJet.getListOfWaterJetCutCounts; i++)
                        {
                            summary += "Cut type: " + waterJet.getWaterJetCutType(i) + Environment.NewLine;
                            newLines += 1;
                        }
                    }
                    if (!String.IsNullOrEmpty(customDesign.Name))
                    {
                        summary += customDesign.Name + " - " + Environment.NewLine;
                        newLines += 1;
                        for (int i = 0; i < customDesign.getCustomItemListCount; i++)
                        {
                            summary += "Qty: " + customDesign.getCustomItemQuantity(i) + " Material: " + customDesign.getCustomItemMaterial(i) + " Price: " + customDesign.getCustomItemPrice(i) + " Hours worked: " + customDesign.getCustomItemHoursWorked(i) + Environment.NewLine;
                            newLines += 1;
                        }
                        
                    }

                    return summary;
                }
            }

            public double glass_price
            {
                get
                {
                    return _glass_price;
                }
                set
                {
                    _glass_price = value;
                }
            }

            public double toughen_price
            {
                get
                {
                    return _toughen_price;
                }
                set
                {
                    _toughen_price = value;
                }
            }

            public string FindName(string name)
            {
                string result = "";
                foreach (KeyValuePair<string, double> kvp in listOfPrices)
                {
                    if (kvp.Key == name)
                    {
                        result = kvp.Key.ToString();
                    }
                }
                return result;
            }

            public string FindPrice(string name)
            {
                string result = "";
                foreach (KeyValuePair<string, double> kvp in listOfPrices)
                {
                    if (kvp.Key == name)
                    {
                        result = kvp.Value.ToString();
                    }
                }
                return result;
            }

            public void AddPrice(string name, double price)
            {
                listOfPrices.Add(name, price);
            }

            //Remove price from the list
            public void RemovePrice(string name)
            {
                foreach (KeyValuePair<string, double> kvp in listOfPrices)
                {
                    //Controls how the first value is handled
                    if (kvp.Key.Contains(name))
                    {
                        listOfPrices.Remove(kvp.Key);
                        break;
                    }
                }
            }

            public string GetPriceListCount
            {
                get
                {
                    return listOfPrices.Count.ToString();
                }
            }

            //Clear the price list
            public void ClearPriceList()
            {
                listOfPrices.Clear();
            }

            public double PriceFactor
            {
                get
                {
                    return _pricefactor;
                }
                set
                {
                    _pricefactor = value;
                }
            }

            public double GetCostPrice
            {
                get
                {
                    double result = 0;
                    //Calculates the combined price of all processes checked
                    foreach (KeyValuePair<string, double> kvp in listOfPrices)
                    {
                        result += kvp.Value * Convert.ToDouble(dimensions.Area);
                    }
                    _process_price = result; //Assign to global variable to be used in the GetJobPrice property
                    
                    if (!String.IsNullOrEmpty(dimensions.Area))
                    {
                        result += _glass_price * Convert.ToDouble(dimensions.Area);
                        result += _toughen_price * Convert.ToDouble(dimensions.Area);
                    } else
                    {
                        result += _glass_price;
                        result += _toughen_price;
                    }

                    //Apply the Quantity to the result
                    if (materials.Quantity != 0)
                    {
                        costPrice = result * Convert.ToDouble(materials.Quantity);
                    }
                    else
                    {
                        costPrice = result;
                    }
                    //Console.WriteLine("CostPrice: " + costPrice + " Result: " + result);
                    return costPrice;
                }
            }

            public double GetJobPrice
            {
                get
                {
                    if (!String.IsNullOrEmpty(dimensions.Area))
                    {
                        jobPrice = (_glass_price * Convert.ToDouble(dimensions.Area) * _pricefactor) + (_toughen_price * Convert.ToDouble(dimensions.Area)) + (_process_price * Convert.ToDouble(dimensions.Area) * _process_pricefactor) + waterJet.getWaterjetCutPrices;
                    } else
                    {
                        jobPrice = waterJet.getWaterjetCutPrices;
                    }

                    if (materials.Quantity != 0)
                    {
                        jobPrice = (jobPrice * Convert.ToDouble(materials.Quantity)) + customDesign.getCustomDesignPrice;
                    }
                    else
                    {
                        jobPrice = customDesign.getCustomDesignPrice;
                    }
                    //Console.WriteLine("JobPrice: " + jobPrice);
                    return jobPrice;
                }
            }

            public double GetCustomerPrice
            {
                get
                {
                    customerPrice = jobPrice;

                    //Check if override ($)
                    if (_override_dollar != 0.0)
                    {
                        //Check the dollar sign (+/-)
                        if (_override_dollar_sign == "+")
                        {
                            customerPrice += _override_dollar;
                        }
                        if (_override_dollar_sign == "-")
                        {
                            customerPrice -= _override_dollar;
                        }
                    }

                    if (_override_percent != 0.0)
                    {
                        if (_override_percent_sign == "+")
                        {
                            customerPrice += ((_override_percent / 100) * customerPrice);
                        }
                        if (_override_percent_sign == "-")
                        {
                            customerPrice -= ((_override_percent / 100) * customerPrice);
                        }
                    }
                    return customerPrice;
                }
            }

            public double OverrideDollar
            {
                get
                {
                    return _override_dollar;
                }
                set
                {
                    _override_dollar = value;
                }
            }

            public double OverridePercent
            {
                get
                {
                    return _override_percent;
                }
                set
                {
                    _override_percent = value;
                }
            }

            public string OverrideDollarSign
            {
                get
                {
                    return _override_dollar_sign;
                }
                set
                {
                    _override_dollar_sign = value;
                }
            }

            public string OverridePercentSign
            {
                get
                {
                    return _override_percent_sign;
                }
                set
                {
                    _override_percent_sign = value;
                }
            }
        }

        public class Materials
        {
            private string name = "";
            private string thickness = "";
            private string description = ""; //NEW
            private int quantity = 0;

            public string Name
            {
                get
                {
                    return name;
                }
                set
                {
                    name = value;
                }
            }
            public string Description
            {
                get
                {
                    return description;
                }
                set
                {
                    description = value;
                }
            }
            public string Thickness
            {
                get
                {
                    return thickness;
                }
                set
                {
                    thickness = value;
                }
            }
            public int Quantity
            {
                get
                {
                    return quantity;
                }
                set
                {
                    quantity = value;
                }
            }
        }

        public class Dimensions
        {
            private string height = "";
            private string width = "";
            private string radius = "";
            private string area = "0.0";
            private string perimeter = "";

            public Dimensions(string height, string width, string radius, string area, string perimeter)
            {
                this.height = height;
                this.width = width;
                this.radius = radius;
                this.area = area;
                this.perimeter = perimeter;
            }

            public string Height
            {
                get
                {
                    return height;
                }
                set
                {
                    height = value;
                }

            }
            public string Width
            {
                get
                {
                    return width;
                }
                set
                {
                    width = value;
                }

            }
            public string Radius
            {
                get
                {
                    return radius;
                }
                set
                {
                    radius = value;
                }

            }
            public string Area
            {
                get
                {
                    return area;
                }
                set
                {
                    area = value;
                }

            }
            public string Perimeter
            {
                get
                {
                    return perimeter;
                }
                set
                {
                    perimeter = value;
                }

            }
        }

        public class WaterJet
        {
            private string name = "";
            public string Name
            {
                get
                {
                    return name;
                }
                set
                {
                    name = value;
                }
            }

            private string waterjetCutSelection = "";
            public string getWaterjetCutSelection
            {
                get
                {
                    return waterjetCutSelection;
                }
                set
                {
                    waterjetCutSelection = value;
                }
            }

            //Contains prices of 3-8 and 10-12 thickness prices
            double[,] priceArray = new double[,]
            {
            { 5.00, 8.00 }, //Holes < 75mm Diameter - 0
            { 7.50, 12.80 }, //GPO Cutout Horizontal - 1
            { 7.50, 12.80 }, //GPO Cutout Vertical - 2
            { 7.50, 12.80 }, //Cutout Length <500mm - 3
            { 12.70, 25.70 }, //Cutout Length 501 - 1000mm - 4
            { 23.70, 51.40 }, //Cutout Length 1001 - 2000mm - 5
            { 38.10, 80.90 }, //Cutout Length 2001 - 3000mm - 6
            { 49.70, 107.40 } //Cutout Length 3001 - 4000mm - 7
            };

            public enum thickness_enum
            {
                thickness_3_8, //0
                thickness_10_12 //1
            }

            //Contains the name of the cuts and their indexes
            List<string> listOfCutTypes = new List<string> {
                "Holes < 75mm Diameter",
                "GPO Cutout Horizontal",
                "GPO Cutout Vertical",
                "Cutout Length < 500mm",
                "Cutout Length 501 - 1000mm",
                "Cutout Length 1001 - 2000mm",
                "Cutout Length 2001 - 3000mm",
                "Cutout Length 3001 - 4000mm",
            };

            /*Dictionary<string, int> listOfCutTypes = new Dictionary<string, int>()
            {
                "Holes < 75mm Diameter",
                "GPO Cutout Horizontal",
                "GPO Cutout Vertical",
                "Cutout Length < 500mm",
                "Cutout Length 501 - 1000mm",
                "Cutout Length 1001 - 2000mm",
                "Cutout Length 2001 - 3000mm",
                "Cutout Length 3001 - 4000mm",
            };*/

            //Contains the stored cuts and there prices
            private List<CutType> listOfWaterJetCuts = new List<CutType>();
            /*public List<CutType> getListOfWaterJetCuts
            {
                get
                {
                    return listOfWaterJetCuts;
                }
            }
            */
            public int getListOfWaterJetCutCounts
            {
                get
                {
                    return listOfWaterJetCuts.Count;
                }
            }

            public string getWaterJetCutType(int index)
            {
                return listOfWaterJetCuts[index].getType;//customItemList[index].getQuantity;
            }

            //Function to add to the list of Waterjet cuts and calculate the prices
            public string AddWaterjetCuts(string type, string thickness)
            {
                if (!String.IsNullOrEmpty(thickness))
                {
                    double thick = Convert.ToDouble(thickness);

                    //Determine the current Thickness
                    if (thick >= 3 && thick <= 8)
                    {
                        try
                        {
                            //Determine if the cut type exists in the list of cut types
                            /*foreach (KeyValuePair<string, int> type in listOfCutTypes)
                            {
                                //Obtain the value according to the cut type
                                if (cutType.Contains(type.Key))
                                {
                                    //Add cut type and price to the list of cuts
                                    listOfWaterJetCuts.Add(type.Key, priceArray[type.Value, (int)thickness_enum.thickness_3_8]);
                                    
                                }
                            }*/

                            for(int i = 0; i < listOfCutTypes.Count; i++ )
                            {
                                if(type.Contains(listOfCutTypes[i]))
                                {
                                    CutType cutType = new CutType(type, priceArray[i, (int)thickness_enum.thickness_3_8]);
                                    listOfWaterJetCuts.Add(cutType);
                                }
                            }

                            return "";
                        }
                        catch (Exception e)
                        {
                            return "Error catch " + e + ": " + " Cut count: " + listOfCutTypes.Count + "  Price array length: " + priceArray.Length;
                        }
                    }
                    else if (thick >= 10 && thick <= 12)
                    {
                        try
                        {
                            //Determine if the cut type exists in the list of cut types
                            /*foreach (KeyValuePair<string, int> type in listOfCutTypes)
                            {
                                //Obtain the value according to the cut type
                                if (cutType.Contains(type.Key))
                                {
                                    //Add cut type and price to the list of cuts
                                    listOfWaterJetCuts.Add(type.Key, priceArray[type.Value, (int)thickness_enum.thickness_10_12]);
                                }
                            }*/

                            for (int i = 0; i < listOfCutTypes.Count; i++)
                            {
                                if (type.Contains(listOfCutTypes[i]))
                                {
                                    CutType cutType = new CutType(type, priceArray[i, (int)thickness_enum.thickness_10_12]);
                                    listOfWaterJetCuts.Add(cutType);
                                }
                            }

                            return "";
                        }
                        catch (Exception e)
                        {
                            return "Error catch " + e + ": " + " Cut count: " + listOfCutTypes.Count + "  Price array length: " + priceArray.Length;
                        }
                    }
                    else
                    {
                        return "Unknown Error";
                    }
                }
                else
                {
                    return "A Thickness needs to be set";
                }
            }

            //Function to delete from the list of Waterjet cuts
            public string DeleteWaterjetCuts(string type)
            {
                try
                {
                    for (int i = 0; i < listOfCutTypes.Count; i++)
                    {
                        if (type.Contains(listOfCutTypes[i]))
                        {
                            listOfWaterJetCuts.RemoveAt(i);
                        }
                    }


                    //Determine if the cut type exists in the list of cut types
                    /*foreach (KeyValuePair<string, int> type in listOfCutTypes)
                    {
                        //Obtain the value according to the cut type
                        if (cutType.Contains(type.Key))
                        {
                            //Delete cut type and price from the list of cuts
                            listOfWaterJetCuts.Remove(type.Key);
                        }
                    }*/
                    return "";
                }
                catch (Exception e)
                {
                    return "Error catch " + e;
                }
            }

            public class CutType
            {

                private string type = "";
                private double price = 0.0;

                public CutType(string type, double price)
                {
                    this.type = type;
                    this.price = price;
                }

                public string getType
                {
                    get
                    {
                        return type;
                    }
                }

                public double getPrice
                {
                    get
                    {
                        return price;
                    }
                }

            }

            //Calculate Waterjet cut prices
            public double getWaterjetCutPrices
            {
                get
                {
                    double result = 0.0;
                    result += waterJetSetupPrice;
                    foreach (var cutPrice in listOfWaterJetCuts)
                    {
                        result += cutPrice.getPrice;
                    }
                    return result;
                }
            }

            private double waterJetSetupPrice = 0.0;

            public void setWaterJetSetupPrice(string area)
            {
                //Determine if the panel is less than or greater than 1.8m^2
                if (Convert.ToDouble(area) < 1.8)
                {
                    waterJetSetupPrice = 25.0;
                }
                else
                {
                    waterJetSetupPrice = 35.0;
                }
            }

            public void clearWaterJetSetupPrice()
            {
                waterJetSetupPrice = 0.0;
            }

        }

        public class CustomDesign
        {
            private string name = "";

            //Dictionary<string, CustomDesign> _custom_design_list = new Dictionary<string, CustomDesign>();
            List<CustomItem> customItemList = new List<CustomItem>();
            CustomItem customItem;
            private double customDesignPrice = 0.0;

            public string Name
            {
                get
                {
                    return name;
                }
                set
                {
                    name = value;
                }
            }

            public string getCustomItemQuantity(int index)
            {
                return customItemList[index].getQuantity;
            }

            public string getCustomItemMaterial(int index)
            {
                return customItemList[index].getMaterial;
            }

            public string getCustomItemPrice(int index)
            {
                return customItemList[index].getPrice;
            }

            public string getCustomItemHoursWorked(int index)
            {
                return customItemList[index].getHours;
            }

            public int getCustomItemListCount
            {
                get
                {
                    return customItemList.Count;
                }
            }

            public void addCustomItem(int qty, string material, double price, double hours)
            {
                customItem = new CustomItem(qty, material, price, hours);
                customItemList.Add(customItem);
            }

            //Get index
            public void deleteCustomItem(int index)
            {
                customItemList.RemoveAt(index);
                customDesignPrice = 0.0;
            }

            public void clearCustomDesignItems()
            {
                customItemList.Clear();
                customDesignPrice = 0.0;
            }

            public double getCustomDesignPrice
            {
                get
                {
                    //Reset the prices
                    customDesignPrice = 0;
                    foreach (var item in customItemList)
                    {
                        customDesignPrice += item.GetTotalPrice;
                    }
                    //Console.WriteLine("customDesignPrice: " + customDesignPrice);
                    return customDesignPrice;
                }
            }

            public class CustomItem
            {
                private int quantity = 0;
                private string material = "";
                private double price = 0.0;
                private double hoursWorked = 0;
                private double fixedWorkCost = 100.0;
                private double totalPrice = 0.0;

                public CustomItem(int quantity, string material, double price, double hoursWorked)
                {
                    this.quantity = quantity;
                    this.material = material;
                    this.price = price;
                    this.hoursWorked = hoursWorked;
                }

                public string getQuantity
                {
                    get
                    {
                        return quantity.ToString();
                    }
                }

                public string getMaterial
                {
                    get
                    {
                        return material;
                    }
                }

                public string getPrice
                {
                    get
                    {
                        return price.ToString();
                    }
                }

                public string getHours
                {
                    get
                    {
                        return hoursWorked.ToString();
                    }
                }

                public double GetTotalPrice
                {
                    get
                    {
                        totalPrice = Convert.ToDouble(quantity) * price + Convert.ToDouble(hoursWorked) * fixedWorkCost;
                        return totalPrice;
                    }
                }
            }

        }

        public class Slump
        {
            private string name = "";
            private string texture = "";
            private string customDescription = "";

            public string Name
            {
                get
                {
                    return name;
                }
                set
                {
                    name = value;
                }

            }
            public string Texture
            {
                get
                {
                    return texture;
                }
                set
                {
                    texture = value;
                }

            }
            public string CustomDescription
            {
                get
                {
                    return customDescription;
                }
                set
                {
                    customDescription = value;
                }
            }
        }

        public class Paint
        {
            private string name = "";
            private string type = "";
            private string colorCode = "";

            public string Name
            {
                get
                {
                    return name;
                }
                set
                {
                    name = value;
                }

            }
            public string Type
            {
                get
                {
                    return type;
                }
                set
                {
                    type = value;
                }

            }
            public string ColorCode
            {
                get
                {
                    return colorCode;
                }
                set
                {
                    colorCode = value;
                }
            }
        }

        public class Toughen
        {
            private string name = "";
            private string source = "";

            public string Name
            {
                get
                {
                    return name;
                }
                set
                {
                    name = value;
                }

            }
            public string Source
            {
                get
                {
                    return source;
                }
                set
                {
                    source = value;
                }
            }
        }

        public class Laminate
        {
            private string name = "";
            private string source = "";

            public string Name
            {
                get
                {
                    return name;
                }
                set
                {
                    name = value;
                }
            }
            public string Source
            {
                get
                {
                    return source;
                }
                set
                {
                    source = value;
                }
            }
        }

        public class Bend
        {
            private string name = "";
            private string source = "";

            public string Name
            {
                get
                {
                    return name;
                }
                set
                {
                    name = value;
                }

            }
            public string Source
            {
                get
                {
                    return source;
                }
                set
                {
                    source = value;
                }

            }
        }

        public class Laser
        {
            private string name = "";
            private string type = "";

            public string Name
            {
                get
                {
                    return name;
                }
                set
                {
                    name = value;
                }

            }
            public string Type
            {
                get
                {
                    return type;
                }
                set
                {
                    type = value;
                }

            }
        }

        public class Sandblast
        {
            private string name = "";
            private string source = "";

            public string Name
            {
                get
                {
                    return name;
                }
                set
                {
                    name = value;
                }

            }
            public string Source
            {
                get
                {
                    return source;
                }
                set
                {
                    source = value;
                }

            }
        }

        public class Polishing
        {
            private string name = "";
            private string source = "";

            public string Name
            {
                get
                {
                    return name;
                }
                set
                {
                    name = value;
                }

            }
            public string Source
            {
                get
                {
                    return source;
                }
                set
                {
                    source = value;
                }

            }
        }

        public class Print
        {
            private string name = "";
            private string size = "";

            public string Name
            {
                get
                {
                    return name;
                }
                set
                {
                    name = value;
                }

            }
            public string Size
            {
                get
                {
                    return size;
                }
                set
                {
                    size = value;
                }

            }
        }

    }
}
