﻿namespace ProfileGlassHome
{
    partial class frm_ViewCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_ViewPersonClose = new System.Windows.Forms.Button();
            this.dg_ViewPersonPersonList = new System.Windows.Forms.DataGridView();
            this.lbl_ViewPersonFindPerson = new System.Windows.Forms.Label();
            this.txt_ViewPersonFindPerson = new System.Windows.Forms.TextBox();
            this.btn_CustomerEdit = new System.Windows.Forms.Button();
            this.btn_Customer_Refresh = new System.Windows.Forms.Button();
            this.btn_CustomerNewAdd = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dg_ViewPersonPersonList)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_ViewPersonClose
            // 
            this.btn_ViewPersonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_ViewPersonClose.Location = new System.Drawing.Point(797, 404);
            this.btn_ViewPersonClose.Name = "btn_ViewPersonClose";
            this.btn_ViewPersonClose.Size = new System.Drawing.Size(75, 25);
            this.btn_ViewPersonClose.TabIndex = 2;
            this.btn_ViewPersonClose.Text = "Close";
            this.btn_ViewPersonClose.UseVisualStyleBackColor = true;
            this.btn_ViewPersonClose.Click += new System.EventHandler(this.btn_ViewPersonClose_Click);
            // 
            // dg_ViewPersonPersonList
            // 
            this.dg_ViewPersonPersonList.AllowUserToAddRows = false;
            this.dg_ViewPersonPersonList.AllowUserToResizeRows = false;
            this.dg_ViewPersonPersonList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dg_ViewPersonPersonList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dg_ViewPersonPersonList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_ViewPersonPersonList.Location = new System.Drawing.Point(12, 53);
            this.dg_ViewPersonPersonList.Name = "dg_ViewPersonPersonList";
            this.dg_ViewPersonPersonList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dg_ViewPersonPersonList.Size = new System.Drawing.Size(860, 340);
            this.dg_ViewPersonPersonList.TabIndex = 3;
            this.dg_ViewPersonPersonList.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dg_ViewPersonPersonList_CellMouseDoubleClick);
            // 
            // lbl_ViewPersonFindPerson
            // 
            this.lbl_ViewPersonFindPerson.AutoSize = true;
            this.lbl_ViewPersonFindPerson.Location = new System.Drawing.Point(12, 24);
            this.lbl_ViewPersonFindPerson.Name = "lbl_ViewPersonFindPerson";
            this.lbl_ViewPersonFindPerson.Size = new System.Drawing.Size(112, 13);
            this.lbl_ViewPersonFindPerson.TabIndex = 4;
            this.lbl_ViewPersonFindPerson.Text = "Find Person/Company";
            // 
            // txt_ViewPersonFindPerson
            // 
            this.txt_ViewPersonFindPerson.Location = new System.Drawing.Point(130, 21);
            this.txt_ViewPersonFindPerson.Name = "txt_ViewPersonFindPerson";
            this.txt_ViewPersonFindPerson.Size = new System.Drawing.Size(303, 20);
            this.txt_ViewPersonFindPerson.TabIndex = 5;
            this.txt_ViewPersonFindPerson.TextChanged += new System.EventHandler(this.txt_ViewPersonFindPerson_TextChanged);
            this.txt_ViewPersonFindPerson.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_ViewPersonFindPerson_KeyDown);
            // 
            // btn_CustomerEdit
            // 
            this.btn_CustomerEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_CustomerEdit.Location = new System.Drawing.Point(751, 19);
            this.btn_CustomerEdit.Name = "btn_CustomerEdit";
            this.btn_CustomerEdit.Size = new System.Drawing.Size(125, 23);
            this.btn_CustomerEdit.TabIndex = 6;
            this.btn_CustomerEdit.Text = "Edit Customer";
            this.btn_CustomerEdit.UseVisualStyleBackColor = true;
            this.btn_CustomerEdit.Click += new System.EventHandler(this.btn_CustomerEdit_Click);
            // 
            // btn_Customer_Refresh
            // 
            this.btn_Customer_Refresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Customer_Refresh.Location = new System.Drawing.Point(539, 19);
            this.btn_Customer_Refresh.Name = "btn_Customer_Refresh";
            this.btn_Customer_Refresh.Size = new System.Drawing.Size(75, 23);
            this.btn_Customer_Refresh.TabIndex = 7;
            this.btn_Customer_Refresh.Text = "Refresh";
            this.btn_Customer_Refresh.UseVisualStyleBackColor = true;
            this.btn_Customer_Refresh.Click += new System.EventHandler(this.btn_Customer_Refresh_Click);
            // 
            // btn_CustomerNewAdd
            // 
            this.btn_CustomerNewAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_CustomerNewAdd.Location = new System.Drawing.Point(620, 19);
            this.btn_CustomerNewAdd.Name = "btn_CustomerNewAdd";
            this.btn_CustomerNewAdd.Size = new System.Drawing.Size(125, 23);
            this.btn_CustomerNewAdd.TabIndex = 8;
            this.btn_CustomerNewAdd.Text = "Add New Customer";
            this.btn_CustomerNewAdd.UseVisualStyleBackColor = true;
            this.btn_CustomerNewAdd.Click += new System.EventHandler(this.btn_CustomerNewAdd_Click);
            // 
            // frm_ViewCustomer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 441);
            this.Controls.Add(this.btn_CustomerNewAdd);
            this.Controls.Add(this.btn_Customer_Refresh);
            this.Controls.Add(this.btn_CustomerEdit);
            this.Controls.Add(this.txt_ViewPersonFindPerson);
            this.Controls.Add(this.lbl_ViewPersonFindPerson);
            this.Controls.Add(this.dg_ViewPersonPersonList);
            this.Controls.Add(this.btn_ViewPersonClose);
            this.Name = "frm_ViewCustomer";
            this.Text = "View Customer";
            ((System.ComponentModel.ISupportInitialize)(this.dg_ViewPersonPersonList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_ViewPersonClose;
        private System.Windows.Forms.DataGridView dg_ViewPersonPersonList;
        private System.Windows.Forms.Label lbl_ViewPersonFindPerson;
        private System.Windows.Forms.TextBox txt_ViewPersonFindPerson;
        private System.Windows.Forms.Button btn_CustomerEdit;
        private System.Windows.Forms.Button btn_Customer_Refresh;
        private System.Windows.Forms.Button btn_CustomerNewAdd;
    }
}