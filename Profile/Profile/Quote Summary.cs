﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProfileGlassHome
{
    public partial class Quote_Summary : Form
    {
        public Quote_Summary()
        {
            InitializeComponent();

            dg_quote_list.ReadOnly = true;
            dg_quote_list.SelectionMode = DataGridViewSelectionMode.FullRowSelect;


            btn_accept_quote.Click += new EventHandler(All_Button_Changed);
            btn_close.Click += new EventHandler(All_Button_Changed);
            btn_payment_details.Click += new EventHandler(All_Button_Changed);

        }

        private void All_Button_Changed(object sender, EventArgs e)
        {
            System.Windows.Forms.Button btn = (sender as System.Windows.Forms.Button);
            switch (btn.Name)
            {
                case "btn_accept_quote":
                    PaymentDetails payment_details_1 = new PaymentDetails();
                    payment_details_1.ShowDialog();
                    break;
                case "btn_payment_details":
                    PaymentDetails payment_details_2 = new PaymentDetails();
                    payment_details_2.ShowDialog();
                    break;
                case "btn_close":
                    this.Close();
                    break;
            }
        }

        private void dg_quote_list_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            Quotation quotation = new Quotation("","","",true);
            quotation.ShowDialog();
        }
    }
}
