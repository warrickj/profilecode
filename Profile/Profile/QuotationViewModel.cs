﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ProfileGlassHome
{
    public class QuotationViewModel
    {
        //Used for accessing the database class
        dbConnect db = new dbConnect();

        double smallPrintPrice = 55.0;
        double largePrintPrice = 110.0;

        //Note: will be used later for storing information from the database
        private DataTable glassThicknessTable = new DataTable();         //Stores list ofglass thicknesses available
        private DataTable glassMaterialTable = new DataTable();         //Stores list of types of materials available
        private DataTable slumpTextureTable = new DataTable();         //Stores list of name textures available
        private DataTable paintTable = new DataTable();         //Stores list of Name treatments available

        private DataTable glassPriceTable = new DataTable();
        private DataTable paintPriceTable = new DataTable();
        private DataTable polishPriceTable = new DataTable();
        private DataTable laminatePriceTable = new DataTable();
        private DataTable bendPriceTable = new DataTable();
        private DataTable sandblastPriceTable = new DataTable();
        private DataTable laserPriceTable = new DataTable();

        public QuotationViewModel()
        {

        }

        //********************************************************RETRIEVE DATA FUNCTIONS**********************************************************************//
        public DataTable retrieveMaterialData
        {
            get {
                int result = db.Select("SELECT meaning " +
                                            "  FROM lookups " +
                                            " WHERE lookup_type = 'MATERIALS' " +
                                            "   AND enabled_flag = 'Y'" +
                                            "   AND start_date_active <= GETDATE()" +
                                            " ORDER BY cast(display_sequence AS INT)");
                if (result == 0)
                {
                    db.get_SqlDataAdapter.Fill(glassMaterialTable);
                    //MessageBox.Show("Row Count: " + table.Rows.Count + " Column Count: " + table.Columns.Count);
                    return glassMaterialTable;
                } else
                {
                    return null;
                }
            }
        }
        public DataTable retrieveSlumpData
        {
            get
            {
                slumpTextureTable.Clear();
                int result = db.Select("SELECT meaning " +
                                            "  FROM lookups " +
                                            " WHERE lookup_type ='SLUMP' " +
                                            "   AND enabled_flag = 'Y' " +
                                            "   AND start_date_active <= GETDATE() " +
                                            " ORDER BY DISPLAY_SEQUENCE");
                if (result == 0)
                {
                    db.get_SqlDataAdapter.Fill(slumpTextureTable);
                    //MessageBox.Show("Row Count: " + table.Rows.Count + " Column Count: " + table.Columns.Count);
                    return slumpTextureTable;
                } else
                {
                    return null;
                }
            }
        }
        public DataTable retrieveGlassThicknessData (string material)
        {
            glassThicknessTable.Clear();

            //MessageBox.Show("loading cmb_material_type " + name);
            int result = db.Select("SELECT l1.ATTRIBUTE1  FROM lookups l1                                " +
                                            " INNER JOIN lookups l2 on cast(l2.lookup_id as int) = l1.lookup_code " +
                                            " WHERE l2.meaning = '" + material + "'                              " +
                                            "   AND l2.lookup_type = 'MATERIALS'                                  " +
                                            "   AND l1.LOOKUP_TYPE = 'PRICING'                                    " +
                                            "   AND l1.enabled_flag = 'Y'                                         " +
                                            "   AND l1.start_date_active <= GETDATE()                             " +
                                            " ORDER BY cast(l1.display_sequence as int)                           ");
            if (result == 0)
            {
                db.get_SqlDataAdapter.Fill(glassThicknessTable);
                //MessageBox.Show("Row Count: " + thickness_table.Rows.Count + " Column Count: " + thickness_table.Columns.Count);
                return glassThicknessTable;
            } else
            {
                return null;
            }
        }
        public DataTable retrievePaintData
        {
            get
            {
                paintTable.Clear();
                int result = db.Select("SELECT meaning                        " +
                                                "  FROM lookups                        " +
                                                " WHERE lookup_type ='PRICING-PAINT'   " +
                                                "   AND enabled_flag = 'Y'             " +
                                                "   AND start_date_active <= GETDATE() " +
                                                " ORDER BY DISPLAY_SEQUENCE");
                if (result == 0)
                {
                    db.get_SqlDataAdapter.Fill(paintTable);
                    //MessageBox.Show("Row Count: " + table.Rows.Count + " Column Count: " + table.Columns.Count);
                    return paintTable;
                } else
                {
                    return null;
                }
            }
        }

        //********************************************************RETRIEVE PRICE DATA FUNCTIONS**********************************************************************//
        //Handles Price of glass, Price of toughend glass (insource), Price of toughened glass (outsource)
        public Double retrieveGlassPrice(string material, string thickness, string toughen)
        {
            glassThicknessTable.Clear();

            int result = db.Select("SELECT attribute2, attribute3, attribute4 " +
                                            "  FROM lookups " +
                                            " WHERE lookup_type = 'PRICING'" +
                                            "   AND MEANING =  '" + material + "'" +
                                            "   AND attribute1 = '" + thickness + "'" +
                                            "   AND enabled_flag = 'Y'" +
                                            "   AND start_date_active <= GETDATE()" +
                                            " ORDER BY display_sequence");
            if (result == 0)
            {
                db.get_SqlDataAdapter.Fill(glassThicknessTable);
                /*MessageBox.Show("materials: " + name + " Thickness: " + thickness + " |Loosen | " + glass_price_table.Rows[0][0] + Environment.NewLine +
                    " |Cut to size | " + glass_price_table.Rows[0][1] + Environment.NewLine +
                    " |Toughen | " + glass_price_table.Rows[0][2] + Environment.NewLine );   */
                double price = 0.0;
                Object temp;
                if (toughen == "")
                {
                    temp = glassThicknessTable.Rows[0][1];
                    if(temp != DBNull.Value)
                    {
                        price = Convert.ToDouble(temp);
                    }
                } else if(toughen == "outsource")
                {
                    temp = glassThicknessTable.Rows[0][2];
                    if(temp != DBNull.Value)
                    {
                        price = Convert.ToDouble(temp)*1.5;
                    }
                } else if (toughen == "insource")
                {
                    temp = glassThicknessTable.Rows[0][2];
                    if (temp != DBNull.Value)
                    {
                        price = Convert.ToDouble(temp);
                    }
                } else
                {
                    price = 0.0;
                }
                return price;
            } else
            {
                return 0.0;
            }
        }
        public Double retrievePaintPrice(string paintType)
        {
            paintPriceTable.Clear();

            int result = db.Select("SELECT attribute1 " +
                                            "  FROM lookups " +
                                            " WHERE lookup_type = 'PRICING-PAINT'" +
                                            "   AND MEANING =  '" + paintType + "'" +
                                            "   AND enabled_flag = 'Y'" +
                                            "   AND start_date_active <= GETDATE()" +
                                            " ORDER BY display_sequence");
            if (result == 0)
            {
                db.get_SqlDataAdapter.Fill(paintPriceTable);
                //MessageBox.Show(" |Price ($)|" + paint_price_table.Rows[0][0] + Environment.NewLine);
                double price = 0.0;
                price = Convert.ToDouble(paintPriceTable.Rows[0][0]);
                return price;
            } else
            {
                return 0.0;
            }
        }
        public Double retrievePolishingPrice(string type)
        {
            polishPriceTable.Clear();

            int result = db.Select("SELECT attribute1,attribute2 " +
                                            "  FROM lookups " +
                                            " WHERE lookup_type = 'PRICING-PROCESS'" +
                                            "   AND MEANING =  'Polish'" +
                                            "   AND enabled_flag = 'Y'" +
                                            "   AND start_date_active <= GETDATE()" +
                                            " ORDER BY display_sequence");
            if (result == 0)
            {
                db.get_SqlDataAdapter.Fill(polishPriceTable);
                //MessageBox.Show(" |In house - Price ($)|" + polish_price_table.Rows[0][0] + Environment.NewLine + " |Out house Price ($)|" + polish_price_table.Rows[0][1] + Environment.NewLine);
                double price = 0.0;
                if(type == "outsource")
                {
                    price = Convert.ToDouble(polishPriceTable.Rows[0][1]);
                } else if(type == "insource")
                {
                    price = Convert.ToDouble(polishPriceTable.Rows[0][0]);
                } else
                {
                    price = 0.0;
                }
                return price;
            }
            else
            {
                return 0.0;
            }
        }
        public Double retrieveLaminatePrice(string type)
        {
            laminatePriceTable.Clear();

            int result = db.Select("SELECT attribute1,attribute2 " +
                                            "  FROM lookups " +
                                            " WHERE lookup_type = 'PRICING-PROCESS'" +
                                            "   AND MEANING =  'Laminate'" +
                                            "   AND enabled_flag = 'Y'" +
                                            "   AND start_date_active <= GETDATE()" +
                                            " ORDER BY display_sequence");
            if (result == 0)
            {
                db.get_SqlDataAdapter.Fill(laminatePriceTable);
                //MessageBox.Show(" |In house - Price ($)|" + laminate_price_table.Rows[0][0] + Environment.NewLine + " |Out house Price ($)|" + laminate_price_table.Rows[0][1] + Environment.NewLine);
                double price = 0.0;
                if (type == "outsource")
                {
                    price = Convert.ToDouble(laminatePriceTable.Rows[0][1]);
                } else if(type == "insource")
                {
                    price = Convert.ToDouble(laminatePriceTable.Rows[0][0]);
                } else
                {
                    price = 0.0;
                }
                return price;
            }
            else
            {
                return 0.0;
            }
        }
        public Double retrieveBendPrice(string type)
        {
            bendPriceTable.Clear();

            int result = db.Select("SELECT attribute1,attribute2 " +
                                            "  FROM lookups " +
                                            " WHERE lookup_type = 'PRICING-PROCESS'" +
                                            "   AND MEANING =  'Bend'" +
                                            "   AND enabled_flag = 'Y'" +
                                            "   AND start_date_active <= GETDATE()" +
                                            " ORDER BY display_sequence");
            if (result == 0)
            {
                db.get_SqlDataAdapter.Fill(bendPriceTable);
                //MessageBox.Show(" |In house - Price ($)|" + bend_price_table.Rows[0][0] + Environment.NewLine + " |Out house Price ($)|" + bend_price_table.Rows[0][1] + Environment.NewLine);
                double price = 0.0;
                if(type == "outsource")
                {
                    price = Convert.ToDouble(bendPriceTable.Rows[0][1]);
                } else if(type == "insource")
                {
                    price = Convert.ToDouble(bendPriceTable.Rows[0][0]);
                } else
                {
                    price = 0.0;
                }
                return price;
            }
            else
            {
                return 0.0;
            }
        }
        public Double retrieveSandblastPrice(string type)
        {
            sandblastPriceTable.Clear();

            int result = db.Select("SELECT attribute1,attribute2 " +
                                            "  FROM lookups " +
                                            " WHERE lookup_type = 'PRICING-PROCESS'" +
                                            "   AND MEANING =  'Sandblast'" +
                                            "   AND enabled_flag = 'Y'" +
                                            "   AND start_date_active <= GETDATE()" +
                                            " ORDER BY display_sequence");
            if (result == 0)
            {
                db.get_SqlDataAdapter.Fill(sandblastPriceTable);
                //MessageBox.Show(" |In house - Price ($)|" + sandblast_price_table.Rows[0][0] + Environment.NewLine + " |Out house Price ($)|" + sandblast_price_table.Rows[0][1] + Environment.NewLine);
                double price = 0.0;
                if (type == "outsource")
                {
                    price = Convert.ToDouble(sandblastPriceTable.Rows[0][1]);
                } else if(type == "insource")
                {
                    price = Convert.ToDouble(sandblastPriceTable.Rows[0][0]);
                } else
                {
                    price = 0.0;
                }
                return price;
            }
            else
            {
                return 0.0;
            }
        }
        public Double retrieveLaserPrice(string type)
        {
            laserPriceTable.Clear();

            int result = db.Select("SELECT attribute1,attribute2 " +
                                            "  FROM lookups " +
                                            " WHERE lookup_type = 'PRICING-PROCESS'" +
                                            "   AND MEANING =  'Laser'" +
                                            "   AND enabled_flag = 'Y'" +
                                            "   AND start_date_active <= GETDATE()" +
                                            " ORDER BY display_sequence");
            if (result == 0)
            {
                db.get_SqlDataAdapter.Fill(laserPriceTable);
                //MessageBox.Show(" |In house - Price ($)|" + laser_price_table.Rows[0][0] + Environment.NewLine + " |Out house Price ($)|" + laser_price_table.Rows[0][1] + Environment.NewLine);
                double price = 0.0;
                if(type == "simple")
                {
                    price = Convert.ToDouble(laserPriceTable.Rows[0][0]);
                } else if(type == "complex")
                {
                    price = Convert.ToDouble(laserPriceTable.Rows[0][1]);
                } else
                {
                    price = 0.0;
                }
                return price;
            }
            else
            {
                return 0.0;
            }
        }
        public Double retrievePrintPrice(string type)
        {
            double price = 0.0;
            if(type == "small")
            {
                price = smallPrintPrice;
            } else if(type == "large")
            {
                price = largePrintPrice;
            } else
            {
                price = 0.0;
            }
            return price;
        }


        //********************************************************GET QUOTE NUMBER**********************************************************************//
        private string quoteNumber = "";
        public string getQuoteNumber
        {
            get
            {
                return quoteNumber;
            }
            set
            {
                quoteNumber = value;
            }
        }

        public string retrieveQuoteNumber
        {
            get
            {
                // Determine the Current Year prefix from the database
                DataTable tempTable1 = new DataTable();
                DataTable tempTable2 = new DataTable();
                string year;
                string quoteId;

                db.Select("SELECT attribute1 " +
                                 "  FROM [dbo].[lookups] " +
                                 " WHERE [LOOKUP_TYPE] = 'CURRENT_YEAR' " +
                                 "   AND [LOOKUP_CODE] = 'YEAR' " +
                                 "   AND [ENABLED_FLAG] = 'Y' " +
                                 "   AND [START_DATE_ACTIVE] <= GETDATE()");
                db.get_SqlDataAdapter.Fill(tempTable1);

                year = Convert.ToString(tempTable1.Rows[0][0]);

                // Get the System Number for use throughout the Quotation/Job modules
                db.Select("SELECT MAX(quotation_id) FROM [quotation_headers]");
                db.get_SqlDataAdapter.Fill(tempTable2);

                quoteId = Convert.ToString(tempTable2.Rows[0][0]);
                quoteNumber = year + "-0" + quoteId;
                return quoteNumber;
            }
        }

        //********************************************************HANDLE ATTACHMENT LIST**********************************************************************//
        //Used for storing the file name and path when btn_quotation_attach or btn_quotation_delete_attach is used
        private Dictionary<string, string> fileAttachmentList = new Dictionary<string, string>(); //fileName, filePath
        public Dictionary<string, string>  getFileAttachmentList
        {
            get
            {
                return fileAttachmentList;
            }
            set
            {
                fileAttachmentList = value;
            }
        }
        public bool checkAttachmentsExist
        {
            get
            {
                if(fileAttachmentList.Count > 0)
                {
                    return true;
                } else
                {
                    return false;
                }
            }
        }
        public void saveAttachments(string quotePath)
        {
            string attachmentPath = Path.Combine(quotePath, "Attachments");
            //Check if a the Attachments directory already exists
            if (!Directory.Exists(attachmentPath))
            {
                Console.WriteLine("Create a new Attachments folder:" + attachmentPath);
                Directory.CreateDirectory(attachmentPath);
            }
            else
            {
                Console.WriteLine("Attachments folder already exists:" + attachmentPath);
            }
            //Copy a file/s from the current location to a new location
            string sourcefile = ""; //Path of where the file currently resides
            string targetfile = ""; //New path of the destination

            //Search through file list for all file attachment paths
            foreach (var file in getFileAttachmentList)
            {
                //MessageBox.Show("Item: " + " Key - " + file.Key + " Value - " + file.Value);
                sourcefile = file.Value; //Assign file path to variable
                targetfile = Path.Combine(attachmentPath, file.Key); //Assign file name to variable

                if (!File.Exists(targetfile))
                {
                    //MessageBox.Show("Copy the source file into a new destination ");
                    //MessageBox.Show("sourcefile: " + sourcefile +  " targetfile: " + targetfile);
                    File.Copy(sourcefile, targetfile);
                } else
                {
                    Console.WriteLine("File already exists");
                }
            }
        }

        //********************************************************HANDLE ATTACHED NOTES**********************************************************************//
        private string notes = "";
        public string getNotes
        {
            get
            {
                return notes;
            }
            set
            {
                notes = value;
            }
        }
        public bool checkNotesExist
        {
            get
            {
                if(notes == "")
                {
                    return false;
                } else
                {
                    return true;
                }
            }
        }
        public void saveNotes(string quotePath)
        {
            //Check if any notes exist
            string notePath = Path.Combine(quotePath, "Notes");
            //Check if a the Notes directory already exists
            if (!Directory.Exists(notePath))
            {
                Console.WriteLine("Create a new Notes folder:" + notePath);
                Directory.CreateDirectory(notePath);
            }
            else
            {
                Console.WriteLine("Notes folder already exists:" + notePath);
            }
            string file = Path.Combine(notePath, "Notes.txt");
            File.AppendAllText(file, notes);
        }



        //********************************************************HANDLE CREATING THE QUOTATION INVOICE**********************************************************************//
        private string templateFilePath = "";
        public string getTemplateFilePath
        {
            get
            {
                return templateFilePath;
            }
            set
            {
                templateFilePath = value;
            }
        }
        public void CreateQuote(string quotePath)
        {
            PdfDocument pdfDocument = new PdfDocument();

            List<string> itemStringList = new List<string>();
            List<string> quantityStringList = new List<string>();
            List<string> unitCostStringList = new List<string>();
            List<string> subTotalStringList = new List<string>();
            List<int> newLineCountList = new List<int>();
            int itemCount = quotationModel.getQuote.getItemCount;

            string company = quotationModel.getQuote.getCompanyName;
            string name = quotationModel.getQuote.getFirstLastName;
            string address = quotationModel.getQuote.getAddress;
            string quote = retrieveQuoteNumber;
            string date = DateTime.Now.ToShortDateString();
            string delivery = quotationModel.getQuote.getDeliveryCost();
            string installation = quotationModel.getQuote.getInstallationCost();
            string totalPrice = Convert.ToString( Math.Round( quotationModel.getQuote.getTotalPrice, 2) );
            string gstAmount = Convert.ToString( Math.Round( quotationModel.getQuote.getGstAmount, 2) );
            string totalAndGst = Convert.ToString( Math.Round( quotationModel.getQuote.getQuoteTotalAndGstPrice, 2) );

            //Check if a Quote folder already exists
            string quotePdfPath = Path.Combine(quotePath, "Quotes");
            
            //Check if a the Notes directory already exists
            if (!Directory.Exists(quotePdfPath))
            {
                Console.WriteLine("Create a new Quotes folder:" + quotePdfPath);
                Directory.CreateDirectory(quotePdfPath);
            }
            else
            {
                Console.WriteLine("Quote folder already exists:" + quotePdfPath);
            }
            //Assemble the file name and file path
            string file = Path.Combine(quotePdfPath, quote + "-Quote-" + name + ".pdf");

            //Obtain the pdf file path and the template file path
            pdfDocument.getFileName = templateFilePath; //templateFilePath;
            pdfDocument.getSaveToLocation = file; ; //file;

            //Loop through each item added in the Quote and add any processes etc. , quantites, subTotals and unit prices
            for (int i = 0; i < itemCount; i++)
            {
                int k = i + 1;
                itemStringList.Add("----- Item " + k + "-----" + Environment.NewLine + quotationModel.getQuote.getItem(i).itemSummary + Environment.NewLine);
                quantityStringList.Add(Convert.ToString(quotationModel.getQuote.getItem(i).materials.Quantity));
                unitCostStringList.Add(Convert.ToString( Math.Round( quotationModel.getQuote.getItem(i).GetCustomerPrice / quotationModel.getQuote.getItem(i).materials.Quantity, 2) ));
                subTotalStringList.Add(Convert.ToString( Math.Round( quotationModel.getQuote.getItem(i).GetCustomerPrice, 2) ));
                newLineCountList.Add(quotationModel.getQuote.getItem(i).getNewLineCount);
            }

            /*
            Console.WriteLine("Company name: " + company);
            Console.WriteLine("First and last name: " + name);
            Console.WriteLine("Address: " + address);
            Console.WriteLine("Quote number: " + retrieveQuoteNumber);
            Console.WriteLine("Date: " + date);
            for (int i = 0; i < itemCount; i++)
            {
                Console.WriteLine("Item " + i + Environment.NewLine + quotationModel.getQuote.getItem(i).itemSummary);
                Console.WriteLine("Quantity [" + i + "] " + quotationModel.getQuote.getItem(i).materials.Quantity);
                Console.WriteLine("Each [" + i + "] " + quotationModel.getQuote.getItem(i).GetCustomerPrice / quotationModel.getQuote.getItem(i).materials.Quantity);
                Console.WriteLine("Subtotal [" + i + "] " + quotationModel.getQuote.getItem(i).GetCustomerPrice);
                Console.WriteLine("newline: " + quotationModel.getQuote.getItem(i).getNewLineCount);
            }*/
            //Console.WriteLine("Delivery cost: " + delivery);
            //Console.WriteLine("Installation cost: " + installation);
            //Console.WriteLine("Total price: " + totalPrice);
            //Console.WriteLine("Gst amount: " + gstAmount);
            //Console.WriteLine("Job total: " + totalAndGst);

            //Send all quote information to create this pdf
            pdfDocument.CreatePdfDocument(
                company, name, address, quote, date, itemCount, 
                itemStringList, quantityStringList, unitCostStringList, subTotalStringList, newLineCountList, 
                delivery, installation, totalPrice, gstAmount, totalAndGst
                );
        }


        //********************************************************HANDLE THE DATA MODEL**********************************************************************//
        QuotationModel quotationModel = new QuotationModel();

        public void addNewItem()
        {
            quotationModel.getQuote.addItem();
        }
        public void removeItem(int index)
        {
            quotationModel.getQuote.removeItem(index);
        }
        public bool rearrangeItems
        {
            get
            {
                if (quotationModel.getQuote.rearrangeItems())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public int getItemCount
        {
            get
            {
                return quotationModel.getQuote.getItemCount;
            }
        }


        //***************************ITEM CODE FUNCTIONALITY**********************//

        /************ ITEM PRICES CODE ************/
        public double getItemCostPrice(int index)
        {
            double price = quotationModel.getQuote.getItem(index).GetCostPrice;
            return price;
        }
        public double getItemJobPrice(int index)
        {
            double price = quotationModel.getQuote.getItem(index).GetJobPrice;
            return price;
        }
        public double getItemCustomerPrice(int index)
        {
            double price = quotationModel.getQuote.getItem(index).GetCustomerPrice;
            return price;
        }
        public void addItemPrice(int index, string source, double price)
        {
            quotationModel.getQuote.getItem(index).AddPrice(source, price);
        }
        public string findItemName(int index, string name)
        {
            string result = quotationModel.getQuote.getItem(index).FindName(name);
            return result;
        }
        public void removeItemPrice(int index, string name)
        {
            quotationModel.getQuote.getItem(index).RemovePrice(name);
        }

        /************ HEIGHT CODE ************/
        public string getItemDimensionHeight(int index)
        {
            return quotationModel.getQuote.getItem(index).dimensions.Height;
        }
        public void setItemDimensionHeight(int index, string height)
        {
            quotationModel.getQuote.getItem(index).dimensions.Height = height;
        }

        /************ WIDTH CODE ************/
        public string getItemDimensionWidth(int index)
        {
            return quotationModel.getQuote.getItem(index).dimensions.Width;
        }
        public void setItemDimensionWidth(int index, string width)
        {
            quotationModel.getQuote.getItem(index).dimensions.Width = width;
        }

        /************ RADIUS CODE ************/
        public string getItemDimensionRadius(int index)
        {
            return quotationModel.getQuote.getItem(index).dimensions.Radius;
        }
        public void setItemDimensionRadius(int index, string radius)
        {
            quotationModel.getQuote.getItem(index).dimensions.Radius = radius;
        }

        /************ PERIMETER CODE ************/
        public string getItemDimensionPerimeter(int index)
        {
            return quotationModel.getQuote.getItem(index).dimensions.Perimeter;
        }
        public void setItemDimensionPerimeter(int index, string perimeter)
        {
            quotationModel.getQuote.getItem(index).dimensions.Perimeter = perimeter;
        }

        /************ AREA CODE ************/
        public string getItemDimensionArea(int index)
        {
            return quotationModel.getQuote.getItem(index).dimensions.Area;
        }
        public void setItemDimensionArea(int index, string area)
        {
            quotationModel.getQuote.getItem(index).dimensions.Area = area;
        }



        /************ PRICE FACTOR CODE ************/
        public double getItemPriceFactor(int index)
        {
            return quotationModel.getQuote.getItem(index).PriceFactor;
        }
        public void setItemPriceFactor(int index, double factor)
        {
            quotationModel.getQuote.getItem(index).PriceFactor = factor;
        }

        /************ SLUMP CODE ************/
        public string getItemSlumpName(int index)
        {
            return quotationModel.getQuote.getItem(index).slump.Name;
        }
        public string getItemSlumpTexture(int index)
        {
            return quotationModel.getQuote.getItem(index).slump.Texture;
        }
        public string getItemSlumpCustomDescription(int index)
        {
            return quotationModel.getQuote.getItem(index).slump.CustomDescription;
        }
        public void setItemSlumpName(int index, string name)
        {
            quotationModel.getQuote.getItem(index).slump.Name = name;
        }
        public void setItemSlumpTexture(int index, string texture)
        {
            quotationModel.getQuote.getItem(index).slump.Texture = texture;
        }
        public void setItemSlumpCustomDescrition(int index, string desc)
        {
            quotationModel.getQuote.getItem(index).slump.CustomDescription = desc;
        }

        /************ LAMINATE CODE ************/
        public string getItemLaminateName(int index)
        {
            return quotationModel.getQuote.getItem(index).laminate.Name;
        }
        public string getItemLaminateSource(int index)
        {
            return quotationModel.getQuote.getItem(index).laminate.Source;
        }
        public void setItemLaminateName(int index, string name)
        {
            quotationModel.getQuote.getItem(index).laminate.Name = name;
        }
        public void setItemLaminateSource(int index, string source)
        {
            quotationModel.getQuote.getItem(index).laminate.Source = source;
        }

        /************ BEND CODE ************/
        public string getItemBendName(int index)
        {
            return quotationModel.getQuote.getItem(index).bend.Name;
        }
        public string getItemBendSource(int index)
        {
            return quotationModel.getQuote.getItem(index).bend.Source;
        }
        public void setItemBendName(int index, string name)
        {
            quotationModel.getQuote.getItem(index).bend.Name = name;
        }
        public void setItemBendSource(int index, string source)
        {
            quotationModel.getQuote.getItem(index).bend.Source = source;
        }

        /************ TOUGHEN CODE ************/
        public string getItemToughenName(int index)
        {
            return quotationModel.getQuote.getItem(index).toughen.Name;
        }
        public string getItemToughenSource(int index)
        {
            return quotationModel.getQuote.getItem(index).toughen.Source;
        }
        public void setItemToughenName(int index, string name)
        {
            quotationModel.getQuote.getItem(index).toughen.Name = name;
        }
        public void setItemToughenSource(int index, string source)
        {
            quotationModel.getQuote.getItem(index).toughen.Source = source;
        }

        /************ LASER CODE ************/
        public string getItemLaserName(int index)
        {
            return quotationModel.getQuote.getItem(index).laser.Name;
        }
        public string getItemLaserType(int index)
        {
            return quotationModel.getQuote.getItem(index).laser.Type;
        }
        public void setItemLaserName(int index, string name)
        {
            quotationModel.getQuote.getItem(index).laser.Name = name;
        }
        public void setItemLaserType(int index, string type)
        {
            quotationModel.getQuote.getItem(index).laser.Type = type;
        }

        /************ SANDBLAST CODE ************/
        public string getItemSandblastName(int index)
        {
            return quotationModel.getQuote.getItem(index).sandblast.Name;
        }
        public string getItemSandblastSource(int index)
        {
            return quotationModel.getQuote.getItem(index).sandblast.Source;
        }
        public void setItemSandblastName(int index, string name)
        {
            quotationModel.getQuote.getItem(index).sandblast.Name = name;
        }
        public void setItemSandblastSource(int index, string source)
        {
            quotationModel.getQuote.getItem(index).sandblast.Source = source;
        }

        /************ POLISHING CODE ************/
        public string getItemPolishingName(int index)
        {
            return quotationModel.getQuote.getItem(index).polishing.Name;
        }
        public string getItemPolishingSource(int index)
        {
            return quotationModel.getQuote.getItem(index).polishing.Source;
        }
        public void setItemPolishingName(int index, string name)
        {
            quotationModel.getQuote.getItem(index).polishing.Name = name;
        }
        public void setItemPolishingSource(int index, string source)
        {
            quotationModel.getQuote.getItem(index).polishing.Source = source;
        }

        /************ PRINT CODE ************/
        public string getItemPrintName(int index)
        {
            return quotationModel.getQuote.getItem(index).print.Name;
        }
        public string getItemPrintSize(int index)
        {
            return quotationModel.getQuote.getItem(index).print.Size;
        }
        public void setItemPrintName(int index, string name)
        {
            quotationModel.getQuote.getItem(index).print.Name = name;
        }
        public void setItemPrintSize(int index, string size)
        {
            quotationModel.getQuote.getItem(index).print.Size = size;
        }

        /************ PAINT CODE ************/
        public string getItemPaintType(int index)
        {
            return quotationModel.getQuote.getItem(index).paint.Type;
        }
        public string getItemPaintName(int index)
        {
            return quotationModel.getQuote.getItem(index).paint.Name;
        }
        public string getItemPaintColorCode(int index)
        {
            return quotationModel.getQuote.getItem(index).paint.ColorCode;
        }
        public void setItemPaintName(int index, string name)
        {
            quotationModel.getQuote.getItem(index).paint.Name = name;
        }
        public void setItemPaintColorCode(int index, string colorCode)
        {
            quotationModel.getQuote.getItem(index).paint.ColorCode = colorCode;
        }
        public void setItemPaintType(int index, string type)
        {
            quotationModel.getQuote.getItem(index).paint.Type = type;
        }

        /************ WATERJET CODE ************/
        public string addItemWaterJetCut(int index)
        {
            string selection = quotationModel.getQuote.getItem(index).waterJet.getWaterjetCutSelection;
            string thickness = quotationModel.getQuote.getItem(index).materials.Thickness;
            string result = quotationModel.getQuote.getItem(index).waterJet.AddWaterjetCuts(selection, thickness);
            return result;
        }
        public string deleteItemWaterJetCut(int index)
        {
            string selection = quotationModel.getQuote.getItem(index).waterJet.getWaterjetCutSelection;
            string result = quotationModel.getQuote.getItem(index).waterJet.DeleteWaterjetCuts(selection);
            return result;
        }
        public string getItemWaterJetName(int index)
        {
            return quotationModel.getQuote.getItem(index).waterJet.Name;
        }
        public string getItemWaterJetSelection(int index)
        {
            return quotationModel.getQuote.getItem(index).waterJet.getWaterjetCutSelection;
        }
        public void setItemWaterJetName(int index, string name)
        {
            quotationModel.getQuote.getItem(index).waterJet.Name = name;
        }
        public void setItemWaterJetSetup(int index, string area)
        {
            
            if(String.IsNullOrEmpty(area))
            {
                Console.WriteLine("setItemWaterJetSetup: " + area + " clear ");
                quotationModel.getQuote.getItem(index).waterJet.clearWaterJetSetupPrice();
            } else
            {
                Console.WriteLine("setItemWaterJetSetup: " + area + " set price ");
                quotationModel.getQuote.getItem(index).waterJet.setWaterJetSetupPrice(area);
            }
        }
        public void setItemWaterJetCutSelection(int index, string selection)
        {
            quotationModel.getQuote.getItem(index).waterJet.getWaterjetCutSelection = selection;
        }
        public int getItemWaterJetCutListCount(int index)
        {
            return quotationModel.getQuote.getItem(index).waterJet.getListOfWaterJetCutCounts;
        }
        public string getItemWaterJetCutType(int index, int i)
        {
            return quotationModel.getQuote.getItem(index).waterJet.getWaterJetCutType(i);
        }

        /************ CUSTOM DESIGN CODE ************/
        public string getItemCustomDesign(int index)
        {
            return quotationModel.getQuote.getItem(index).customDesign.Name;
        }
        public void setItemCustomDesign(int index, string name)
        {
            quotationModel.getQuote.getItem(index).customDesign.Name = name;
        }
        public void addItemCustomDesignItem(int index, int qty, string material, double price, double hours)
        {
            quotationModel.getQuote.getItem(index).customDesign.addCustomItem(qty, material, price, hours);
        }
        public void deleteItemCustomDesignItem(int index, int removeIndex)
        {
            quotationModel.getQuote.getItem(index).customDesign.deleteCustomItem(removeIndex);
        }
        public void clearItemCustomDesignItem(int index)
        {
            quotationModel.getQuote.getItem(index).customDesign.clearCustomDesignItems();
        }

        public string getItemCustomItemQuantity(int index, int i)
        {
            return quotationModel.getQuote.getItem(index).customDesign.getCustomItemQuantity(i);
        }
        public string getItemCustomItemMaterial(int index, int i)
        {
            return quotationModel.getQuote.getItem(index).customDesign.getCustomItemMaterial(i);
        }
        public string getItemCustomItemPrice(int index, int i)
        {
            return quotationModel.getQuote.getItem(index).customDesign.getCustomItemPrice(i);
        }
        public string getItemCustomItemHoursWorked(int index, int i)
        {
            return quotationModel.getQuote.getItem(index).customDesign.getCustomItemHoursWorked(i);
        }
 
        public int getItemCustomDesignItemList (int index)
        {
            return quotationModel.getQuote.getItem(index).customDesign.getCustomItemListCount;   
        }

        /************ MATERIALS CODE ************/
        public string getItemMaterialName(int index)
        {
            return quotationModel.getQuote.getItem(index).materials.Name;
        }
        public void setItemMaterialName(int index, string material)
        {
            quotationModel.getQuote.getItem(index).materials.Name = material;
        }

        public string getItemMaterialThickness(int index)
        {
            return quotationModel.getQuote.getItem(index).materials.Thickness;
        }
        public void setItemMaterialThickness(int index, string thickness)
        {
            quotationModel.getQuote.getItem(index).materials.Thickness = thickness;
        }

        public string getItemMaterialDescription(int index)
        {
            return quotationModel.getQuote.getItem(index).materials.Description;
        }
        public void setItemMaterialDescription(int index, string desc)
        {
            quotationModel.getQuote.getItem(index).materials.Description = desc;
        }

        public string getItemQuantity(int index)
        {
            return Convert.ToString(quotationModel.getQuote.getItem(index).materials.Quantity);
        }
        public void setItemQuantity(int index, string text)
        {
            if (!String.IsNullOrEmpty(text))
            {
                int qty = Convert.ToInt16(text);
                if (qty != 0)
                {
                    quotationModel.getQuote.getItem(index).materials.Quantity = qty;
                }
                else
                {
                    quotationModel.getQuote.getItem(index).materials.Quantity = 0;
                }

            }
            else
            {
                quotationModel.getQuote.getItem(index).materials.Quantity = 0;
            }

        }

        /************ PRICE CODE ************/
        public void setItemGlassPrice(int index)
        {
            string materialName = quotationModel.getQuote.getItem(index).materials.Name;
            string materialThickness = quotationModel.getQuote.getItem(index).materials.Thickness;
            quotationModel.getQuote.getItem(index).glass_price = retrieveGlassPrice(materialName, materialThickness, "");
        }
        public void clearItemGlassPrice(int index, double price)
        {
            quotationModel.getQuote.getItem(index).glass_price = price;
        }

        public void setItemToughenPrice(int index, string source)
        {
            string materialName = quotationModel.getQuote.getItem(index).materials.Name;
            string materialThickness = quotationModel.getQuote.getItem(index).materials.Thickness;
            quotationModel.getQuote.getItem(index).toughen_price = retrieveGlassPrice(materialName, materialThickness, source);
        }
        public void clearToughenPrice(int index, double price)
        {
            quotationModel.getQuote.getItem(index).toughen_price = price;
        }

        public string getItemOverrideDollarSign(int index)
        {
            return quotationModel.getQuote.getItem(index).OverrideDollarSign;
        }
        public void setItemOverrideDollarSign(int index, string sign)
        {
            quotationModel.getQuote.getItem(index).OverrideDollarSign = sign;
        }

        public string getItemOverridePercentSign(int index)
        {
            return quotationModel.getQuote.getItem(index).OverridePercentSign;
        }
        public void setItemOverridePercentSign(int index, string sign)
        {
            quotationModel.getQuote.getItem(index).OverridePercentSign = sign;
        }

        public double getItemOverrideDollar(int index)
        {
            return quotationModel.getQuote.getItem(index).OverrideDollar;
        }
        public void setItemOverrideDollar(int index, double dollar)
        {
            quotationModel.getQuote.getItem(index).OverrideDollar = dollar;
        }

        public double getItemOverridePercent(int index)
        {
            return quotationModel.getQuote.getItem(index).OverridePercent;
        }
        public void setItemOverridePercent(int index, double percent)
        {
            quotationModel.getQuote.getItem(index).OverridePercent = percent;
        }


                                        //***************************QUOTE CODE FUNCTIONALITY**********************//


        /************ DETAILS CODE ************/
        public void setQuoteCompanyName(string company)
        {
            quotationModel.getQuote.getCompanyName = company;
        }
        public string getQuoteCompanyName
        {
            get
            {
                return quotationModel.getQuote.getCompanyName;
            }
        }

        public void setQuoteFirstLastName(string name)
        {
            quotationModel.getQuote.getFirstLastName = name;
        }
        public string getQuoteFirstLastName
        {
            get
            {
                return quotationModel.getQuote.getFirstLastName;
            }
        }

        public void setQuoteAddress(string address)
        {
            quotationModel.getQuote.getAddress = address;
        }

        /************ INSTALLATION CODE ************/
        public string getQuoteInstallationCost
        {
            get
            {
                return quotationModel.getQuote.getInstallationCost();
            }
            
        }

        public void setQuoteInstallationHours(string hours)
        {
            quotationModel.getQuote.setInstallationHours(hours);
        }

        /************ DELIVERY CODE ************/
        public string getQuoteDeliveryCost
        {
            get
            {
                return quotationModel.getQuote.getDeliveryCost();
            }
            
        }

        public void setQuoteDeliveryCost(string distance)
        {
            quotationModel.getQuote.setDeliveryDistance(distance);
        }

        /************ PRICE CODE ************/
        public double getQuoteTotalPrice
        {
            get
            {
                return quotationModel.getQuote.getTotalPrice;
            }
        }

        public double getQuoteGstAmount
        {
            get
            {
                return quotationModel.getQuote.getGstAmount;
            }
        }

        public double getQuoteTotalAndGstPrice
        {
            get
            {
                return quotationModel.getQuote.getQuoteTotalAndGstPrice;
            }
        }

        /************ PRICE CODE ************/
        public void setQuoteOverrideDollarSign(string sign)
        {
            quotationModel.getQuote.OverrideDollarSign = sign;
        }

        public void setQuoteOverridePercentSign(string sign)
        {
            quotationModel.getQuote.OverridePercentSign = sign;
        }

        public void setQuoteOverrideDollar(double dollar)
        {
            quotationModel.getQuote.OverrideDollar = dollar;
        }

        public void setQuoteOverridePercent(double percent)
        {
            quotationModel.getQuote.OverridePercent = percent;
        }

    }
}
