﻿namespace ProfileGlassHome
{
    partial class Quotation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnQuotationClose = new System.Windows.Forms.Button();
            this.tabcontrol_quotation = new System.Windows.Forms.TabControl();
            this.tabpage_quotation = new System.Windows.Forms.TabPage();
            this.pnl_quotation = new System.Windows.Forms.Panel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.label31 = new System.Windows.Forms.Label();
            this.txtMaterialCustomDescription = new System.Windows.Forms.TextBox();
            this.panel18 = new System.Windows.Forms.Panel();
            this.txtDimensionQuantity = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.cmbMaterialThickness = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.panel16 = new System.Windows.Forms.Panel();
            this.label30 = new System.Windows.Forms.Label();
            this.cmbMaterialType = new System.Windows.Forms.ComboBox();
            this.label33 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.txtDimensionArea = new System.Windows.Forms.TextBox();
            this.txtAreaOriginal = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.panel19 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.txtDimensionRadius = new System.Windows.Forms.TextBox();
            this.panel17 = new System.Windows.Forms.Panel();
            this.lblDimensionPerimeter = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.txtDimensionWidth = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDimensionHeight = new System.Windows.Forms.TextBox();
            this.grp_process = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.grpPaint = new System.Windows.Forms.GroupBox();
            this.label24 = new System.Windows.Forms.Label();
            this.chkPaint = new System.Windows.Forms.CheckBox();
            this.lblColourCode = new System.Windows.Forms.Label();
            this.richtxtPaintColorCode = new System.Windows.Forms.RichTextBox();
            this.cmbPaintType = new System.Windows.Forms.ComboBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.grpSlump = new System.Windows.Forms.GroupBox();
            this.richtxtSlumpCustomDescription = new System.Windows.Forms.RichTextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.chkSlump = new System.Windows.Forms.CheckBox();
            this.cmbSlumpTexture = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.grpPrint = new System.Windows.Forms.GroupBox();
            this.chkPrint = new System.Windows.Forms.CheckBox();
            this.rbPrintLarge = new System.Windows.Forms.RadioButton();
            this.rbPrintSmall = new System.Windows.Forms.RadioButton();
            this.panel6 = new System.Windows.Forms.Panel();
            this.grpLaser = new System.Windows.Forms.GroupBox();
            this.chkLaser = new System.Windows.Forms.CheckBox();
            this.rbLaserComplex = new System.Windows.Forms.RadioButton();
            this.rbLaserSimple = new System.Windows.Forms.RadioButton();
            this.panel5 = new System.Windows.Forms.Panel();
            this.grpPolishing = new System.Windows.Forms.GroupBox();
            this.chkPolishing = new System.Windows.Forms.CheckBox();
            this.rbPolishInsource = new System.Windows.Forms.RadioButton();
            this.rbPolishOutsource = new System.Windows.Forms.RadioButton();
            this.panel4 = new System.Windows.Forms.Panel();
            this.grpSandblast = new System.Windows.Forms.GroupBox();
            this.chkSandblast = new System.Windows.Forms.CheckBox();
            this.rbSandblastInsource = new System.Windows.Forms.RadioButton();
            this.rbSandblastOutsource = new System.Windows.Forms.RadioButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.grpBend = new System.Windows.Forms.GroupBox();
            this.chkBend = new System.Windows.Forms.CheckBox();
            this.rbBendInsource = new System.Windows.Forms.RadioButton();
            this.rbBendOutsource = new System.Windows.Forms.RadioButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.grpLaminate = new System.Windows.Forms.GroupBox();
            this.chkLaminate = new System.Windows.Forms.CheckBox();
            this.rbLaminateInsource = new System.Windows.Forms.RadioButton();
            this.rbLaminateOutsource = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.grpToughen = new System.Windows.Forms.GroupBox();
            this.chkToughen = new System.Windows.Forms.CheckBox();
            this.rbToughenInsource = new System.Windows.Forms.RadioButton();
            this.rbToughenOutsource = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.panel21 = new System.Windows.Forms.Panel();
            this.grpCustomDesign = new System.Windows.Forms.GroupBox();
            this.btnCustomDelete = new System.Windows.Forms.Button();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.panel26 = new System.Windows.Forms.Panel();
            this.txtCustomHours = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.panel25 = new System.Windows.Forms.Panel();
            this.txtCustomPrice = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.panel24 = new System.Windows.Forms.Panel();
            this.label35 = new System.Windows.Forms.Label();
            this.txtCustomMaterial = new System.Windows.Forms.TextBox();
            this.panel23 = new System.Windows.Forms.Panel();
            this.label25 = new System.Windows.Forms.Label();
            this.txtCustomQuantity = new System.Windows.Forms.TextBox();
            this.lstCustomDesign = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnCustomAdd = new System.Windows.Forms.Button();
            this.chkCustomDesign = new System.Windows.Forms.CheckBox();
            this.panel22 = new System.Windows.Forms.Panel();
            this.grpWaterJet = new System.Windows.Forms.GroupBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.lstWaterjetCutList = new System.Windows.Forms.ListBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.panel29 = new System.Windows.Forms.Panel();
            this.cmbGpoCutout = new System.Windows.Forms.ComboBox();
            this.label34 = new System.Windows.Forms.Label();
            this.panel28 = new System.Windows.Forms.Panel();
            this.cmbCutoutLength = new System.Windows.Forms.ComboBox();
            this.label32 = new System.Windows.Forms.Label();
            this.panel27 = new System.Windows.Forms.Panel();
            this.chkHoleDiameter = new System.Windows.Forms.CheckBox();
            this.btnWaterjetDelete = new System.Windows.Forms.Button();
            this.btnWaterjetAdd = new System.Windows.Forms.Button();
            this.chkWaterJet = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cmbItemOverridePercent = new System.Windows.Forms.ComboBox();
            this.cmbItemOverrideDollar = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.cmbPriceFactor = new System.Windows.Forms.ComboBox();
            this.lblItemCustomerPrice = new System.Windows.Forms.Label();
            this.txtItemOverridePercent = new System.Windows.Forms.TextBox();
            this.lblItemJobPrice = new System.Windows.Forms.Label();
            this.txtItemOverrideDollar = new System.Windows.Forms.TextBox();
            this.lblItemCostPrice = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.btnQuotationNewItem = new System.Windows.Forms.Button();
            this.btnQuotationDeleteItem = new System.Windows.Forms.Button();
            this.btnQuotationConfirm = new System.Windows.Forms.Button();
            this.btnQuotationDeleteAttach = new System.Windows.Forms.Button();
            this.ltbQuotationListFiles = new System.Windows.Forms.ListBox();
            this.btnQuotationAttach = new System.Windows.Forms.Button();
            this.btnQuotationSave = new System.Windows.Forms.Button();
            this.btnQuotationNote = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.lblGstAmount = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lblQuoteGstTotal = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lblQuoteTotal = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.cmbQuoteOverridePercent = new System.Windows.Forms.ComboBox();
            this.txtQuoteOverridePercent = new System.Windows.Forms.TextBox();
            this.cmbQuoteOverrideDollar = new System.Windows.Forms.ComboBox();
            this.txtQuoteOverrideDollar = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.lblQuoteDelivery = new System.Windows.Forms.Label();
            this.txtDeliveryDistance = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.lblInstallationCost = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtInstallationHrs = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtVersion = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtCreatedDate = new System.Windows.Forms.TextBox();
            this.txtQuoteNumber = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.txtCompanyName = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnBrowseTemplate = new System.Windows.Forms.Button();
            this.label40 = new System.Windows.Forms.Label();
            this.lblTemplateFileName = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.tabcontrol_quotation.SuspendLayout();
            this.tabpage_quotation.SuspendLayout();
            this.pnl_quotation.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.panel10.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel18.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel11.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel12.SuspendLayout();
            this.grp_process.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.panel9.SuspendLayout();
            this.grpPaint.SuspendLayout();
            this.panel8.SuspendLayout();
            this.grpSlump.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel7.SuspendLayout();
            this.grpPrint.SuspendLayout();
            this.panel6.SuspendLayout();
            this.grpLaser.SuspendLayout();
            this.panel5.SuspendLayout();
            this.grpPolishing.SuspendLayout();
            this.panel4.SuspendLayout();
            this.grpSandblast.SuspendLayout();
            this.panel3.SuspendLayout();
            this.grpBend.SuspendLayout();
            this.panel2.SuspendLayout();
            this.grpLaminate.SuspendLayout();
            this.panel1.SuspendLayout();
            this.grpToughen.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.panel21.SuspendLayout();
            this.grpCustomDesign.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.panel26.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panel24.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panel22.SuspendLayout();
            this.grpWaterJet.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.panel29.SuspendLayout();
            this.panel28.SuspendLayout();
            this.panel27.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnQuotationClose
            // 
            this.btnQuotationClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnQuotationClose.Location = new System.Drawing.Point(1011, 678);
            this.btnQuotationClose.Name = "btnQuotationClose";
            this.btnQuotationClose.Size = new System.Drawing.Size(75, 23);
            this.btnQuotationClose.TabIndex = 26;
            this.btnQuotationClose.Text = "Close";
            this.btnQuotationClose.UseVisualStyleBackColor = true;
            // 
            // tabcontrol_quotation
            // 
            this.tabcontrol_quotation.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabcontrol_quotation.Controls.Add(this.tabpage_quotation);
            this.tabcontrol_quotation.Location = new System.Drawing.Point(13, 143);
            this.tabcontrol_quotation.Name = "tabcontrol_quotation";
            this.tabcontrol_quotation.SelectedIndex = 0;
            this.tabcontrol_quotation.Size = new System.Drawing.Size(1095, 527);
            this.tabcontrol_quotation.TabIndex = 20;
            this.tabcontrol_quotation.TabStop = false;
            this.tabcontrol_quotation.SelectedIndexChanged += new System.EventHandler(this.tabcontrol_quotation_SelectedIndexChanged);
            // 
            // tabpage_quotation
            // 
            this.tabpage_quotation.Controls.Add(this.pnl_quotation);
            this.tabpage_quotation.Location = new System.Drawing.Point(4, 22);
            this.tabpage_quotation.Name = "tabpage_quotation";
            this.tabpage_quotation.Padding = new System.Windows.Forms.Padding(3);
            this.tabpage_quotation.Size = new System.Drawing.Size(1087, 501);
            this.tabpage_quotation.TabIndex = 0;
            this.tabpage_quotation.Text = "Item 1";
            // 
            // pnl_quotation
            // 
            this.pnl_quotation.BackColor = System.Drawing.SystemColors.Control;
            this.pnl_quotation.Controls.Add(this.tableLayoutPanel3);
            this.pnl_quotation.Controls.Add(this.grp_process);
            this.pnl_quotation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnl_quotation.Location = new System.Drawing.Point(3, 3);
            this.pnl_quotation.Name = "pnl_quotation";
            this.pnl_quotation.Size = new System.Drawing.Size(1081, 495);
            this.pnl_quotation.TabIndex = 20;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.panel10, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.panel11, 1, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(9, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1066, 124);
            this.tableLayoutPanel3.TabIndex = 24;
            // 
            // panel10
            // 
            this.panel10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel10.Controls.Add(this.groupBox4);
            this.panel10.Location = new System.Drawing.Point(3, 3);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(527, 118);
            this.panel10.TabIndex = 22;
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.tableLayoutPanel11);
            this.groupBox4.Controls.Add(this.tableLayoutPanel5);
            this.groupBox4.Controls.Add(this.label33);
            this.groupBox4.Location = new System.Drawing.Point(3, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(521, 112);
            this.groupBox4.TabIndex = 27;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Materials";
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 2;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel11.Controls.Add(this.panel15, 0, 0);
            this.tableLayoutPanel11.Controls.Add(this.panel18, 1, 0);
            this.tableLayoutPanel11.Location = new System.Drawing.Point(6, 65);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 1;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(509, 41);
            this.tableLayoutPanel11.TabIndex = 47;
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.label31);
            this.panel15.Controls.Add(this.txtMaterialCustomDescription);
            this.panel15.Location = new System.Drawing.Point(3, 3);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(350, 35);
            this.panel15.TabIndex = 15;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(8, 12);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(99, 13);
            this.label31.TabIndex = 9;
            this.label31.Text = "Custom description:";
            // 
            // txtMaterialCustomDescription
            // 
            this.txtMaterialCustomDescription.Location = new System.Drawing.Point(113, 9);
            this.txtMaterialCustomDescription.Name = "txtMaterialCustomDescription";
            this.txtMaterialCustomDescription.Size = new System.Drawing.Size(234, 20);
            this.txtMaterialCustomDescription.TabIndex = 29;
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.txtDimensionQuantity);
            this.panel18.Controls.Add(this.label37);
            this.panel18.Location = new System.Drawing.Point(359, 3);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(147, 35);
            this.panel18.TabIndex = 46;
            // 
            // txtDimensionQuantity
            // 
            this.txtDimensionQuantity.Location = new System.Drawing.Point(57, 9);
            this.txtDimensionQuantity.Name = "txtDimensionQuantity";
            this.txtDimensionQuantity.Size = new System.Drawing.Size(87, 20);
            this.txtDimensionQuantity.TabIndex = 33;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(3, 12);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(49, 13);
            this.label37.TabIndex = 16;
            this.label37.Text = "Quantity:";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Controls.Add(this.panel20, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.panel16, 0, 0);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(6, 15);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(509, 47);
            this.tableLayoutPanel5.TabIndex = 45;
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.cmbMaterialThickness);
            this.panel20.Controls.Add(this.label29);
            this.panel20.Location = new System.Drawing.Point(257, 3);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(226, 41);
            this.panel20.TabIndex = 17;
            // 
            // cmbMaterialThickness
            // 
            this.cmbMaterialThickness.FormattingEnabled = true;
            this.cmbMaterialThickness.Location = new System.Drawing.Point(72, 13);
            this.cmbMaterialThickness.Name = "cmbMaterialThickness";
            this.cmbMaterialThickness.Size = new System.Drawing.Size(112, 21);
            this.cmbMaterialThickness.TabIndex = 28;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(5, 16);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(59, 13);
            this.label29.TabIndex = 6;
            this.label29.Text = "Thickness:";
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.label30);
            this.panel16.Controls.Add(this.cmbMaterialType);
            this.panel16.Location = new System.Drawing.Point(3, 3);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(225, 41);
            this.panel16.TabIndex = 16;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(5, 16);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(34, 13);
            this.label30.TabIndex = 7;
            this.label30.Text = "Type:";
            // 
            // cmbMaterialType
            // 
            this.cmbMaterialType.FormattingEnabled = true;
            this.cmbMaterialType.Location = new System.Drawing.Point(46, 13);
            this.cmbMaterialType.Name = "cmbMaterialType";
            this.cmbMaterialType.Size = new System.Drawing.Size(172, 21);
            this.cmbMaterialType.TabIndex = 27;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(382, 96);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(0, 13);
            this.label33.TabIndex = 14;
            // 
            // panel11
            // 
            this.panel11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel11.Controls.Add(this.groupBox7);
            this.panel11.Location = new System.Drawing.Point(536, 3);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(527, 118);
            this.panel11.TabIndex = 23;
            // 
            // groupBox7
            // 
            this.groupBox7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox7.Controls.Add(this.tableLayoutPanel4);
            this.groupBox7.Location = new System.Drawing.Point(3, 3);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(521, 112);
            this.groupBox7.TabIndex = 30;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Dimensions";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel4.ColumnCount = 3;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.Controls.Add(this.panel14, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.panel19, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.panel17, 2, 1);
            this.tableLayoutPanel4.Controls.Add(this.panel13, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.panel12, 0, 0);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(6, 15);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(509, 94);
            this.tableLayoutPanel4.TabIndex = 37;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.txtDimensionArea);
            this.panel14.Controls.Add(this.txtAreaOriginal);
            this.panel14.Controls.Add(this.label8);
            this.panel14.Location = new System.Drawing.Point(341, 3);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(150, 41);
            this.panel14.TabIndex = 37;
            // 
            // txtDimensionArea
            // 
            this.txtDimensionArea.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.txtDimensionArea.Location = new System.Drawing.Point(67, 7);
            this.txtDimensionArea.Name = "txtDimensionArea";
            this.txtDimensionArea.ReadOnly = true;
            this.txtDimensionArea.Size = new System.Drawing.Size(54, 20);
            this.txtDimensionArea.TabIndex = 0;
            this.txtDimensionArea.TabStop = false;
            // 
            // txtAreaOriginal
            // 
            this.txtAreaOriginal.BackColor = System.Drawing.SystemColors.Control;
            this.txtAreaOriginal.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtAreaOriginal.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAreaOriginal.ForeColor = System.Drawing.Color.Red;
            this.txtAreaOriginal.Location = new System.Drawing.Point(67, 27);
            this.txtAreaOriginal.Name = "txtAreaOriginal";
            this.txtAreaOriginal.Size = new System.Drawing.Size(45, 11);
            this.txtAreaOriginal.TabIndex = 0;
            this.txtAreaOriginal.TabStop = false;
            this.txtAreaOriginal.Text = "0.0";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 10);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(58, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "Area ( m² ):";
            // 
            // panel19
            // 
            this.panel19.Controls.Add(this.label7);
            this.panel19.Controls.Add(this.txtDimensionRadius);
            this.panel19.Location = new System.Drawing.Point(3, 50);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(148, 41);
            this.panel19.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 15);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Radius (mm):";
            // 
            // txtDimensionRadius
            // 
            this.txtDimensionRadius.Location = new System.Drawing.Point(75, 12);
            this.txtDimensionRadius.Name = "txtDimensionRadius";
            this.txtDimensionRadius.Size = new System.Drawing.Size(68, 20);
            this.txtDimensionRadius.TabIndex = 32;
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.lblDimensionPerimeter);
            this.panel17.Controls.Add(this.label28);
            this.panel17.Location = new System.Drawing.Point(341, 50);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(150, 41);
            this.panel17.TabIndex = 1;
            // 
            // lblDimensionPerimeter
            // 
            this.lblDimensionPerimeter.AutoSize = true;
            this.lblDimensionPerimeter.Location = new System.Drawing.Point(73, 15);
            this.lblDimensionPerimeter.Name = "lblDimensionPerimeter";
            this.lblDimensionPerimeter.Size = new System.Drawing.Size(13, 13);
            this.lblDimensionPerimeter.TabIndex = 32;
            this.lblDimensionPerimeter.Text = "0";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(3, 15);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(64, 13);
            this.label28.TabIndex = 31;
            this.label28.Text = "Perim. (mm):";
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.txtDimensionWidth);
            this.panel13.Controls.Add(this.label6);
            this.panel13.Location = new System.Drawing.Point(172, 3);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(148, 41);
            this.panel13.TabIndex = 1;
            // 
            // txtDimensionWidth
            // 
            this.txtDimensionWidth.Location = new System.Drawing.Point(72, 10);
            this.txtDimensionWidth.Name = "txtDimensionWidth";
            this.txtDimensionWidth.Size = new System.Drawing.Size(73, 20);
            this.txtDimensionWidth.TabIndex = 31;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 13);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "Width (mm):";
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.label5);
            this.panel12.Controls.Add(this.txtDimensionHeight);
            this.panel12.Location = new System.Drawing.Point(3, 3);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(148, 41);
            this.panel12.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Height (mm):";
            // 
            // txtDimensionHeight
            // 
            this.txtDimensionHeight.Location = new System.Drawing.Point(75, 10);
            this.txtDimensionHeight.Name = "txtDimensionHeight";
            this.txtDimensionHeight.Size = new System.Drawing.Size(70, 20);
            this.txtDimensionHeight.TabIndex = 30;
            // 
            // grp_process
            // 
            this.grp_process.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grp_process.Controls.Add(this.tableLayoutPanel9);
            this.grp_process.Controls.Add(this.tableLayoutPanel6);
            this.grp_process.Location = new System.Drawing.Point(9, 130);
            this.grp_process.Name = "grp_process";
            this.grp_process.Size = new System.Drawing.Size(1066, 352);
            this.grp_process.TabIndex = 0;
            this.grp_process.TabStop = false;
            this.grp_process.Text = "Process";
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel9.ColumnCount = 2;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel9.Controls.Add(this.tableLayoutPanel8, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.tableLayoutPanel2, 1, 0);
            this.tableLayoutPanel9.Location = new System.Drawing.Point(15, 229);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 1;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(1037, 117);
            this.tableLayoutPanel9.TabIndex = 0;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel8.ColumnCount = 2;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Controls.Add(this.panel9, 1, 0);
            this.tableLayoutPanel8.Controls.Add(this.panel8, 0, 0);
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(408, 111);
            this.tableLayoutPanel8.TabIndex = 0;
            // 
            // panel9
            // 
            this.panel9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel9.Controls.Add(this.grpPaint);
            this.panel9.Location = new System.Drawing.Point(207, 3);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(198, 105);
            this.panel9.TabIndex = 0;
            // 
            // grpPaint
            // 
            this.grpPaint.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpPaint.Controls.Add(this.label24);
            this.grpPaint.Controls.Add(this.chkPaint);
            this.grpPaint.Controls.Add(this.lblColourCode);
            this.grpPaint.Controls.Add(this.richtxtPaintColorCode);
            this.grpPaint.Controls.Add(this.cmbPaintType);
            this.grpPaint.Location = new System.Drawing.Point(4, 5);
            this.grpPaint.Name = "grpPaint";
            this.grpPaint.Size = new System.Drawing.Size(190, 94);
            this.grpPaint.TabIndex = 50;
            this.grpPaint.TabStop = false;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(6, 23);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(34, 13);
            this.label24.TabIndex = 35;
            this.label24.Text = "Type:";
            // 
            // chkPaint
            // 
            this.chkPaint.AutoSize = true;
            this.chkPaint.Location = new System.Drawing.Point(6, 0);
            this.chkPaint.Name = "chkPaint";
            this.chkPaint.Size = new System.Drawing.Size(50, 17);
            this.chkPaint.TabIndex = 50;
            this.chkPaint.Text = "Paint";
            this.chkPaint.UseVisualStyleBackColor = true;
            // 
            // lblColourCode
            // 
            this.lblColourCode.AutoSize = true;
            this.lblColourCode.Location = new System.Drawing.Point(6, 45);
            this.lblColourCode.Name = "lblColourCode";
            this.lblColourCode.Size = new System.Drawing.Size(67, 13);
            this.lblColourCode.TabIndex = 16;
            this.lblColourCode.Text = "Colour/Code";
            // 
            // richtxtPaintColorCode
            // 
            this.richtxtPaintColorCode.Location = new System.Drawing.Point(9, 61);
            this.richtxtPaintColorCode.Name = "richtxtPaintColorCode";
            this.richtxtPaintColorCode.Size = new System.Drawing.Size(133, 22);
            this.richtxtPaintColorCode.TabIndex = 52;
            this.richtxtPaintColorCode.Text = "";
            // 
            // cmbPaintType
            // 
            this.cmbPaintType.FormattingEnabled = true;
            this.cmbPaintType.Location = new System.Drawing.Point(41, 20);
            this.cmbPaintType.Name = "cmbPaintType";
            this.cmbPaintType.Size = new System.Drawing.Size(105, 21);
            this.cmbPaintType.TabIndex = 51;
            // 
            // panel8
            // 
            this.panel8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel8.Controls.Add(this.grpSlump);
            this.panel8.Location = new System.Drawing.Point(3, 3);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(198, 105);
            this.panel8.TabIndex = 0;
            // 
            // grpSlump
            // 
            this.grpSlump.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpSlump.Controls.Add(this.richtxtSlumpCustomDescription);
            this.grpSlump.Controls.Add(this.label27);
            this.grpSlump.Controls.Add(this.label22);
            this.grpSlump.Controls.Add(this.chkSlump);
            this.grpSlump.Controls.Add(this.cmbSlumpTexture);
            this.grpSlump.Location = new System.Drawing.Point(3, 6);
            this.grpSlump.Name = "grpSlump";
            this.grpSlump.Size = new System.Drawing.Size(192, 93);
            this.grpSlump.TabIndex = 47;
            this.grpSlump.TabStop = false;
            // 
            // richtxtSlumpCustomDescription
            // 
            this.richtxtSlumpCustomDescription.Location = new System.Drawing.Point(11, 60);
            this.richtxtSlumpCustomDescription.Name = "richtxtSlumpCustomDescription";
            this.richtxtSlumpCustomDescription.Size = new System.Drawing.Size(141, 23);
            this.richtxtSlumpCustomDescription.TabIndex = 49;
            this.richtxtSlumpCustomDescription.Text = "";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(9, 44);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(101, 13);
            this.label27.TabIndex = 16;
            this.label27.Text = "Custom Description:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(8, 23);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(51, 13);
            this.label22.TabIndex = 15;
            this.label22.Text = "Textures:";
            // 
            // chkSlump
            // 
            this.chkSlump.AutoSize = true;
            this.chkSlump.Location = new System.Drawing.Point(6, 0);
            this.chkSlump.Name = "chkSlump";
            this.chkSlump.Size = new System.Drawing.Size(55, 17);
            this.chkSlump.TabIndex = 47;
            this.chkSlump.Text = "Slump";
            this.chkSlump.UseVisualStyleBackColor = true;
            // 
            // cmbSlumpTexture
            // 
            this.cmbSlumpTexture.FormattingEnabled = true;
            this.cmbSlumpTexture.Location = new System.Drawing.Point(62, 20);
            this.cmbSlumpTexture.Name = "cmbSlumpTexture";
            this.cmbSlumpTexture.Size = new System.Drawing.Size(90, 21);
            this.cmbSlumpTexture.TabIndex = 48;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.Controls.Add(this.panel7, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.panel6, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.panel5, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.panel4, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.panel3, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.panel2, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(417, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(617, 111);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // panel7
            // 
            this.panel7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel7.Controls.Add(this.grpPrint);
            this.panel7.Location = new System.Drawing.Point(311, 58);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(148, 50);
            this.panel7.TabIndex = 21;
            // 
            // grpPrint
            // 
            this.grpPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpPrint.Controls.Add(this.chkPrint);
            this.grpPrint.Controls.Add(this.rbPrintLarge);
            this.grpPrint.Controls.Add(this.rbPrintSmall);
            this.grpPrint.Location = new System.Drawing.Point(3, 3);
            this.grpPrint.Name = "grpPrint";
            this.grpPrint.Size = new System.Drawing.Size(142, 44);
            this.grpPrint.TabIndex = 71;
            this.grpPrint.TabStop = false;
            // 
            // chkPrint
            // 
            this.chkPrint.Location = new System.Drawing.Point(2, 2);
            this.chkPrint.Name = "chkPrint";
            this.chkPrint.Size = new System.Drawing.Size(56, 17);
            this.chkPrint.TabIndex = 71;
            this.chkPrint.Text = "Print";
            this.chkPrint.UseVisualStyleBackColor = true;
            // 
            // rbPrintLarge
            // 
            this.rbPrintLarge.AutoSize = true;
            this.rbPrintLarge.Dock = System.Windows.Forms.DockStyle.Right;
            this.rbPrintLarge.Location = new System.Drawing.Point(87, 16);
            this.rbPrintLarge.Name = "rbPrintLarge";
            this.rbPrintLarge.Size = new System.Drawing.Size(52, 25);
            this.rbPrintLarge.TabIndex = 73;
            this.rbPrintLarge.TabStop = true;
            this.rbPrintLarge.Text = "Large";
            this.rbPrintLarge.UseVisualStyleBackColor = true;
            // 
            // rbPrintSmall
            // 
            this.rbPrintSmall.AutoSize = true;
            this.rbPrintSmall.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbPrintSmall.Location = new System.Drawing.Point(3, 16);
            this.rbPrintSmall.Name = "rbPrintSmall";
            this.rbPrintSmall.Size = new System.Drawing.Size(50, 25);
            this.rbPrintSmall.TabIndex = 72;
            this.rbPrintSmall.TabStop = true;
            this.rbPrintSmall.Text = "Small";
            this.rbPrintSmall.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel6.Controls.Add(this.grpLaser);
            this.panel6.Location = new System.Drawing.Point(157, 58);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(148, 50);
            this.panel6.TabIndex = 21;
            // 
            // grpLaser
            // 
            this.grpLaser.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpLaser.Controls.Add(this.chkLaser);
            this.grpLaser.Controls.Add(this.rbLaserComplex);
            this.grpLaser.Controls.Add(this.rbLaserSimple);
            this.grpLaser.Location = new System.Drawing.Point(3, 3);
            this.grpLaser.Name = "grpLaser";
            this.grpLaser.Size = new System.Drawing.Size(142, 44);
            this.grpLaser.TabIndex = 68;
            this.grpLaser.TabStop = false;
            // 
            // chkLaser
            // 
            this.chkLaser.Location = new System.Drawing.Point(3, 2);
            this.chkLaser.Name = "chkLaser";
            this.chkLaser.Size = new System.Drawing.Size(61, 17);
            this.chkLaser.TabIndex = 68;
            this.chkLaser.Text = "Laser";
            this.chkLaser.UseVisualStyleBackColor = true;
            // 
            // rbLaserComplex
            // 
            this.rbLaserComplex.AutoSize = true;
            this.rbLaserComplex.Dock = System.Windows.Forms.DockStyle.Right;
            this.rbLaserComplex.Location = new System.Drawing.Point(74, 16);
            this.rbLaserComplex.Name = "rbLaserComplex";
            this.rbLaserComplex.Size = new System.Drawing.Size(65, 25);
            this.rbLaserComplex.TabIndex = 70;
            this.rbLaserComplex.TabStop = true;
            this.rbLaserComplex.Text = "Complex";
            this.rbLaserComplex.UseVisualStyleBackColor = true;
            // 
            // rbLaserSimple
            // 
            this.rbLaserSimple.AutoSize = true;
            this.rbLaserSimple.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbLaserSimple.Location = new System.Drawing.Point(3, 16);
            this.rbLaserSimple.Name = "rbLaserSimple";
            this.rbLaserSimple.Size = new System.Drawing.Size(56, 25);
            this.rbLaserSimple.TabIndex = 69;
            this.rbLaserSimple.TabStop = true;
            this.rbLaserSimple.Text = "Simple";
            this.rbLaserSimple.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel5.Controls.Add(this.grpPolishing);
            this.panel5.Location = new System.Drawing.Point(3, 58);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(148, 50);
            this.panel5.TabIndex = 21;
            // 
            // grpPolishing
            // 
            this.grpPolishing.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpPolishing.Controls.Add(this.chkPolishing);
            this.grpPolishing.Controls.Add(this.rbPolishInsource);
            this.grpPolishing.Controls.Add(this.rbPolishOutsource);
            this.grpPolishing.Location = new System.Drawing.Point(3, 3);
            this.grpPolishing.Name = "grpPolishing";
            this.grpPolishing.Size = new System.Drawing.Size(142, 44);
            this.grpPolishing.TabIndex = 65;
            this.grpPolishing.TabStop = false;
            // 
            // chkPolishing
            // 
            this.chkPolishing.Location = new System.Drawing.Point(3, 2);
            this.chkPolishing.Name = "chkPolishing";
            this.chkPolishing.Size = new System.Drawing.Size(77, 17);
            this.chkPolishing.TabIndex = 65;
            this.chkPolishing.Text = "Polishing";
            this.chkPolishing.UseVisualStyleBackColor = true;
            // 
            // rbPolishInsource
            // 
            this.rbPolishInsource.AutoSize = true;
            this.rbPolishInsource.Dock = System.Windows.Forms.DockStyle.Right;
            this.rbPolishInsource.Location = new System.Drawing.Point(105, 16);
            this.rbPolishInsource.Name = "rbPolishInsource";
            this.rbPolishInsource.Size = new System.Drawing.Size(34, 25);
            this.rbPolishInsource.TabIndex = 67;
            this.rbPolishInsource.TabStop = true;
            this.rbPolishInsource.Text = "In";
            this.rbPolishInsource.UseVisualStyleBackColor = true;
            // 
            // rbPolishOutsource
            // 
            this.rbPolishOutsource.AutoSize = true;
            this.rbPolishOutsource.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbPolishOutsource.Location = new System.Drawing.Point(3, 16);
            this.rbPolishOutsource.Name = "rbPolishOutsource";
            this.rbPolishOutsource.Size = new System.Drawing.Size(42, 25);
            this.rbPolishOutsource.TabIndex = 66;
            this.rbPolishOutsource.TabStop = true;
            this.rbPolishOutsource.Text = "Out";
            this.rbPolishOutsource.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.Controls.Add(this.grpSandblast);
            this.panel4.Location = new System.Drawing.Point(465, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(149, 49);
            this.panel4.TabIndex = 21;
            // 
            // grpSandblast
            // 
            this.grpSandblast.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpSandblast.Controls.Add(this.chkSandblast);
            this.grpSandblast.Controls.Add(this.rbSandblastInsource);
            this.grpSandblast.Controls.Add(this.rbSandblastOutsource);
            this.grpSandblast.Location = new System.Drawing.Point(3, 3);
            this.grpSandblast.Name = "grpSandblast";
            this.grpSandblast.Size = new System.Drawing.Size(143, 43);
            this.grpSandblast.TabIndex = 62;
            this.grpSandblast.TabStop = false;
            // 
            // chkSandblast
            // 
            this.chkSandblast.Location = new System.Drawing.Point(2, 2);
            this.chkSandblast.Name = "chkSandblast";
            this.chkSandblast.Size = new System.Drawing.Size(75, 19);
            this.chkSandblast.TabIndex = 62;
            this.chkSandblast.Text = "Sandblast";
            this.chkSandblast.UseVisualStyleBackColor = true;
            // 
            // rbSandblastInsource
            // 
            this.rbSandblastInsource.AutoSize = true;
            this.rbSandblastInsource.Dock = System.Windows.Forms.DockStyle.Right;
            this.rbSandblastInsource.Location = new System.Drawing.Point(106, 16);
            this.rbSandblastInsource.Name = "rbSandblastInsource";
            this.rbSandblastInsource.Size = new System.Drawing.Size(34, 24);
            this.rbSandblastInsource.TabIndex = 64;
            this.rbSandblastInsource.TabStop = true;
            this.rbSandblastInsource.Text = "In";
            this.rbSandblastInsource.UseVisualStyleBackColor = true;
            // 
            // rbSandblastOutsource
            // 
            this.rbSandblastOutsource.AutoSize = true;
            this.rbSandblastOutsource.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbSandblastOutsource.Location = new System.Drawing.Point(3, 16);
            this.rbSandblastOutsource.Name = "rbSandblastOutsource";
            this.rbSandblastOutsource.Size = new System.Drawing.Size(42, 24);
            this.rbSandblastOutsource.TabIndex = 63;
            this.rbSandblastOutsource.TabStop = true;
            this.rbSandblastOutsource.Text = "Out";
            this.rbSandblastOutsource.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.Controls.Add(this.grpBend);
            this.panel3.Location = new System.Drawing.Point(311, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(148, 49);
            this.panel3.TabIndex = 21;
            // 
            // grpBend
            // 
            this.grpBend.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpBend.Controls.Add(this.chkBend);
            this.grpBend.Controls.Add(this.rbBendInsource);
            this.grpBend.Controls.Add(this.rbBendOutsource);
            this.grpBend.Location = new System.Drawing.Point(3, 3);
            this.grpBend.Name = "grpBend";
            this.grpBend.Size = new System.Drawing.Size(142, 43);
            this.grpBend.TabIndex = 59;
            this.grpBend.TabStop = false;
            // 
            // chkBend
            // 
            this.chkBend.Location = new System.Drawing.Point(3, 2);
            this.chkBend.Name = "chkBend";
            this.chkBend.Size = new System.Drawing.Size(89, 17);
            this.chkBend.TabIndex = 59;
            this.chkBend.Text = "Bend";
            this.chkBend.UseVisualStyleBackColor = true;
            // 
            // rbBendInsource
            // 
            this.rbBendInsource.AutoSize = true;
            this.rbBendInsource.Dock = System.Windows.Forms.DockStyle.Right;
            this.rbBendInsource.Location = new System.Drawing.Point(105, 16);
            this.rbBendInsource.Name = "rbBendInsource";
            this.rbBendInsource.Size = new System.Drawing.Size(34, 24);
            this.rbBendInsource.TabIndex = 61;
            this.rbBendInsource.TabStop = true;
            this.rbBendInsource.Text = "In";
            this.rbBendInsource.UseVisualStyleBackColor = true;
            // 
            // rbBendOutsource
            // 
            this.rbBendOutsource.AutoSize = true;
            this.rbBendOutsource.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbBendOutsource.Location = new System.Drawing.Point(3, 16);
            this.rbBendOutsource.Name = "rbBendOutsource";
            this.rbBendOutsource.Size = new System.Drawing.Size(42, 24);
            this.rbBendOutsource.TabIndex = 60;
            this.rbBendOutsource.TabStop = true;
            this.rbBendOutsource.Text = "Out";
            this.rbBendOutsource.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.grpLaminate);
            this.panel2.Location = new System.Drawing.Point(157, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(148, 49);
            this.panel2.TabIndex = 20;
            // 
            // grpLaminate
            // 
            this.grpLaminate.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpLaminate.Controls.Add(this.chkLaminate);
            this.grpLaminate.Controls.Add(this.rbLaminateInsource);
            this.grpLaminate.Controls.Add(this.rbLaminateOutsource);
            this.grpLaminate.Location = new System.Drawing.Point(3, 3);
            this.grpLaminate.Name = "grpLaminate";
            this.grpLaminate.Size = new System.Drawing.Size(142, 46);
            this.grpLaminate.TabIndex = 56;
            this.grpLaminate.TabStop = false;
            // 
            // chkLaminate
            // 
            this.chkLaminate.Location = new System.Drawing.Point(3, 2);
            this.chkLaminate.Name = "chkLaminate";
            this.chkLaminate.Size = new System.Drawing.Size(69, 17);
            this.chkLaminate.TabIndex = 56;
            this.chkLaminate.Text = "Laminate";
            this.chkLaminate.UseVisualStyleBackColor = true;
            // 
            // rbLaminateInsource
            // 
            this.rbLaminateInsource.AutoSize = true;
            this.rbLaminateInsource.Dock = System.Windows.Forms.DockStyle.Right;
            this.rbLaminateInsource.Location = new System.Drawing.Point(105, 16);
            this.rbLaminateInsource.Name = "rbLaminateInsource";
            this.rbLaminateInsource.Size = new System.Drawing.Size(34, 27);
            this.rbLaminateInsource.TabIndex = 58;
            this.rbLaminateInsource.TabStop = true;
            this.rbLaminateInsource.Text = "In";
            this.rbLaminateInsource.UseVisualStyleBackColor = true;
            // 
            // rbLaminateOutsource
            // 
            this.rbLaminateOutsource.AutoSize = true;
            this.rbLaminateOutsource.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbLaminateOutsource.Location = new System.Drawing.Point(3, 16);
            this.rbLaminateOutsource.Name = "rbLaminateOutsource";
            this.rbLaminateOutsource.Size = new System.Drawing.Size(42, 27);
            this.rbLaminateOutsource.TabIndex = 57;
            this.rbLaminateOutsource.TabStop = true;
            this.rbLaminateOutsource.Text = "Out";
            this.rbLaminateOutsource.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.grpToughen);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(148, 49);
            this.panel1.TabIndex = 20;
            // 
            // grpToughen
            // 
            this.grpToughen.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpToughen.Controls.Add(this.chkToughen);
            this.grpToughen.Controls.Add(this.rbToughenInsource);
            this.grpToughen.Controls.Add(this.rbToughenOutsource);
            this.grpToughen.Location = new System.Drawing.Point(3, 3);
            this.grpToughen.Name = "grpToughen";
            this.grpToughen.Size = new System.Drawing.Size(142, 43);
            this.grpToughen.TabIndex = 53;
            this.grpToughen.TabStop = false;
            // 
            // chkToughen
            // 
            this.chkToughen.Location = new System.Drawing.Point(3, 2);
            this.chkToughen.Name = "chkToughen";
            this.chkToughen.Size = new System.Drawing.Size(69, 17);
            this.chkToughen.TabIndex = 53;
            this.chkToughen.Text = "Toughen";
            this.chkToughen.UseVisualStyleBackColor = true;
            // 
            // rbToughenInsource
            // 
            this.rbToughenInsource.AutoSize = true;
            this.rbToughenInsource.Dock = System.Windows.Forms.DockStyle.Right;
            this.rbToughenInsource.Location = new System.Drawing.Point(105, 16);
            this.rbToughenInsource.Name = "rbToughenInsource";
            this.rbToughenInsource.Size = new System.Drawing.Size(34, 24);
            this.rbToughenInsource.TabIndex = 55;
            this.rbToughenInsource.TabStop = true;
            this.rbToughenInsource.Text = "In";
            this.rbToughenInsource.UseVisualStyleBackColor = true;
            // 
            // rbToughenOutsource
            // 
            this.rbToughenOutsource.AutoSize = true;
            this.rbToughenOutsource.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbToughenOutsource.Location = new System.Drawing.Point(3, 16);
            this.rbToughenOutsource.Name = "rbToughenOutsource";
            this.rbToughenOutsource.Size = new System.Drawing.Size(42, 24);
            this.rbToughenOutsource.TabIndex = 54;
            this.rbToughenOutsource.TabStop = true;
            this.rbToughenOutsource.Text = "Out";
            this.rbToughenOutsource.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 43F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 57F));
            this.tableLayoutPanel6.Controls.Add(this.panel21, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.panel22, 0, 0);
            this.tableLayoutPanel6.Location = new System.Drawing.Point(15, 19);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(1037, 204);
            this.tableLayoutPanel6.TabIndex = 0;
            // 
            // panel21
            // 
            this.panel21.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel21.Controls.Add(this.grpCustomDesign);
            this.panel21.Location = new System.Drawing.Point(448, 3);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(586, 198);
            this.panel21.TabIndex = 40;
            // 
            // grpCustomDesign
            // 
            this.grpCustomDesign.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpCustomDesign.Controls.Add(this.btnCustomDelete);
            this.grpCustomDesign.Controls.Add(this.groupBox10);
            this.grpCustomDesign.Controls.Add(this.lstCustomDesign);
            this.grpCustomDesign.Controls.Add(this.btnCustomAdd);
            this.grpCustomDesign.Controls.Add(this.chkCustomDesign);
            this.grpCustomDesign.Location = new System.Drawing.Point(6, 3);
            this.grpCustomDesign.Name = "grpCustomDesign";
            this.grpCustomDesign.Size = new System.Drawing.Size(571, 192);
            this.grpCustomDesign.TabIndex = 40;
            this.grpCustomDesign.TabStop = false;
            // 
            // btnCustomDelete
            // 
            this.btnCustomDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCustomDelete.Location = new System.Drawing.Point(490, 163);
            this.btnCustomDelete.Name = "btnCustomDelete";
            this.btnCustomDelete.Size = new System.Drawing.Size(75, 23);
            this.btnCustomDelete.TabIndex = 46;
            this.btnCustomDelete.Text = "Delete";
            this.btnCustomDelete.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.groupBox10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox10.Controls.Add(this.tableLayoutPanel7);
            this.groupBox10.Location = new System.Drawing.Point(306, 8);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(178, 178);
            this.groupBox10.TabIndex = 41;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Custom";
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel7.ColumnCount = 1;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Controls.Add(this.panel26, 0, 3);
            this.tableLayoutPanel7.Controls.Add(this.panel25, 0, 2);
            this.tableLayoutPanel7.Controls.Add(this.panel24, 0, 1);
            this.tableLayoutPanel7.Controls.Add(this.panel23, 0, 0);
            this.tableLayoutPanel7.Location = new System.Drawing.Point(6, 11);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 4;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(166, 161);
            this.tableLayoutPanel7.TabIndex = 58;
            // 
            // panel26
            // 
            this.panel26.Controls.Add(this.txtCustomHours);
            this.panel26.Controls.Add(this.label39);
            this.panel26.Location = new System.Drawing.Point(3, 123);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(160, 35);
            this.panel26.TabIndex = 3;
            // 
            // txtCustomHours
            // 
            this.txtCustomHours.Location = new System.Drawing.Point(91, 7);
            this.txtCustomHours.Name = "txtCustomHours";
            this.txtCustomHours.Size = new System.Drawing.Size(66, 20);
            this.txtCustomHours.TabIndex = 44;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(5, 10);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(83, 13);
            this.label39.TabIndex = 55;
            this.label39.Text = "Work ($100/hr):";
            // 
            // panel25
            // 
            this.panel25.Controls.Add(this.txtCustomPrice);
            this.panel25.Controls.Add(this.label38);
            this.panel25.Location = new System.Drawing.Point(3, 83);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(160, 34);
            this.panel25.TabIndex = 2;
            // 
            // txtCustomPrice
            // 
            this.txtCustomPrice.Location = new System.Drawing.Point(91, 7);
            this.txtCustomPrice.Name = "txtCustomPrice";
            this.txtCustomPrice.Size = new System.Drawing.Size(66, 20);
            this.txtCustomPrice.TabIndex = 43;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(5, 10);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(49, 13);
            this.label38.TabIndex = 54;
            this.label38.Text = "Price ($):";
            // 
            // panel24
            // 
            this.panel24.Controls.Add(this.label35);
            this.panel24.Controls.Add(this.txtCustomMaterial);
            this.panel24.Location = new System.Drawing.Point(3, 43);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(160, 34);
            this.panel24.TabIndex = 1;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(5, 10);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(51, 13);
            this.label35.TabIndex = 53;
            this.label35.Text = "materials:";
            // 
            // txtCustomMaterial
            // 
            this.txtCustomMaterial.Location = new System.Drawing.Point(57, 7);
            this.txtCustomMaterial.Name = "txtCustomMaterial";
            this.txtCustomMaterial.Size = new System.Drawing.Size(100, 20);
            this.txtCustomMaterial.TabIndex = 42;
            // 
            // panel23
            // 
            this.panel23.Controls.Add(this.label25);
            this.panel23.Controls.Add(this.txtCustomQuantity);
            this.panel23.Location = new System.Drawing.Point(3, 3);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(160, 34);
            this.panel23.TabIndex = 0;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(5, 10);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(49, 13);
            this.label25.TabIndex = 47;
            this.label25.Text = "Quantity:";
            // 
            // txtCustomQuantity
            // 
            this.txtCustomQuantity.Location = new System.Drawing.Point(111, 7);
            this.txtCustomQuantity.Name = "txtCustomQuantity";
            this.txtCustomQuantity.Size = new System.Drawing.Size(46, 20);
            this.txtCustomQuantity.TabIndex = 41;
            // 
            // lstCustomDesign
            // 
            this.lstCustomDesign.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstCustomDesign.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4});
            this.lstCustomDesign.FullRowSelect = true;
            this.lstCustomDesign.GridLines = true;
            this.lstCustomDesign.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lstCustomDesign.Location = new System.Drawing.Point(6, 19);
            this.lstCustomDesign.MultiSelect = false;
            this.lstCustomDesign.Name = "lstCustomDesign";
            this.lstCustomDesign.Size = new System.Drawing.Size(294, 167);
            this.lstCustomDesign.TabIndex = 46;
            this.lstCustomDesign.UseCompatibleStateImageBehavior = false;
            this.lstCustomDesign.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Tag = "";
            this.columnHeader1.Text = "Qty";
            this.columnHeader1.Width = 40;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Tag = "";
            this.columnHeader2.Text = "Materials";
            this.columnHeader2.Width = 150;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Tag = "";
            this.columnHeader3.Text = "Price";
            this.columnHeader3.Width = 50;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Tag = "";
            this.columnHeader4.Text = "Hrs";
            this.columnHeader4.Width = 50;
            // 
            // btnCustomAdd
            // 
            this.btnCustomAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCustomAdd.Location = new System.Drawing.Point(490, 134);
            this.btnCustomAdd.Name = "btnCustomAdd";
            this.btnCustomAdd.Size = new System.Drawing.Size(75, 23);
            this.btnCustomAdd.TabIndex = 45;
            this.btnCustomAdd.Text = "Add";
            this.btnCustomAdd.UseVisualStyleBackColor = true;
            // 
            // chkCustomDesign
            // 
            this.chkCustomDesign.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chkCustomDesign.AutoSize = true;
            this.chkCustomDesign.Location = new System.Drawing.Point(6, 0);
            this.chkCustomDesign.Name = "chkCustomDesign";
            this.chkCustomDesign.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkCustomDesign.Size = new System.Drawing.Size(95, 17);
            this.chkCustomDesign.TabIndex = 40;
            this.chkCustomDesign.Text = "Custom design";
            this.chkCustomDesign.UseVisualStyleBackColor = true;
            // 
            // panel22
            // 
            this.panel22.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel22.Controls.Add(this.grpWaterJet);
            this.panel22.Location = new System.Drawing.Point(3, 3);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(439, 198);
            this.panel22.TabIndex = 34;
            // 
            // grpWaterJet
            // 
            this.grpWaterJet.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpWaterJet.Controls.Add(this.groupBox8);
            this.grpWaterJet.Controls.Add(this.groupBox6);
            this.grpWaterJet.Controls.Add(this.btnWaterjetDelete);
            this.grpWaterJet.Controls.Add(this.btnWaterjetAdd);
            this.grpWaterJet.Controls.Add(this.chkWaterJet);
            this.grpWaterJet.Location = new System.Drawing.Point(6, 3);
            this.grpWaterJet.Name = "grpWaterJet";
            this.grpWaterJet.Size = new System.Drawing.Size(426, 192);
            this.grpWaterJet.TabIndex = 34;
            this.grpWaterJet.TabStop = false;
            // 
            // groupBox8
            // 
            this.groupBox8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox8.Controls.Add(this.lstWaterjetCutList);
            this.groupBox8.Location = new System.Drawing.Point(6, 19);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(186, 167);
            this.groupBox8.TabIndex = 37;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "List of cuts";
            // 
            // lstWaterjetCutList
            // 
            this.lstWaterjetCutList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstWaterjetCutList.FormattingEnabled = true;
            this.lstWaterjetCutList.Location = new System.Drawing.Point(6, 19);
            this.lstWaterjetCutList.Name = "lstWaterjetCutList";
            this.lstWaterjetCutList.Size = new System.Drawing.Size(174, 134);
            this.lstWaterjetCutList.TabIndex = 0;
            this.lstWaterjetCutList.TabStop = false;
            // 
            // groupBox6
            // 
            this.groupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox6.Controls.Add(this.tableLayoutPanel10);
            this.groupBox6.Location = new System.Drawing.Point(198, 19);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(222, 138);
            this.groupBox6.TabIndex = 35;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Type of cuts (choose one)";
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel10.ColumnCount = 1;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.Controls.Add(this.panel29, 0, 2);
            this.tableLayoutPanel10.Controls.Add(this.panel28, 0, 1);
            this.tableLayoutPanel10.Controls.Add(this.panel27, 0, 0);
            this.tableLayoutPanel10.Location = new System.Drawing.Point(6, 15);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 3;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(210, 117);
            this.tableLayoutPanel10.TabIndex = 37;
            // 
            // panel29
            // 
            this.panel29.Controls.Add(this.cmbGpoCutout);
            this.panel29.Controls.Add(this.label34);
            this.panel29.Location = new System.Drawing.Point(3, 81);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(204, 28);
            this.panel29.TabIndex = 2;
            // 
            // cmbGpoCutout
            // 
            this.cmbGpoCutout.FormattingEnabled = true;
            this.cmbGpoCutout.Location = new System.Drawing.Point(83, 3);
            this.cmbGpoCutout.Name = "cmbGpoCutout";
            this.cmbGpoCutout.Size = new System.Drawing.Size(118, 21);
            this.cmbGpoCutout.TabIndex = 37;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(4, 8);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(67, 13);
            this.label34.TabIndex = 39;
            this.label34.Text = "GPO Cutout:";
            // 
            // panel28
            // 
            this.panel28.Controls.Add(this.cmbCutoutLength);
            this.panel28.Controls.Add(this.label32);
            this.panel28.Location = new System.Drawing.Point(3, 42);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(204, 27);
            this.panel28.TabIndex = 1;
            // 
            // cmbCutoutLength
            // 
            this.cmbCutoutLength.FormattingEnabled = true;
            this.cmbCutoutLength.Location = new System.Drawing.Point(83, 4);
            this.cmbCutoutLength.Name = "cmbCutoutLength";
            this.cmbCutoutLength.Size = new System.Drawing.Size(118, 21);
            this.cmbCutoutLength.TabIndex = 36;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(4, 7);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(73, 13);
            this.label32.TabIndex = 38;
            this.label32.Text = "Cutout length:";
            // 
            // panel27
            // 
            this.panel27.Controls.Add(this.chkHoleDiameter);
            this.panel27.Location = new System.Drawing.Point(3, 3);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(204, 27);
            this.panel27.TabIndex = 0;
            // 
            // chkHoleDiameter
            // 
            this.chkHoleDiameter.AutoSize = true;
            this.chkHoleDiameter.Location = new System.Drawing.Point(4, 6);
            this.chkHoleDiameter.Name = "chkHoleDiameter";
            this.chkHoleDiameter.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chkHoleDiameter.Size = new System.Drawing.Size(138, 17);
            this.chkHoleDiameter.TabIndex = 35;
            this.chkHoleDiameter.Text = "Holes < 75mm Diameter";
            this.chkHoleDiameter.UseVisualStyleBackColor = true;
            // 
            // btnWaterjetDelete
            // 
            this.btnWaterjetDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnWaterjetDelete.Location = new System.Drawing.Point(345, 163);
            this.btnWaterjetDelete.Name = "btnWaterjetDelete";
            this.btnWaterjetDelete.Size = new System.Drawing.Size(75, 23);
            this.btnWaterjetDelete.TabIndex = 39;
            this.btnWaterjetDelete.Text = "Delete";
            this.btnWaterjetDelete.UseVisualStyleBackColor = true;
            // 
            // btnWaterjetAdd
            // 
            this.btnWaterjetAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnWaterjetAdd.Location = new System.Drawing.Point(264, 163);
            this.btnWaterjetAdd.Name = "btnWaterjetAdd";
            this.btnWaterjetAdd.Size = new System.Drawing.Size(75, 23);
            this.btnWaterjetAdd.TabIndex = 38;
            this.btnWaterjetAdd.Text = "Add";
            this.btnWaterjetAdd.UseVisualStyleBackColor = true;
            // 
            // chkWaterJet
            // 
            this.chkWaterJet.AutoSize = true;
            this.chkWaterJet.Location = new System.Drawing.Point(6, 0);
            this.chkWaterJet.Name = "chkWaterJet";
            this.chkWaterJet.Size = new System.Drawing.Size(66, 17);
            this.chkWaterJet.TabIndex = 34;
            this.chkWaterJet.Text = "Waterjet";
            this.chkWaterJet.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.cmbItemOverridePercent);
            this.groupBox3.Controls.Add(this.cmbItemOverrideDollar);
            this.groupBox3.Controls.Add(this.label21);
            this.groupBox3.Controls.Add(this.cmbPriceFactor);
            this.groupBox3.Controls.Add(this.lblItemCustomerPrice);
            this.groupBox3.Controls.Add(this.txtItemOverridePercent);
            this.groupBox3.Controls.Add(this.lblItemJobPrice);
            this.groupBox3.Controls.Add(this.txtItemOverrideDollar);
            this.groupBox3.Controls.Add(this.lblItemCostPrice);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.Location = new System.Drawing.Point(1114, 165);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(159, 209);
            this.groupBox3.TabIndex = 14;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Item Calculation";
            // 
            // cmbItemOverridePercent
            // 
            this.cmbItemOverridePercent.FormattingEnabled = true;
            this.cmbItemOverridePercent.Location = new System.Drawing.Point(118, 137);
            this.cmbItemOverridePercent.Name = "cmbItemOverridePercent";
            this.cmbItemOverridePercent.Size = new System.Drawing.Size(31, 21);
            this.cmbItemOverridePercent.TabIndex = 17;
            // 
            // cmbItemOverrideDollar
            // 
            this.cmbItemOverrideDollar.FormattingEnabled = true;
            this.cmbItemOverrideDollar.Location = new System.Drawing.Point(118, 116);
            this.cmbItemOverrideDollar.Name = "cmbItemOverrideDollar";
            this.cmbItemOverrideDollar.Size = new System.Drawing.Size(31, 21);
            this.cmbItemOverrideDollar.TabIndex = 15;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(5, 166);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(67, 13);
            this.label21.TabIndex = 29;
            this.label21.Text = "Price Factor:";
            // 
            // cmbPriceFactor
            // 
            this.cmbPriceFactor.FormattingEnabled = true;
            this.cmbPriceFactor.Location = new System.Drawing.Point(78, 163);
            this.cmbPriceFactor.Name = "cmbPriceFactor";
            this.cmbPriceFactor.Size = new System.Drawing.Size(47, 21);
            this.cmbPriceFactor.TabIndex = 18;
            // 
            // lblItemCustomerPrice
            // 
            this.lblItemCustomerPrice.AutoSize = true;
            this.lblItemCustomerPrice.Location = new System.Drawing.Point(79, 79);
            this.lblItemCustomerPrice.Name = "lblItemCustomerPrice";
            this.lblItemCustomerPrice.Size = new System.Drawing.Size(13, 13);
            this.lblItemCustomerPrice.TabIndex = 27;
            this.lblItemCustomerPrice.Text = "0";
            // 
            // txtItemOverridePercent
            // 
            this.txtItemOverridePercent.Location = new System.Drawing.Point(69, 137);
            this.txtItemOverridePercent.Name = "txtItemOverridePercent";
            this.txtItemOverridePercent.Size = new System.Drawing.Size(43, 20);
            this.txtItemOverridePercent.TabIndex = 16;
            // 
            // lblItemJobPrice
            // 
            this.lblItemJobPrice.AutoSize = true;
            this.lblItemJobPrice.Location = new System.Drawing.Point(79, 53);
            this.lblItemJobPrice.Name = "lblItemJobPrice";
            this.lblItemJobPrice.Size = new System.Drawing.Size(13, 13);
            this.lblItemJobPrice.TabIndex = 26;
            this.lblItemJobPrice.Text = "0";
            // 
            // txtItemOverrideDollar
            // 
            this.txtItemOverrideDollar.Location = new System.Drawing.Point(69, 116);
            this.txtItemOverrideDollar.Name = "txtItemOverrideDollar";
            this.txtItemOverrideDollar.Size = new System.Drawing.Size(43, 20);
            this.txtItemOverrideDollar.TabIndex = 14;
            // 
            // lblItemCostPrice
            // 
            this.lblItemCostPrice.AutoSize = true;
            this.lblItemCostPrice.Location = new System.Drawing.Point(79, 28);
            this.lblItemCostPrice.Name = "lblItemCostPrice";
            this.lblItemCostPrice.Size = new System.Drawing.Size(13, 13);
            this.lblItemCostPrice.TabIndex = 25;
            this.lblItemCostPrice.Text = "0";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(5, 140);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(58, 13);
            this.label10.TabIndex = 17;
            this.label10.Text = "Override %";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(5, 119);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(56, 13);
            this.label17.TabIndex = 17;
            this.label17.Text = "Override $";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 79);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(67, 13);
            this.label18.TabIndex = 17;
            this.label18.Text = "Cust. Price $";
            // 
            // label19
            // 
            this.label19.Location = new System.Drawing.Point(5, 28);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(79, 19);
            this.label19.TabIndex = 19;
            this.label19.Text = "Cost Price $";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 53);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(60, 13);
            this.label20.TabIndex = 17;
            this.label20.Text = "Job Price $";
            // 
            // btnQuotationNewItem
            // 
            this.btnQuotationNewItem.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnQuotationNewItem.Location = new System.Drawing.Point(17, 41);
            this.btnQuotationNewItem.Name = "btnQuotationNewItem";
            this.btnQuotationNewItem.Size = new System.Drawing.Size(75, 39);
            this.btnQuotationNewItem.TabIndex = 19;
            this.btnQuotationNewItem.Text = "Add Item";
            this.btnQuotationNewItem.UseVisualStyleBackColor = true;
            // 
            // btnQuotationDeleteItem
            // 
            this.btnQuotationDeleteItem.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnQuotationDeleteItem.Location = new System.Drawing.Point(17, 110);
            this.btnQuotationDeleteItem.Name = "btnQuotationDeleteItem";
            this.btnQuotationDeleteItem.Size = new System.Drawing.Size(75, 39);
            this.btnQuotationDeleteItem.TabIndex = 20;
            this.btnQuotationDeleteItem.Text = "Delete Item";
            this.btnQuotationDeleteItem.UseVisualStyleBackColor = true;
            // 
            // btnQuotationConfirm
            // 
            this.btnQuotationConfirm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnQuotationConfirm.Location = new System.Drawing.Point(921, 678);
            this.btnQuotationConfirm.Name = "btnQuotationConfirm";
            this.btnQuotationConfirm.Size = new System.Drawing.Size(75, 23);
            this.btnQuotationConfirm.TabIndex = 25;
            this.btnQuotationConfirm.Text = "Confirm";
            this.btnQuotationConfirm.UseVisualStyleBackColor = true;
            // 
            // btnQuotationDeleteAttach
            // 
            this.btnQuotationDeleteAttach.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnQuotationDeleteAttach.Location = new System.Drawing.Point(174, 708);
            this.btnQuotationDeleteAttach.Name = "btnQuotationDeleteAttach";
            this.btnQuotationDeleteAttach.Size = new System.Drawing.Size(110, 23);
            this.btnQuotationDeleteAttach.TabIndex = 22;
            this.btnQuotationDeleteAttach.Text = "Delete Attachment";
            this.btnQuotationDeleteAttach.UseVisualStyleBackColor = true;
            // 
            // ltbQuotationListFiles
            // 
            this.ltbQuotationListFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ltbQuotationListFiles.FormattingEnabled = true;
            this.ltbQuotationListFiles.Location = new System.Drawing.Point(12, 676);
            this.ltbQuotationListFiles.Name = "ltbQuotationListFiles";
            this.ltbQuotationListFiles.Size = new System.Drawing.Size(156, 56);
            this.ltbQuotationListFiles.TabIndex = 31;
            // 
            // btnQuotationAttach
            // 
            this.btnQuotationAttach.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnQuotationAttach.Location = new System.Drawing.Point(174, 676);
            this.btnQuotationAttach.Name = "btnQuotationAttach";
            this.btnQuotationAttach.Size = new System.Drawing.Size(110, 23);
            this.btnQuotationAttach.TabIndex = 21;
            this.btnQuotationAttach.Text = "Add Attachment";
            this.btnQuotationAttach.UseVisualStyleBackColor = true;
            // 
            // btnQuotationSave
            // 
            this.btnQuotationSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnQuotationSave.Location = new System.Drawing.Point(836, 678);
            this.btnQuotationSave.Name = "btnQuotationSave";
            this.btnQuotationSave.Size = new System.Drawing.Size(75, 23);
            this.btnQuotationSave.TabIndex = 24;
            this.btnQuotationSave.Text = "Save";
            this.btnQuotationSave.UseVisualStyleBackColor = true;
            // 
            // btnQuotationNote
            // 
            this.btnQuotationNote.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnQuotationNote.Location = new System.Drawing.Point(296, 708);
            this.btnQuotationNote.Name = "btnQuotationNote";
            this.btnQuotationNote.Size = new System.Drawing.Size(75, 23);
            this.btnQuotationNote.TabIndex = 23;
            this.btnQuotationNote.Text = "Notes";
            this.btnQuotationNote.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox5.Controls.Add(this.btnQuotationNewItem);
            this.groupBox5.Controls.Add(this.btnQuotationDeleteItem);
            this.groupBox5.Location = new System.Drawing.Point(1114, 497);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(125, 172);
            this.groupBox5.TabIndex = 19;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Item Tools";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24F));
            this.tableLayoutPanel1.Controls.Add(this.groupBox9, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupBox15, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupBox14, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupBox2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupBox1, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(17, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1255, 125);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // groupBox9
            // 
            this.groupBox9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox9.Controls.Add(this.lblGstAmount);
            this.groupBox9.Controls.Add(this.label13);
            this.groupBox9.Controls.Add(this.lblQuoteGstTotal);
            this.groupBox9.Controls.Add(this.label12);
            this.groupBox9.Controls.Add(this.lblQuoteTotal);
            this.groupBox9.Controls.Add(this.textBox5);
            this.groupBox9.Controls.Add(this.cmbQuoteOverridePercent);
            this.groupBox9.Controls.Add(this.txtQuoteOverridePercent);
            this.groupBox9.Controls.Add(this.cmbQuoteOverrideDollar);
            this.groupBox9.Controls.Add(this.txtQuoteOverrideDollar);
            this.groupBox9.Controls.Add(this.label16);
            this.groupBox9.Controls.Add(this.label15);
            this.groupBox9.Controls.Add(this.label14);
            this.groupBox9.Location = new System.Drawing.Point(955, 3);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(297, 119);
            this.groupBox9.TabIndex = 8;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Quote Calculation Total";
            // 
            // lblGstAmount
            // 
            this.lblGstAmount.AutoSize = true;
            this.lblGstAmount.Location = new System.Drawing.Point(129, 78);
            this.lblGstAmount.Name = "lblGstAmount";
            this.lblGstAmount.Size = new System.Drawing.Size(13, 13);
            this.lblGstAmount.TabIndex = 41;
            this.lblGstAmount.Text = "0";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 78);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 13);
            this.label13.TabIndex = 40;
            this.label13.Text = "GST ( 10%) :";
            // 
            // lblQuoteGstTotal
            // 
            this.lblQuoteGstTotal.AutoSize = true;
            this.lblQuoteGstTotal.Location = new System.Drawing.Point(129, 94);
            this.lblQuoteGstTotal.Name = "lblQuoteGstTotal";
            this.lblQuoteGstTotal.Size = new System.Drawing.Size(13, 13);
            this.lblQuoteGstTotal.TabIndex = 39;
            this.lblQuoteGstTotal.Text = "0";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(7, 94);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(37, 13);
            this.label12.TabIndex = 38;
            this.label12.Text = "Total :";
            // 
            // lblQuoteTotal
            // 
            this.lblQuoteTotal.AutoSize = true;
            this.lblQuoteTotal.Location = new System.Drawing.Point(129, 16);
            this.lblQuoteTotal.Name = "lblQuoteTotal";
            this.lblQuoteTotal.Size = new System.Drawing.Size(13, 13);
            this.lblQuoteTotal.TabIndex = 27;
            this.lblQuoteTotal.Text = "0";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(-112, 50);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(77, 20);
            this.textBox5.TabIndex = 5;
            // 
            // cmbQuoteOverridePercent
            // 
            this.cmbQuoteOverridePercent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbQuoteOverridePercent.FormattingEnabled = true;
            this.cmbQuoteOverridePercent.Location = new System.Drawing.Point(228, 54);
            this.cmbQuoteOverridePercent.Name = "cmbQuoteOverridePercent";
            this.cmbQuoteOverridePercent.Size = new System.Drawing.Size(46, 21);
            this.cmbQuoteOverridePercent.TabIndex = 11;
            // 
            // txtQuoteOverridePercent
            // 
            this.txtQuoteOverridePercent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtQuoteOverridePercent.Location = new System.Drawing.Point(74, 54);
            this.txtQuoteOverridePercent.Name = "txtQuoteOverridePercent";
            this.txtQuoteOverridePercent.Size = new System.Drawing.Size(148, 20);
            this.txtQuoteOverridePercent.TabIndex = 10;
            // 
            // cmbQuoteOverrideDollar
            // 
            this.cmbQuoteOverrideDollar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbQuoteOverrideDollar.FormattingEnabled = true;
            this.cmbQuoteOverrideDollar.Location = new System.Drawing.Point(228, 33);
            this.cmbQuoteOverrideDollar.Name = "cmbQuoteOverrideDollar";
            this.cmbQuoteOverrideDollar.Size = new System.Drawing.Size(46, 21);
            this.cmbQuoteOverrideDollar.TabIndex = 9;
            // 
            // txtQuoteOverrideDollar
            // 
            this.txtQuoteOverrideDollar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtQuoteOverrideDollar.Location = new System.Drawing.Point(74, 33);
            this.txtQuoteOverrideDollar.Name = "txtQuoteOverrideDollar";
            this.txtQuoteOverrideDollar.Size = new System.Drawing.Size(148, 20);
            this.txtQuoteOverrideDollar.TabIndex = 8;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 57);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(67, 13);
            this.label16.TabIndex = 17;
            this.label16.Text = "Override (%):";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 36);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(65, 13);
            this.label15.TabIndex = 17;
            this.label15.Text = "Override ($):";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 17);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(52, 13);
            this.label14.TabIndex = 17;
            this.label14.Text = "Subtotal :";
            // 
            // groupBox15
            // 
            this.groupBox15.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox15.AutoSize = true;
            this.groupBox15.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox15.Controls.Add(this.lblQuoteDelivery);
            this.groupBox15.Controls.Add(this.txtDeliveryDistance);
            this.groupBox15.Controls.Add(this.label36);
            this.groupBox15.Controls.Add(this.label23);
            this.groupBox15.Location = new System.Drawing.Point(742, 3);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(207, 73);
            this.groupBox15.TabIndex = 7;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Delivery";
            // 
            // lblQuoteDelivery
            // 
            this.lblQuoteDelivery.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblQuoteDelivery.AutoSize = true;
            this.lblQuoteDelivery.Location = new System.Drawing.Point(98, 44);
            this.lblQuoteDelivery.Name = "lblQuoteDelivery";
            this.lblQuoteDelivery.Size = new System.Drawing.Size(66, 13);
            this.lblQuoteDelivery.TabIndex = 3;
            this.lblQuoteDelivery.Text = "delivery cost";
            // 
            // txtDeliveryDistance
            // 
            this.txtDeliveryDistance.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDeliveryDistance.Location = new System.Drawing.Point(94, 19);
            this.txtDeliveryDistance.Name = "txtDeliveryDistance";
            this.txtDeliveryDistance.Size = new System.Drawing.Size(107, 20);
            this.txtDeliveryDistance.TabIndex = 7;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(10, 44);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(88, 13);
            this.label36.TabIndex = 1;
            this.label36.Text = "Rate - $1.81/km:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(10, 19);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(78, 13);
            this.label23.TabIndex = 0;
            this.label23.Text = "Distance (km) :";
            // 
            // groupBox14
            // 
            this.groupBox14.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox14.AutoSize = true;
            this.groupBox14.Controls.Add(this.lblInstallationCost);
            this.groupBox14.Controls.Add(this.label11);
            this.groupBox14.Controls.Add(this.txtInstallationHrs);
            this.groupBox14.Controls.Add(this.label9);
            this.groupBox14.Location = new System.Drawing.Point(529, 3);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(207, 73);
            this.groupBox14.TabIndex = 6;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Installation";
            // 
            // lblInstallationCost
            // 
            this.lblInstallationCost.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblInstallationCost.AutoSize = true;
            this.lblInstallationCost.Location = new System.Drawing.Point(94, 44);
            this.lblInstallationCost.Name = "lblInstallationCost";
            this.lblInstallationCost.Size = new System.Drawing.Size(79, 13);
            this.lblInstallationCost.TabIndex = 35;
            this.lblInstallationCost.Text = "installation cost";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 44);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(78, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "Cost($100/hr) :";
            // 
            // txtInstallationHrs
            // 
            this.txtInstallationHrs.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtInstallationHrs.Location = new System.Drawing.Point(68, 20);
            this.txtInstallationHrs.Name = "txtInstallationHrs";
            this.txtInstallationHrs.Size = new System.Drawing.Size(128, 20);
            this.txtInstallationHrs.TabIndex = 6;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 23);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "No. of hrs:";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.txtVersion);
            this.groupBox2.Controls.Add(this.label26);
            this.groupBox2.Controls.Add(this.txtCreatedDate);
            this.groupBox2.Controls.Add(this.txtQuoteNumber);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(304, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(219, 107);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Quote Details";
            // 
            // txtVersion
            // 
            this.txtVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtVersion.Location = new System.Drawing.Point(92, 69);
            this.txtVersion.Name = "txtVersion";
            this.txtVersion.Size = new System.Drawing.Size(42, 20);
            this.txtVersion.TabIndex = 5;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(7, 72);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(45, 13);
            this.label26.TabIndex = 4;
            this.label26.Text = "Version:";
            // 
            // txtCreatedDate
            // 
            this.txtCreatedDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCreatedDate.Location = new System.Drawing.Point(92, 41);
            this.txtCreatedDate.Name = "txtCreatedDate";
            this.txtCreatedDate.Size = new System.Drawing.Size(120, 20);
            this.txtCreatedDate.TabIndex = 4;
            // 
            // txtQuoteNumber
            // 
            this.txtQuoteNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtQuoteNumber.Location = new System.Drawing.Point(92, 17);
            this.txtQuoteNumber.Name = "txtQuoteNumber";
            this.txtQuoteNumber.Size = new System.Drawing.Size(119, 20);
            this.txtQuoteNumber.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 44);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Created Date:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Quote Number:";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox1.Controls.Add(this.txtAddress);
            this.groupBox1.Controls.Add(this.label41);
            this.groupBox1.Controls.Add(this.txtCompanyName);
            this.groupBox1.Controls.Add(this.txtName);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(295, 107);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Customer Details";
            // 
            // txtAddress
            // 
            this.txtAddress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAddress.Location = new System.Drawing.Point(67, 75);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(222, 20);
            this.txtAddress.TabIndex = 6;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(6, 78);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(48, 13);
            this.label41.TabIndex = 3;
            this.label41.Text = "Address:";
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCompanyName.Location = new System.Drawing.Point(67, 17);
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Size = new System.Drawing.Size(222, 20);
            this.txtCompanyName.TabIndex = 1;
            // 
            // txtName
            // 
            this.txtName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtName.Location = new System.Drawing.Point(67, 47);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(222, 20);
            this.txtName.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Name:";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Company:";
            // 
            // btnBrowseTemplate
            // 
            this.btnBrowseTemplate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBrowseTemplate.Location = new System.Drawing.Point(329, 19);
            this.btnBrowseTemplate.Name = "btnBrowseTemplate";
            this.btnBrowseTemplate.Size = new System.Drawing.Size(75, 23);
            this.btnBrowseTemplate.TabIndex = 32;
            this.btnBrowseTemplate.Text = "Browse";
            this.btnBrowseTemplate.UseVisualStyleBackColor = true;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(10, 24);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(80, 13);
            this.label40.TabIndex = 33;
            this.label40.Text = "Template used:";
            // 
            // lblTemplateFileName
            // 
            this.lblTemplateFileName.AutoSize = true;
            this.lblTemplateFileName.Location = new System.Drawing.Point(96, 24);
            this.lblTemplateFileName.Name = "lblTemplateFileName";
            this.lblTemplateFileName.Size = new System.Drawing.Size(20, 13);
            this.lblTemplateFileName.TabIndex = 34;
            this.lblTemplateFileName.Text = "file";
            // 
            // groupBox11
            // 
            this.groupBox11.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.groupBox11.Controls.Add(this.lblTemplateFileName);
            this.groupBox11.Controls.Add(this.btnBrowseTemplate);
            this.groupBox11.Controls.Add(this.label40);
            this.groupBox11.Location = new System.Drawing.Point(377, 676);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(410, 53);
            this.groupBox11.TabIndex = 35;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Quote Template";
            // 
            // Quotation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1284, 741);
            this.Controls.Add(this.groupBox11);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.btnQuotationSave);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.btnQuotationNote);
            this.Controls.Add(this.btnQuotationDeleteAttach);
            this.Controls.Add(this.ltbQuotationListFiles);
            this.Controls.Add(this.btnQuotationAttach);
            this.Controls.Add(this.btnQuotationConfirm);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.btnQuotationClose);
            this.Controls.Add(this.tabcontrol_quotation);
            this.MinimumSize = new System.Drawing.Size(1300, 726);
            this.Name = "Quotation";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.tabcontrol_quotation.ResumeLayout(false);
            this.tabpage_quotation.ResumeLayout(false);
            this.pnl_quotation.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.tableLayoutPanel11.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.panel20.PerformLayout();
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.panel19.ResumeLayout(false);
            this.panel19.PerformLayout();
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.grp_process.ResumeLayout(false);
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.grpPaint.ResumeLayout(false);
            this.grpPaint.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.grpSlump.ResumeLayout(false);
            this.grpSlump.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.grpPrint.ResumeLayout(false);
            this.grpPrint.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.grpLaser.ResumeLayout(false);
            this.grpLaser.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.grpPolishing.ResumeLayout(false);
            this.grpPolishing.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.grpSandblast.ResumeLayout(false);
            this.grpSandblast.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.grpBend.ResumeLayout(false);
            this.grpBend.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.grpLaminate.ResumeLayout(false);
            this.grpLaminate.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.grpToughen.ResumeLayout(false);
            this.grpToughen.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.panel21.ResumeLayout(false);
            this.grpCustomDesign.ResumeLayout(false);
            this.grpCustomDesign.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.panel26.ResumeLayout(false);
            this.panel26.PerformLayout();
            this.panel25.ResumeLayout(false);
            this.panel25.PerformLayout();
            this.panel24.ResumeLayout(false);
            this.panel24.PerformLayout();
            this.panel23.ResumeLayout(false);
            this.panel23.PerformLayout();
            this.panel22.ResumeLayout(false);
            this.grpWaterJet.ResumeLayout(false);
            this.grpWaterJet.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.tableLayoutPanel10.ResumeLayout(false);
            this.panel29.ResumeLayout(false);
            this.panel29.PerformLayout();
            this.panel28.ResumeLayout(false);
            this.panel28.PerformLayout();
            this.panel27.ResumeLayout(false);
            this.panel27.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnQuotationClose;
        private System.Windows.Forms.TabControl tabcontrol_quotation;
        private System.Windows.Forms.TabPage tabpage_quotation;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtItemOverridePercent;
        private System.Windows.Forms.TextBox txtItemOverrideDollar;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button btnQuotationNewItem;
        private System.Windows.Forms.Button btnQuotationDeleteItem;
        private System.Windows.Forms.Label lblItemCustomerPrice;
        private System.Windows.Forms.Label lblItemJobPrice;
        private System.Windows.Forms.Label lblItemCostPrice;
        private System.Windows.Forms.Button btnQuotationConfirm;
        private System.Windows.Forms.Button btnQuotationDeleteAttach;
        private System.Windows.Forms.ListBox ltbQuotationListFiles;
        private System.Windows.Forms.Button btnQuotationAttach;
        private System.Windows.Forms.Button btnQuotationSave;
        private System.Windows.Forms.Button btnQuotationNote;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox cmbPriceFactor;
        private System.Windows.Forms.ComboBox cmbItemOverridePercent;
        private System.Windows.Forms.ComboBox cmbItemOverrideDollar;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label lblGstAmount;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lblQuoteGstTotal;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lblQuoteTotal;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.ComboBox cmbQuoteOverridePercent;
        private System.Windows.Forms.TextBox txtQuoteOverridePercent;
        private System.Windows.Forms.ComboBox cmbQuoteOverrideDollar;
        private System.Windows.Forms.TextBox txtQuoteOverrideDollar;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.Label lblQuoteDelivery;
        private System.Windows.Forms.TextBox txtDeliveryDistance;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.Label lblInstallationCost;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtInstallationHrs;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtVersion;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtCreatedDate;
        private System.Windows.Forms.TextBox txtQuoteNumber;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtCompanyName;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pnl_quotation;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.ComboBox cmbMaterialThickness;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.ComboBox cmbMaterialType;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.TextBox txtDimensionArea;
        private System.Windows.Forms.TextBox txtAreaOriginal;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtDimensionRadius;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Label lblDimensionPerimeter;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.TextBox txtDimensionWidth;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtDimensionHeight;
        private System.Windows.Forms.GroupBox grp_process;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.GroupBox grpPaint;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.CheckBox chkPaint;
        private System.Windows.Forms.Label lblColourCode;
        private System.Windows.Forms.RichTextBox richtxtPaintColorCode;
        private System.Windows.Forms.ComboBox cmbPaintType;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.GroupBox grpSlump;
        private System.Windows.Forms.RichTextBox richtxtSlumpCustomDescription;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.CheckBox chkSlump;
        private System.Windows.Forms.ComboBox cmbSlumpTexture;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.GroupBox grpPrint;
        private System.Windows.Forms.CheckBox chkPrint;
        private System.Windows.Forms.RadioButton rbPrintLarge;
        private System.Windows.Forms.RadioButton rbPrintSmall;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.GroupBox grpLaser;
        private System.Windows.Forms.CheckBox chkLaser;
        private System.Windows.Forms.RadioButton rbLaserComplex;
        private System.Windows.Forms.RadioButton rbLaserSimple;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.GroupBox grpPolishing;
        private System.Windows.Forms.CheckBox chkPolishing;
        private System.Windows.Forms.RadioButton rbPolishInsource;
        private System.Windows.Forms.RadioButton rbPolishOutsource;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.GroupBox grpSandblast;
        private System.Windows.Forms.CheckBox chkSandblast;
        private System.Windows.Forms.RadioButton rbSandblastInsource;
        private System.Windows.Forms.RadioButton rbSandblastOutsource;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.GroupBox grpBend;
        private System.Windows.Forms.CheckBox chkBend;
        private System.Windows.Forms.RadioButton rbBendInsource;
        private System.Windows.Forms.RadioButton rbBendOutsource;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox grpLaminate;
        private System.Windows.Forms.CheckBox chkLaminate;
        private System.Windows.Forms.RadioButton rbLaminateInsource;
        private System.Windows.Forms.RadioButton rbLaminateOutsource;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox grpToughen;
        private System.Windows.Forms.CheckBox chkToughen;
        private System.Windows.Forms.RadioButton rbToughenInsource;
        private System.Windows.Forms.RadioButton rbToughenOutsource;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.GroupBox grpCustomDesign;
        private System.Windows.Forms.CheckBox chkCustomDesign;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.GroupBox grpWaterJet;
        private System.Windows.Forms.CheckBox chkWaterJet;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.CheckBox chkHoleDiameter;
        private System.Windows.Forms.ComboBox cmbCutoutLength;
        private System.Windows.Forms.ComboBox cmbGpoCutout;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Button btnWaterjetDelete;
        private System.Windows.Forms.Button btnWaterjetAdd;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.ListBox lstWaterjetCutList;
        private System.Windows.Forms.ListView lstCustomDesign;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.TextBox txtCustomQuantity;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Button btnCustomDelete;
        private System.Windows.Forms.Button btnCustomAdd;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.TextBox txtCustomHours;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.TextBox txtCustomPrice;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox txtCustomMaterial;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txtMaterialCustomDescription;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.TextBox txtDimensionQuantity;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Button btnBrowseTemplate;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label lblTemplateFileName;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox txtAddress;
    }
}