﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProfileGlassHome
{
    public partial class Supplier_Profile : Form
    {
        public Supplier_Profile()
        {
            InitializeComponent();


            dg_purchase_order_list.ReadOnly = true;
            dg_purchase_order_list.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            btn_new_purchase_order.Click += new EventHandler(All_Button_Changed);
            btn_edit_purchase_order.Click += new EventHandler(All_Button_Changed);
            btn_close.Click += new EventHandler(All_Button_Changed);
        }


        private void All_Button_Changed(object sender, EventArgs e)
        {
            System.Windows.Forms.Button btn = (sender as System.Windows.Forms.Button);
            switch (btn.Name)
            {
                case "btn_new_purchase_order":
                    PurchaseOrder purchase_order_1 = new PurchaseOrder();
                    purchase_order_1.ShowDialog();
                    break;
                case "btn_edit_purchase_order":
                    PurchaseOrder purchase_order_2 = new PurchaseOrder();
                    purchase_order_2.ShowDialog();
                    break;
                case "btn_close":
                    this.Close();
                    break;
            }
        }



    }
}
