﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using Word = Microsoft.Office.Interop.Word;

namespace ProfileGlassHome
{
    class PdfDocument
    {
        private object saveToLocation;
        private object fileName;

        public object getSaveToLocation
        {
            get
            {
                return saveToLocation;
            }
            set
            {
                saveToLocation = value;
            }
        }
        public object getFileName
        {
            get
            {
                return fileName;
            }
            set
            {
                fileName = value;
            }
        }

        //Find and Replace Method
        private void FindAndReplace(Word.Application wordApp, string toFindText, string replaceWithText)
        {
            try
            {
                //Handles text when the length is over 255 characters
                if (replaceWithText.Length > 255)
                {
                    FindAndReplace(wordApp, toFindText, toFindText + replaceWithText.Substring(255));
                    replaceWithText = replaceWithText.Substring(0, 255);
                }

                object matchCase = true;
                object matchWholeWord = true;
                object matchWildCards = false;
                object matchSoundLike = false;
                object nmatchAllforms = false;
                object forward = true;
                object format = false;
                object matchKashida = false;
                object matchDiactitics = false;
                object matchAlefHamza = false;
                object matchControl = false;
                object read_only = false;
                object visible = true;
                object replace = 2;
                object wrap = 1;

                wordApp.Selection.Find.Execute(toFindText,
                    ref matchCase, ref matchWholeWord,
                    ref matchWildCards, ref matchSoundLike,
                    ref nmatchAllforms, ref forward,
                    ref wrap, ref format, replaceWithText,
                    ref replace, ref matchKashida,
                    ref matchDiactitics, ref matchAlefHamza,
                    ref matchControl);
            } catch (Exception e)
            {
                MessageBox.Show("PdfDocument - FindAndReplace : " + e);
            }
        }

        //Create the Doc Method
        public void CreatePdfDocument(
            string company, string name, string address, string quote, string date, 
            int itemCount, List<string> itemStringList, List<string> quantityStringList, List<string> unitCostStringList, List<string> subTotalStringList, List<int> newLineCountList,  
            string delivery, string installation,
            string totalPrice, string gstAmount, string totalAndGst)
        {
            Word.Application wordApp = new Word.Application();
            object missing = Missing.Value;
            Word.Document myWordDoc = null;
            object readOnly = false;
            object isVisible = false;
            wordApp.Visible = false;

            string appendedItemString = "";
            string appendedQuantityString = "";
            string appendedunitCostString = "";
            string appendedsubTotalString = "";

            try
            {
                if (File.Exists((string)fileName))
                {
                    myWordDoc = wordApp.Documents.Open(ref fileName, ref missing, ref readOnly,
                                            ref missing, ref missing, ref missing,
                                            ref missing, ref missing, ref missing,
                                            ref missing, ref missing, ref missing,
                                            ref missing, ref missing, ref missing, ref missing);
                    myWordDoc.Activate();

                    //Find and replace the customer details
                    this.FindAndReplace(wordApp, "<company>", company);
                    this.FindAndReplace(wordApp, "<name>", name);
                    this.FindAndReplace(wordApp, "<address>", address);

                    //Find and replace the quote code and dates
                    this.FindAndReplace(wordApp, "<quotationCode>", quote);
                    this.FindAndReplace(wordApp, "<date>", date);

                    //Handle the items list into one long string
                    for (int i = 0; i < itemCount; i++)
                    {
                        appendedItemString += itemStringList[i];
                    }
                    this.FindAndReplace(wordApp, "<item>", appendedItemString);

                    //Handle the qty, price and subtotal list into one long string with the correct line spacing (using newline)
                    for (int i = 0; i < itemCount; i++)
                    {
                        //Append the item qty, unitCost and subTotal
                        appendedQuantityString += quantityStringList[i];
                        appendedunitCostString += unitCostStringList[i];
                        appendedsubTotalString += subTotalStringList[i];

                        //Handle the number of newlines and append accordingly
                        for (int j = 0; j < newLineCountList[i]; j++)
                        {
                            appendedQuantityString += Environment.NewLine;
                            appendedunitCostString += Environment.NewLine;
                            appendedsubTotalString += Environment.NewLine;
                        }
                    }
                    
                    //Handle the qty, unit price and subtotal
                    this.FindAndReplace(wordApp, "<qty>", appendedQuantityString);
                    this.FindAndReplace(wordApp, "<price>", appendedunitCostString);
                    this.FindAndReplace(wordApp, "<subtotal>", appendedsubTotalString);
                    
                    //Handle the installation and delivery cost
                    this.FindAndReplace(wordApp, "<installationcost>", installation);
                    this.FindAndReplace(wordApp, "<deliverycost>", delivery);

                    //Handle the total, job and gst
                    this.FindAndReplace(wordApp, "<jobPrice>", totalPrice);
                    this.FindAndReplace(wordApp, "<gst>", gstAmount);
                    this.FindAndReplace(wordApp, "<totalPrice>", totalAndGst);
                }
                else
                {
                    MessageBox.Show("File not Found!");
                }
                object fileFormat = Microsoft.Office.Interop.Word.WdSaveFormat.wdFormatPDF;
                //Save as
                myWordDoc.SaveAs2(ref saveToLocation, ref fileFormat, ref missing, ref missing,
                                ref missing, ref missing, ref missing,
                                ref missing, ref missing, ref missing,
                                ref missing, ref missing, ref missing,
                                ref missing, ref missing, ref missing);

                //Do not save the changes currently made in the template
                object doNotSavedChanges = Word.WdSaveOptions.wdDoNotSaveChanges;
                myWordDoc.Close(doNotSavedChanges, ref missing, ref missing);
                wordApp.Quit(doNotSavedChanges, ref missing, ref missing);

                //Open the saved pdf 
                System.Diagnostics.Process.Start(Convert.ToString(saveToLocation) );

                MessageBox.Show("Quote has been created, opening saved pdf");
            } catch (Exception e )
            {
                MessageBox.Show("Error catch: " + e);
            }
            return;
        }


    }
}
