﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Controls;

namespace ProfileGlassHome
{
    public partial class frmCustomer : Form
    {
        private string contact_id;
        private string detail_id;
        
        private string company_name;
        private string first_name;
        private string last_name;
        private string position_title;
        private string address;
        private string address2;
        private string suburb;
        private string state;
        private string post_code;
        private string email;
        private string phone;
        private string mobile;
        private string fax;
        
        private int form_flag;
        private int save_flag = 1;
        private int edit_flag = 2;

        private bool empty_flag = false;

        private DBQueries dbquery;
        private DataTable stateTable = new DataTable();
        private dbConnect db;

        private frm_ViewCustomer _frm_viewcustomer;

        List<string> query_list = new List<string>();


        //Note: need to pass in contact_id and details_id
        public frmCustomer(frm_ViewCustomer frm_viewcustomer,string p_id, string d_id, string c, string fn, string ln, string pt, string a, string a2, string sb, string st, string pc, string p, string m, string f, int fl, string e)
        {
            InitializeComponent();

            _frm_viewcustomer = frm_viewcustomer;

            //Assign contact_id and details_id for database queries
            contact_id = p_id;
            detail_id = d_id;

            //Load a list of Australian states into the combobox
            load_state_combobox();

            //Save parameters passed in through the constructor and assign a textchanged handler
            txtCompanyName.Text = c;
            //txtCompanyName.TextChanged += new EventHandler (AllTextBoxes_TextChanged);//EventHandler(AllTextBoxes_TextChanged);

            txtFirstName.Text = fn;
            txtFirstName.TextChanged += new EventHandler(AllTextBoxes_TextChanged);//EventHandler(AllTextBoxes_TextChanged);

            txtLastName.Text = ln;
            txtLastName.TextChanged += new EventHandler(AllTextBoxes_TextChanged);//EventHandler(AllTextBoxes_TextChanged);

            txtPosition.Text = pt;
            //txt_position.TextChanged += new EventHandler(AllTextBoxes_TextChanged);//EventHandler(AllTextBoxes_TextChanged);

            txtAddress.Text = a;
            //txtAddress.TextChanged += new EventHandler(AllTextBoxes_TextChanged);//EventHandler(AllTextBoxes_TextChanged);

            txtAddress2.Text = a2;
            //txtAddress2.TextChanged += new EventHandler(AllTextBoxes_TextChanged);//EventHandler(AllTextBoxes_TextChanged);

            txtSuburb.Text = sb;
            //txtSuburb.TextChanged += new EventHandler(AllTextBoxes_TextChanged);//EventHandler(AllTextBoxes_TextChanged);

            cmbState.SelectedValue = st;

            txtPostCode.Text = pc;
            //txtPostCode.TextChanged += new EventHandler(AllTextBoxes_TextChanged);//EventHandler(AllTextBoxes_TextChanged);

            txtPhone.Text = p;
            txtPhone.TextChanged += new EventHandler(AllTextBoxes_TextChanged);//EventHandler(AllTextBoxes_TextChanged);

            txtMobile.Text = m;
            txtMobile.TextChanged += new EventHandler(AllTextBoxes_TextChanged);//EventHandler(AllTextBoxes_TextChanged);

            txtFax.Text = f;
            //txt_fax.TextChanged += new EventHandler(AllTextBoxes_TextChanged);//EventHandler(AllTextBoxes_TextChanged);

            txtEmail.Text = e;
            //txtEmail.TextChanged += new EventHandler(AllTextBoxes_TextChanged);//EventHandler(AllTextBoxes_TextChanged);

            form_flag = fl;

            if (form_flag == edit_flag)
            {
                //Loads in the 
                save_text_box_values();
            }

        }

        private void load_state_combobox()
        {
            stateTable.Columns.Add("STATE:", typeof(string));

            stateTable.Rows.Add("VIC");
            stateTable.Rows.Add("NSW");
            stateTable.Rows.Add("QLD");
            stateTable.Rows.Add("SA");
            stateTable.Rows.Add("TAS");
            stateTable.Rows.Add("WA");
            stateTable.Rows.Add("NT");
            stateTable.Rows.Add("ACT");

            this.cmbState.ValueMember = "STATE:";

            this.cmbState.DataSource = stateTable.DefaultView;
        }


        /*
        - Checks and converts some input values to the appropriate strings (strings that are appropriate for database handling)
        - Assigns all input values to a global variable to be used for database queries
         */
        private void save_text_box_values()
        {
            company_name   = convert_apostrophe(txtCompanyName.Text);
            first_name     = txtFirstName.Text;
            last_name      = txtLastName.Text;
            position_title = txtPosition.Text;
            address        = txtAddress.Text;
            address2       = txtAddress2.Text;
            suburb         = txtSuburb.Text;

            if (cmbState.SelectedValue == null)
            {
                state = "";
            }
            else
            {
                state = cmbState.SelectedValue.ToString();
            }
            
            post_code      = txtPostCode.Text;
            phone          = txtPhone.Text;
            mobile         = txtMobile.Text;
            fax            = txtFax.Text;
            email          = txtEmail.Text;
        }

        //Used to convert text that contains apostrophes to be suitable for database entries
        private string convert_apostrophe(string input)
        {
            return input.Replace("'", "''");
        }




        /* Checks whether text has been changed in the textboxes: 
         * - if New customer
         * Check if field is empty 
         * - if Existing customer (in the process of editing)
         * 
         * 
         */
        private void AllTextBoxes_TextChanged(object sender, EventArgs e)
        {
            //Set a flag to the save_click button in order to stop the user from saving their details
            //


            System.Windows.Forms.TextBox txt = (sender as System.Windows.Forms.TextBox);
            //MessageBox.Show("Textbox name: " + txt.Name + " | Textbox text:" + txt.Text);
            switch (txt.Name)
            {
                /*case "txtCompanyName":
                    //MessageBox.Show("Textbox name: " + txt.Name + " | Textbox text:" + txt.Text);
                    
                    //Check if the textbox is empty 
                    if (String.IsNullOrEmpty(txt.Text))
                    {
                        //Set empty_flag to true, color textbox red and set textbox text
                        empty_flag = true;
                        txt.BackColor = System.Drawing.Color.Red;
                    }
                    else
                    {
                        //Set empty_flag to false, this allows the customer details to be saved
                        empty_flag = false;
                        txt.BackColor = System.Drawing.Color.White;
                    }

                break;*/
                case "txtFirstName":
                    //MessageBox.Show("Textbox name: " + txt.Name + " | Textbox text:" + txt.Text);
                    
                    //Check if the textbox is empty 
                    if (String.IsNullOrEmpty(txt.Text))
                    {
                        //Set empty_flag to true, color textbox red and set textbox text
                        empty_flag = true;
                        txt.BackColor = System.Drawing.Color.Red;
                    }
                    else
                    {
                        //Set empty_flag to false, this allows the customer details to be saved
                        empty_flag = false;
                        txt.BackColor = System.Drawing.Color.White;
                    }
                break;
                case "txtLastName":
                    //MessageBox.Show("Textbox name: " + txt.Name + " | Textbox text:" + txt.Text);
                    
                    //Check if the textbox is empty 
                    if (String.IsNullOrEmpty(txt.Text))
                    {
                        //Set empty_flag to true, color textbox red and set textbox text
                        empty_flag = true;
                        txt.BackColor = System.Drawing.Color.Red;
                    }
                    else
                    {
                        //Set empty_flag to false, this allows the customer details to be saved
                        empty_flag = false;
                        txt.BackColor = System.Drawing.Color.White;
                    }
                break;
                /*case "txtAddress":
                    //MessageBox.Show("Textbox name: " + txt.Name + " | Textbox text:" + txt.Text);
                    
                    //Check if the textbox is empty 
                    if (String.IsNullOrEmpty(txt.Text))
                    {
                        //Set empty_flag to true, color textbox red and set textbox text
                        empty_flag = true;
                        txt.BackColor = System.Drawing.Color.Red;
                    }
                    else
                    {
                        //Set empty_flag to false, this allows the customer details to be saved
                        empty_flag = false;
                        txt.BackColor = System.Drawing.Color.White;
                    }
                break;*/
                /*case "txtSuburb":
                    //MessageBox.Show("Textbox name: " + txt.Name + " | Textbox text:" + txt.Text);
                    
                    //Check if the textbox is empty 
                    if (String.IsNullOrEmpty(txt.Text))
                    {
                        //Set empty_flag to true, color textbox red and set textbox text
                        empty_flag = true;
                        txt.BackColor = System.Drawing.Color.Red;
                    }
                    else
                    {
                        //Set empty_flag to false, this allows the customer details to be saved
                        empty_flag = false;
                        txt.BackColor = System.Drawing.Color.White;
                    }
                break;*/
                /*case "txtPostCode":
                    //MessageBox.Show("Textbox name: " + txt.Name + " | Textbox text:" + txt.Text);
                    
                    //Check if the textbox is empty 
                    if (String.IsNullOrEmpty(txt.Text))
                    {
                        //Set empty_flag to true, color textbox red and set textbox text
                        empty_flag = true;
                        txt.BackColor = System.Drawing.Color.Red;
                    }
                    else
                    {
                        //Set empty_flag to false, this allows the customer details to be saved
                        empty_flag = false;
                        txt.BackColor = System.Drawing.Color.White;
                    }
                break;*/
                case "txtPhone":
                    //MessageBox.Show("Textbox name: " + txt.Name + " | Textbox text:" + txt.Text);
                    
                    //Check if the textbox is empty 
                    if (String.IsNullOrEmpty(txt.Text))
                    {
                        //Set empty_flag to true, color textbox red and set textbox text
                        empty_flag = true;
                        txt.BackColor = System.Drawing.Color.Red;
                    }
                    else
                    {
                        //Set empty_flag to false, this allows the customer details to be saved
                        empty_flag = false;
                        txt.BackColor = System.Drawing.Color.White;
                    }
                break;
                case "txtMobile":
                    //MessageBox.Show("Textbox name: " + txt.Name + " | Textbox text:" + txt.Text);
                    
                    //Check if the textbox is empty 
                    if (String.IsNullOrEmpty(txt.Text))
                    {
                        //Set empty_flag to true, color textbox red and set textbox text
                        empty_flag = true;
                        txt.BackColor = System.Drawing.Color.Red;
                    }
                    else
                    {
                        //Set empty_flag to false, this allows the customer details to be saved
                        empty_flag = false;
                        txt.BackColor = System.Drawing.Color.White;
                    }
                break;
                /*case "txtEmail":
                    //MessageBox.Show("Textbox name: " + txt.Name + " | Textbox text:" + txt.Text);
                    
                    //Check if the textbox is empty 
                    if (String.IsNullOrEmpty(txt.Text))
                    {
                        //Set empty_flag to true, color textbox red and set textbox text
                        empty_flag = true;
                        txt.BackColor = System.Drawing.Color.Red;
                    }
                    else
                    {
                        //Set empty_flag to false, this allows the customer details to be saved
                        empty_flag = false;
                        txt.BackColor = System.Drawing.Color.White;
                    }
                break;*/
            }
        }

        private void btn_CustomerSave_Click(object sender, EventArgs e)
        {
            //Check if the following fields are empty
            dbquery = new DBQueries();
            db = new dbConnect();


            /*
             Check if the following important text fields are filled in:
             - First name
             - Last name
             - Phone number or Mobile
            */
            if (String.IsNullOrEmpty(txtFirstName.Text) && String.IsNullOrEmpty(txtLastName.Text) && (String.IsNullOrEmpty(txtPhone.Text) || String.IsNullOrEmpty(txtMobile.Text)))
            {
                if (String.IsNullOrEmpty(txtFirstName.Text))
                {
                    txtFirstName.BackColor = System.Drawing.Color.Red;
                }
                if (String.IsNullOrEmpty(txtLastName.Text))
                {
                    txtLastName.BackColor = System.Drawing.Color.Red;
                }

                if (String.IsNullOrEmpty(txtPhone.Text) || String.IsNullOrEmpty(txtMobile.Text))
                {
                    if (String.IsNullOrEmpty(txtPhone.Text) && String.IsNullOrEmpty(txtMobile.Text))
                    {
                        txtMobile.BackColor = System.Drawing.Color.Red;
                        txtPhone.BackColor = System.Drawing.Color.Red;
                    }
                    
                    
                    if (String.IsNullOrEmpty(txtPhone.Text) && !String.IsNullOrEmpty(txtMobile.Text))
                    {
                        txtPhone.BackColor = System.Drawing.Color.White;
                    }
                    

                    if (String.IsNullOrEmpty(txtMobile.Text) && !String.IsNullOrEmpty(txtPhone.Text))
                    {
                        txtMobile.BackColor = System.Drawing.Color.White;
                    }
                    
                }
                
                empty_flag = true;
            }

            if (empty_flag == false)
            {
                //Insert new entry into the database
                if (form_flag == save_flag)
                {
                    DialogResult option_result = MessageBox.Show("Are the customer's details correct? \n\nDo you want to continue?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (option_result.ToString() == "Yes")
                    {
                        //For the moment we'll set the current record flag = 1
                        string iscurrent = Convert.ToString('1');
                        
                        //Assign 
                        save_text_box_values();
                        
                        query_list.Add(dbquery.customer_query_save_1(company_name, first_name, last_name, position_title, iscurrent));
                        query_list.Add(dbquery.customer_query_save_2(address, address2, suburb, state, post_code, phone, mobile, fax, email, iscurrent));
                        query_list.Add(dbquery.customer_query_save_contact_id);
                        query_list.Add(dbquery.customer_query_save_detail_id);
                        query_list.Add(dbquery.customer_query_save_3()); //remove get_contact_id and get_detail_id does not do anything
                        int result = db.Save_New_Customer(query_list);

                        query_list.Clear();

                        //MessageBox.Show("Database rows that have been affected: " + result);
                        
                        if(result == 0)
                        {
                            MessageBox.Show(" Database Insert Query has failed ");
                        }

                        //Give the user the option to create a new quote or exit
                        DialogResult new_quote_option = MessageBox.Show("Do you want to create a new quote for this customer?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (new_quote_option.ToString() == "Yes")
                        {
                            //Create a new quotation based on the customer selected in the table
                            //Quotation q = new Quotation(company_name, first_name + " " + last_name);
                            //q.ShowDialog();
                        }

                    }

                }

                //Update the current customer entry in the database
                if (form_flag == edit_flag)
                {
                   
                    DialogResult option_result = MessageBox.Show("Make sure your changes are correct. \nDo you want to accept the changes?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    
                    if (option_result.ToString() == "Yes")
                    {
                        dbquery = new DBQueries();
                        db = new dbConnect();

                        string iscurrent = Convert.ToString('1');
                        save_text_box_values();
                        //MessageBox.Show(" company_name: " + company_name + " first_name: " + first_name + " last_name: " + last_name + " position_title: " + position_title);
                        //MessageBox.Show(" address: " + address + " address2: " + address2 + " suburb: " + suburb + " state: " + state + " post_code:" + post_code + " phone: " + phone + " mobile: " + mobile + " fax: " + fax + " email: " + email);
                        
                        //Insert into contacts
                        query_list.Add(dbquery.customer_query_edit_1(company_name, first_name, last_name, position_title, iscurrent));
                        
                        //Insert into details
                        query_list.Add(dbquery.customer_query_edit_2(address, address2, suburb, state, post_code, phone, mobile, fax, email, iscurrent));

                        //Update contacts
                        db.get_contact_id = contact_id; //Set the property for contact_id obtained from the DataTable
                        query_list.Add(dbquery.customer_query_edit_3(contact_id));

                        //Update details
                        db.get_detail_id = detail_id; //Set the property for detail_id obtained from the DataTable
                        query_list.Add(dbquery.customer_query_edit_4(detail_id));

                        //Select max contact_id from contacts
                        query_list.Add(dbquery.customer_query_edit_5());

                        //Select max detail_id from details
                        query_list.Add(dbquery.customer_query_edit_6());

                        //Insert into contact_details
                        query_list.Add(dbquery.customer_query_edit_7(db.get_contact_id, db.get_detail_id));


                        int result = db.Edit_Existing_Customer(query_list);

                        query_list.Clear();


                        //MessageBox.Show("Database rows that have been affected: " + result);

                        if (result == 0)
                        {
                            MessageBox.Show(" Database Insert Query has failed ");
                        }

                        
                    }
                }


                //Refresh the datagridview
                _frm_viewcustomer.refreshDataGridView();

                //Close the form
                this.Close();

            }
            else
            {
                MessageBox.Show(" Please fill in the mandatory fields that are coloured in red ");
            }
            
            
        }


        private void btn_CustomerClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }


    }
}
