﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProfileGlassHome
{
    public partial class frm_ViewCustomer : Form
    {

        private BindingSource bindingSource1 = new BindingSource();

        DataTable table;
        AutoCompleteStringCollection autoCustomer;
        dbConnect db;
        DBQueries dbquery;

        private string contact_id;
        private string detail_id;
        private string company_name;
        private string first_name;
        private string last_name;
        private string position_title;
        private string address;
        private string address2;
        private string suburb;
        private string state;
        private string post_code;
        private string phone;
        private string mobile;
        private string fax;
        private string email;

        private string txt_FindPerson_input;

        //Flags used to distinguish between a new and edit customer selection
        private int save_flag = 1;
        private int edit_flag = 2;

        enum Details { ContactID, DetailID, Company, FirstName, LastName, PositionTitle, Address, Address2, Suburb, State, PostCode, Phone, Mobile, Fax, Email };

        public frm_ViewCustomer()
        {
            FlowLayoutPanel panel = new FlowLayoutPanel();
            panel.Dock = DockStyle.Fill;
            panel.AutoSize = true;
            this.Controls.AddRange(new Control[] { dg_ViewPersonPersonList});
            this.Load += new System.EventHandler(frm_ViewCustomer_Load);
            this.Text = "GridView databinding";
            InitializeComponent();
        }

        private void frm_ViewCustomer_Load(object sender, System.EventArgs e)
        {
            // Bind the DataGridView to the BindingSource
            dg_ViewPersonPersonList.DataSource = bindingSource1;
            populateGrid();
            setUpTextBoxAutoComplete();
        }

        private void setUpTextBoxAutoComplete()
        {
            try
            {
                //ViewPersonFindPerson textbox autocomplete 
                autoCustomer = new AutoCompleteStringCollection();

                foreach (DataGridViewRow row in dg_ViewPersonPersonList.Rows)
                {
                    try
                    {
                        string _company = row.Cells[(int)Details.Company].Value.ToString();
                        string _first_name = row.Cells[(int)Details.FirstName].Value.ToString();
                        string _last_name = row.Cells[(int)Details.LastName].Value.ToString();
                        string placeholder = " | " + _company + " , " + _first_name + " " + _last_name;
                        //Search - company -> first name , last name
                        autoCustomer.Add(_company + placeholder); //+ " | Name: " + row.Cells[1].Value.ToString() + " " + row.Cells[2].Value.ToString());
                        txt_ViewPersonFindPerson.MaxLength = 40;

                        //Search - first name -> last name , company
                        autoCustomer.Add(_first_name + placeholder);// + " " + row.Cells[2].Value.ToString() + " | Company: " + row.Cells[0].Value.ToString());
                        txt_ViewPersonFindPerson.MaxLength = 40;

                        //Search - last name -> first name , company
                        autoCustomer.Add(_last_name + placeholder);// + " " + row.Cells[1].Value.ToString() + " | Company: " + row.Cells[0].Value.ToString());
                        txt_ViewPersonFindPerson.MaxLength = 40;
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Exception:" + row.Index + " " + row.Cells.Count + " Possible issue with data ");
                        break;
                    }
                }

                txt_ViewPersonFindPerson.Clear();
                txt_ViewPersonFindPerson.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                txt_ViewPersonFindPerson.AutoCompleteSource = AutoCompleteSource.CustomSource;
                txt_ViewPersonFindPerson.AutoCompleteCustomSource = autoCustomer;
            } catch (Exception)
            {
                MessageBox.Show("Error has occurred");
            }
        }

        private void populateGrid()
        {
            try
            {
                db = new dbConnect();
                dbquery = new DBQueries();

                // Populate the new data table and bind it to the BindingSource.
                table = new DataTable();

                int result = db.Select(dbquery.view_customer_query_select);

                if (result == 0)
                {

                    table.Locale = System.Globalization.CultureInfo.InvariantCulture;

                    db.get_SqlDataAdapter.Fill(table);
                    
                    bindingSource1.DataSource = table;

                    // Resize the DataGridView columns to fit the new data source content.
                    dg_ViewPersonPersonList.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);


                    //Hide the first two columns p.contact_id and d.detail_id
                    this.dg_ViewPersonPersonList.Columns["contact_id"].Visible = false;
                    this.dg_ViewPersonPersonList.Columns["detail_id"].Visible = false;

                    this.dg_ViewPersonPersonList.ClearSelection();

 
                    
                    
                } else
                {
                    MessageBox.Show(" Database Select Query has failed ");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("D'Oh... Something went wrong!");
            }
        }


        private void btn_ViewPersonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //Double Clicking the customer opens up a new quotation
        //Changed to -> clicking a customer opens up a customer profile where the a new quote can be made, or a quote can be edited 
        private void dg_ViewPersonPersonList_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {

            try
            {
                if (e.RowIndex >= 0)
                {

                    DataGridViewRow row = this.dg_ViewPersonPersonList.Rows[e.RowIndex];
                    contact_id = row.Cells[(int)Details.ContactID].Value.ToString();
                    detail_id = row.Cells[(int)Details.DetailID].Value.ToString();
                    company_name = row.Cells[(int)Details.Company].Value.ToString();
                    first_name = row.Cells[(int)Details.FirstName].Value.ToString();
                    last_name = row.Cells[(int)Details.LastName].Value.ToString();
                    position_title = row.Cells[(int)Details.PositionTitle].Value.ToString();
                    address = row.Cells[(int)Details.Address].Value.ToString();
                    address2 = row.Cells[(int)Details.Address2].Value.ToString();
                    suburb = row.Cells[(int)Details.Suburb].Value.ToString();
                    state = row.Cells[(int)Details.State].Value.ToString();
                    post_code = row.Cells[(int)Details.PostCode].Value.ToString();
                    phone = row.Cells[(int)Details.Phone].Value.ToString();
                    mobile = row.Cells[(int)Details.Mobile].Value.ToString();
                    fax = row.Cells[(int)Details.Fax].Value.ToString();
                    email = row.Cells[(int)Details.Email].Value.ToString();

                    //Opens up the customer profile
                    Customer_Profile customer_profile = new Customer_Profile(contact_id,
                                                                            detail_id,
                                                                            company_name,
                                                                            first_name,
                                                                            last_name,
                                                                            position_title,
                                                                            address,
                                                                            address2,
                                                                            suburb,
                                                                            state,
                                                                            post_code,
                                                                            phone,
                                                                            mobile,
                                                                            fax,
                                                                            email);
                    customer_profile.ShowDialog();

                }
            }
            catch (Exception me)
            {
                MessageBox.Show(me.ToString());
            }
        }

        private void txt_ViewPersonFindPerson_TextChanged(object sender, EventArgs e)
        {
            if(txt_ViewPersonFindPerson.Text == "" )
            {
                refreshDataGridView();
                //MessageBox.Show("TextChange" + txt_ViewPersonFindPerson.Text);
            }
        }

        //Upon 'Enter' key press/ 'Click', search and refresh the datagridview table
        private void txt_ViewPersonFindPerson_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    //Reset the grid after getting the text input
                    refreshDataGridView();

                    //Acquire the name from textbox input
                    txt_FindPerson_input = txt_ViewPersonFindPerson.Text;

                    //Removes the placeholder part of the string
                    int index = txt_FindPerson_input.LastIndexOf("|");
                    if(index > 0)
                    {
                        txt_FindPerson_input = txt_FindPerson_input.Substring(0, index);

                        //Removes any unecessary spaces within the string
                        txt_FindPerson_input = txt_FindPerson_input.Trim().ToLower();
                    }
                    //MessageBox.Show("After removed textbox input: " + txt_FindPerson_input);
                    
                    //Go through each row in the table row and hide the row that does not equate to the search term provided
                    foreach (DataGridViewRow row in dg_ViewPersonPersonList.Rows)
                    {

                        //MessageBox.Show("" + txt_FindPerson_input.IndexOf(row.Cells[(int)Details.Company].Value.ToString(), 0, StringComparison.CurrentCultureIgnoreCase));
                        string _company = row.Cells[(int)Details.Company].Value.ToString().ToLower();
                        string _first_name = row.Cells[(int)Details.FirstName].Value.ToString().ToLower();
                        string _last_name = row.Cells[(int)Details.LastName].Value.ToString().ToLower();
                        //MessageBox.Show(_company + _first_name + _last_name);
                        if (!_company.Contains(txt_FindPerson_input))// || _company.Contains(" ") ) //|| txt_FindPerson_input.Contains(_first_name) || txt_FindPerson_input.Contains(_last_name) )
                        {
                            //MessageBox.Show("Row: " + _company + " " + _first_name + " " + _last_name + " Text input: " + txt_FindPerson_input);
                            //MessageBox.Show("Company: " + _company);
                            if (!_first_name.Contains(txt_FindPerson_input) || _first_name.Contains(" "))
                            {
                                //MessageBox.Show("FirstName: " + _first_name);
                                if (!_last_name.Contains(txt_FindPerson_input) || _last_name.Contains(" "))
                                {
                                    //MessageBox.Show("LastName: " + _last_name);
                                    CurrencyManager currencyManager1 = (CurrencyManager)BindingContext[dg_ViewPersonPersonList.DataSource];
                                    currencyManager1.SuspendBinding();
                                    row.Visible = false;
                                    currencyManager1.ResumeBinding();
                                }
                            }

                        } /*else
                        {
                            MessageBox.Show("Is Company: " + _company + " Text input: " + txt_FindPerson_input);
                        }*/


                        //Check if inputted text is the same as the company name existing within the dataset, if not then continue
                        /*if (txt_FindPerson_input.IndexOf(row.Cells[(int)Details.Company].Value.ToString(), 0, StringComparison.CurrentCultureIgnoreCase) && txt_FindPerson_input.IndexOf(row.Cells[(int)Details.FirstName].Value.ToString(), 0, StringComparison.CurrentCultureIgnoreCase) &&
                            txt_FindPerson_input.IndexOf(row.Cells[(int)Details.LastName].Value.ToString(), 0, StringComparison.CurrentCultureIgnoreCase) == -1)
                        {
                            MessageBox.Show("Company: " + row.Cells[(int)Details.Company].Value.ToString());

                            //Check if inputted text is the same as the first name existing within the dataset, if not then continue
                            if (txt_FindPerson_input.IndexOf(row.Cells[(int)Details.FirstName].Value.ToString(), 0, StringComparison.CurrentCultureIgnoreCase) == -1)
                            {
                                MessageBox.Show("FirstName: " + row.Cells[(int)Details.FirstName].Value.ToString());
                                //Check if inputted text is the same as the last name existing within the dataset, if not then continue
                                if (txt_FindPerson_input.IndexOf(row.Cells[(int)Details.LastName].Value.ToString(), 0, StringComparison.CurrentCultureIgnoreCase) == -1)
                                {
                                    MessageBox.Show("LastName: " + row.Cells[(int)Details.LastName].Value.ToString());
                                    CurrencyManager currencyManager1 = (CurrencyManager)BindingContext[dg_ViewPersonPersonList.DataSource];
                                    currencyManager1.SuspendBinding();
                                    row.Visible = false;
                                    currencyManager1.ResumeBinding();
                                }
                            }
                        }*/
                    }
                }

            }
            catch
            {
                MessageBox.Show("Unknown error has occurred");
            }

        }

        private void btn_CustomerNewAdd_Click(object sender, EventArgs e)
        {
            //DialogResult option_result = MessageBox.Show("Did you find the contact/company using the searchbar? \nWould you like to create a new customer?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            //if (option_result.ToString() == "Yes")
            //{
                frmCustomer frm_c = new frmCustomer(this,contact_id,detail_id,"", "", "", "", "","", "", "", "", "", "", "", save_flag, "");
                frm_c.ShowDialog();
            //}
        }

        //Allows user to edit the customer profile
        private void btn_CustomerEdit_Click(object sender, EventArgs e)
        {
            if (this.dg_ViewPersonPersonList.CurrentRow != null)
            {
                int rowIndex = this.dg_ViewPersonPersonList.CurrentCell.RowIndex;

                DataGridViewRow row = this.dg_ViewPersonPersonList.Rows[rowIndex];
                contact_id = row.Cells[(int)Details.ContactID].Value.ToString();
                detail_id = row.Cells[(int)Details.DetailID].Value.ToString();
                company_name = row.Cells[(int)Details.Company].Value.ToString();
                first_name = row.Cells[(int)Details.FirstName].Value.ToString();
                last_name = row.Cells[(int)Details.LastName].Value.ToString();
                position_title = row.Cells[(int)Details.PositionTitle].Value.ToString();
                address = row.Cells[(int)Details.Address].Value.ToString();
                address2 = row.Cells[(int)Details.Address2].Value.ToString();
                suburb = row.Cells[(int)Details.Suburb].Value.ToString();
                state = row.Cells[(int)Details.State].Value.ToString();
                post_code = row.Cells[(int)Details.PostCode].Value.ToString();
                phone = row.Cells[(int)Details.Phone].Value.ToString();
                mobile = row.Cells[(int)Details.Mobile].Value.ToString();
                fax = row.Cells[(int)Details.Fax].Value.ToString();
                email = row.Cells[(int)Details.Email].Value.ToString();

                frmCustomer frm_c = new frmCustomer(this,contact_id, detail_id, company_name, first_name, last_name, position_title, address, address2, suburb, state, post_code, phone, mobile, fax, edit_flag, email);
                frm_c.ShowDialog();
            }
            else
            {
                MessageBox.Show("No selection");
            }
        }

        private void btn_Customer_Refresh_Click(object sender, EventArgs e)
        {
            refreshDataGridView();
        }

        public void refreshDataGridView()
        {
            dg_ViewPersonPersonList.DataSource = bindingSource1;
            populateGrid();
        }

    }
}
