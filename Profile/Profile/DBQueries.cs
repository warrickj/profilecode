﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProfileGlassHome
{
    //Contains a list of database queries
    public class DBQueries
    {

        private string query_select = "SELECT p.contact_id " +
                                    " ,d.detail_id " +
                                    " ,company_name " +
                                    "  ,first_name " +
                                    "  ,last_name " +
                                    "  ,position_title " +
                                    "  ,address " +
                                    "  ,address2 " +
                                    "  ,suburb " +
                                    "  ,state " +
                                    "  ,postcode" +
                                    "  ,phone" +
                                    "  ,mobile " +
                                    "  ,fax  " +
                                    "  ,email " +
                                "  FROM contacts p " +
                                " INNER JOIN contact_details pd on p.contact_id = pd.contact_id" +
                                " INNER JOIN details d on pd.detail_id = d.detail_id" +
                                " WHERE d.iscurrent = 1" +
                                "   AND p.iscurrent = 1";

        public string view_customer_query_select
        {
            get
            {
                return query_select;
            }
        }


        public string customer_query_save_contact_id = "SELECT max(contact_id) FROM contacts";
        public string customer_query_save_detail_id = "SELECT max(detail_id) FROM details";

        public string customer_query_save_1( string company_name, string first_name, string last_name, string position_title, string iscurrent)
        {
           return "                          " +
                  " INSERT INTO contacts              " +
                  " (      company_name             " +
                  "      , first_name               " +
                  "      , last_name                " +
                  "      , position_title           " +
                  "      , iscurrent                " +
                  "      , created_by               " +
                  "      , created_date             " +
                  "      , modified_by              " +
                  "      , modified_date            " +
                  " )                               " +
                 "VALUES                            " +
                 "(     '" + company_name      + "'" +
                 "   ,'" + first_name          + "'" +
                 "   ,'" + last_name           + "'" +
                 "   ,'" + position_title      + "'" +
                 "     , 1                          " +
                 "     , 1                          " +
                 "   ,   getdate()                  " +
                 "     , 1                          " +
                 "   ,  getdate()                   " +
                 ")                                 ";
        }

        public string customer_query_save_2(string address, string address2, string suburb, string state, 
                                    string postcode, string phone, string mobile, string fax, string email, string iscurrent)
        {
            return "                           " +
                " INSERT INTO details               " +
                " (      address                    " +
                "      , address2                   " +
                "      , suburb                     " +
                "      , state                      " +
                "      , postcode                   " +
                "      , phone                      " +
                "      , mobile                     " +
                "      , fax                        " +
                "      , email                      " +
                "      , iscurrent                  " +
                "      , created_by                 " +
                "      , created_date               " +
                "      , modified_by                " +
                "      , modified_date              " +
                " )                                 " +
                " VALUES                            " +
                " (   '" + address              +"'" +
                "    ,'" + address2             +"'" +
                "    ,'" + suburb               +"'" +
                "    ,'" + state                +"'" +
                "    ,'" + postcode             +"'" + //note: Problem occurs when more than 2 numbers are given for this
                "    ,'" + phone                +"'" + 
                "    ,'" + mobile               +"'" +
                "    ,'" + fax                  +"'" +
                "    ,'" + email                +"'" +
                "     , 1                          " +
                 "     , 1                          " +
                 "   ,   getdate()                  " +
                 "     , 1                          " +
                 "   ,  getdate()                   " +
                 ")                                 ";
        }

        public string customer_query_save_3()
        {
            return
                " INSERT INTO contact_details        " +
                "      ( contact_id                  " +
                "      , detail_id                  " +
                "      , created_by                 " +
                "      , created_date               " +
                "      , modified_by                " +
                "      , modified_date              " +
                "      )                            " +
                " VALUES                            " +
                " (     @contact_id                  " +
                "   ,   @detail_id                  " +
                "   ,   1                           " +
                "   ,   getdate()                   " +
                "   ,   1                           " +
                "   ,   getdate()                   " +
                "      )                            ";

        }




        //Query update 1 - Creates a new record for contacts
        public string customer_query_edit_1( string company_name, string first_name, string last_name, string position_title, string iscurrent)
        {
           return "                          " +
                  " INSERT INTO contacts              " +
                  " (      company_name             " +
                  "      , first_name               " +
                  "      , last_name                " +
                  "      , position_title           " +
                  "      , iscurrent                " +
                  "      , created_by               " +
                  "      , created_date             " +
                  "      , modified_by              " +
                  "      , modified_date            " +
                  " )                               " +
                 "VALUES                            " +
                 "(     '" + company_name      + "'" +
                 "   ,'" + first_name          + "'" +
                 "   ,'" + last_name           + "'" +
                 "   ,'" + position_title      + "'" +
                 "     , 1                          " +
                 "     , 1                          " +
                 "   ,   getdate()                  " +
                 "     , 1                          " +
                 "   ,  getdate()                   " +
                 ")                                 ";
        }

        //Query update 2 - Creates a new record for details
        public string customer_query_edit_2(string address, string address2, string suburb, string state, 
                                    string postcode, string phone, string mobile, string fax, string email, string iscurrent)
        {
            return 
                " INSERT INTO details               " +
                " (      address                    " +
                "      , address2                   " +
                "      , suburb                     " +
                "      , state                      " +
                "      , postcode                   " +
                "      , phone                      " +
                "      , mobile                     " +
                "      , fax                        " +
                "      , email                      " +
                "      , iscurrent                  " +
                "      , created_by                 " +
                "      , created_date               " +
                "      , modified_by                " +
                "      , modified_date              " +
                " )                                 " +
                " VALUES                            " +
                " (   '" + address              +"'" +
                "    ,'" + address2             +"'" +
                "    ,'" + suburb               +"'" +
                "    ,'" + state                +"'" +
                "    ,'" + postcode             +"'" +
                "    ,'" + phone                +"'" + 
                "    ,'" + mobile               +"'" +
                "    ,'" + fax                  +"'" +
                "    ,'" + email                +"'" +
                "    , 1                          " +
                "    , 1                           " +
                 "   , getdate()                  " +
                 "   , 1                          " +
                 "   , getdate()                   " +
                "      )                            " ;
        }



        //Query update 3 - Updates the contact_id in the contacts table
        public string customer_query_edit_3(string contact_id)
        {
            return  " UPDATE contacts                 " +
                    "    SET iscurrent   = 0        " +
                    "  WHERE contact_id = @contact_id ";
                    

        }
        
        //Query update 4 - Updates the contact_id in the details table
        public string customer_query_edit_4(string detail_id)
        {
            return  " UPDATE  details                   " +
                    "    SET iscurrent   = 0            " +
                    "  WHERE detail_id = @detail_id "; 

        }



        //Query update 3 - Selects the value from the record that contains the contact_id and detail_id
        public string customer_query_edit_5()
        {
             return " SELECT MAX(contact_id)             " +
                    "   FROM contacts                     " ;

        }


        //Query update 4 - Selects the value from the record that contains the contact_id and detail_id
        public string customer_query_edit_6()
        {
             return " SELECT MAX(detail_id)             " +
                    "   FROM details                    " ;

        }

        //Query update 5 - Insert the record that contains the contact_id and detail_id
        public string customer_query_edit_7(string contact_id, string detail_id)
        {
            return                  " INSERT INTO contact_details        " +
                                    "      ( contact_id                  " +
                                    "      , detail_id                  " +
                                    "      , created_by                 " +
                                    "      , created_date               " +
                                    "      , modified_by                " +
                                    "      , modified_date              " +
                                    "      )                            " +
                                    " VALUES                            " +
                                    " (      @contact_id                 " +
                                    "      , @detail_id                 " +
                                    "      , 1                          " +
                                    "      , getdate() "                  +
                                    "      , 1                          " +
                                    "      , getdate() "                  +
                                    "      )                            " ;
        }

        //public string get_quotations(int contact_id_int)
        //{
        //    return "SELECT qh.quotation_num                                                   " +
        //           "      ,qh.version                                                         " +
        //           "      ,qh.quotation_amount                                                " +
        //           "      ,qh.quotation_date                                                  " +
        //           "      ,qh.description                                                     " +
        //           "      , l.meaning                                                         " +
        //           "  FROM quotation_headers qh                                               " +
        //           " INNER JOIN quotation_lines ql ON qh.[QUOTATION_ID] = ql.[QUOTATION_ID]   " +
        //           " INNER JOIN lookups l ON l.lookup_id = qh.payment_status_flag             " +
        //           " INNER JOIN contacts c ON c.contact_id = qh.contact_id                    " +
        //           " WHERE qh.[contact_id] = " + contact_id;
        //}


 
    }
}
