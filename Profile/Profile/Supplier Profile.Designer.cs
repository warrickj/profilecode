﻿namespace ProfileGlassHome
{
    partial class Supplier_Profile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbl_customer_profile_position_title = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbl_customer_profile_fax = new System.Windows.Forms.Label();
            this.lbl_customer_profile_mobile = new System.Windows.Forms.Label();
            this.lbl_customer_profile_phone = new System.Windows.Forms.Label();
            this.lbl_customer_profile_email = new System.Windows.Forms.Label();
            this.lbl_customer_profile_postcode = new System.Windows.Forms.Label();
            this.lbl_customer_profile_state = new System.Windows.Forms.Label();
            this.lbl_customer_profile_suburb = new System.Windows.Forms.Label();
            this.lbl_customer_profile_address2 = new System.Windows.Forms.Label();
            this.lbl_customer_profile_address = new System.Windows.Forms.Label();
            this.lbl_customer_profile_lastname = new System.Windows.Forms.Label();
            this.lbl_customer_profile_firstname = new System.Windows.Forms.Label();
            this.lbl_customer_profile_company = new System.Windows.Forms.Label();
            this.lbl_address2 = new System.Windows.Forms.Label();
            this.lbl_email = new System.Windows.Forms.Label();
            this.lbl_fax = new System.Windows.Forms.Label();
            this.lbl_mobile = new System.Windows.Forms.Label();
            this.lbl_phone = new System.Windows.Forms.Label();
            this.lbl_postcode = new System.Windows.Forms.Label();
            this.lbl_state = new System.Windows.Forms.Label();
            this.lbl_suburb = new System.Windows.Forms.Label();
            this.lbl_address = new System.Windows.Forms.Label();
            this.lbl_position = new System.Windows.Forms.Label();
            this.lbl_last_name = new System.Windows.Forms.Label();
            this.lbl_first_name = new System.Windows.Forms.Label();
            this.lbl_company_name = new System.Windows.Forms.Label();
            this.btn_edit_purchase_order = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dg_purchase_order_list = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_new_purchase_order = new System.Windows.Forms.Button();
            this.btn_close = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_purchase_order_list)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.lbl_customer_profile_position_title);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.lbl_customer_profile_fax);
            this.groupBox1.Controls.Add(this.lbl_customer_profile_mobile);
            this.groupBox1.Controls.Add(this.lbl_customer_profile_phone);
            this.groupBox1.Controls.Add(this.lbl_customer_profile_email);
            this.groupBox1.Controls.Add(this.lbl_customer_profile_postcode);
            this.groupBox1.Controls.Add(this.lbl_customer_profile_state);
            this.groupBox1.Controls.Add(this.lbl_customer_profile_suburb);
            this.groupBox1.Controls.Add(this.lbl_customer_profile_address2);
            this.groupBox1.Controls.Add(this.lbl_customer_profile_address);
            this.groupBox1.Controls.Add(this.lbl_customer_profile_lastname);
            this.groupBox1.Controls.Add(this.lbl_customer_profile_firstname);
            this.groupBox1.Controls.Add(this.lbl_customer_profile_company);
            this.groupBox1.Controls.Add(this.lbl_address2);
            this.groupBox1.Controls.Add(this.lbl_email);
            this.groupBox1.Controls.Add(this.lbl_fax);
            this.groupBox1.Controls.Add(this.lbl_mobile);
            this.groupBox1.Controls.Add(this.lbl_phone);
            this.groupBox1.Controls.Add(this.lbl_postcode);
            this.groupBox1.Controls.Add(this.lbl_state);
            this.groupBox1.Controls.Add(this.lbl_suburb);
            this.groupBox1.Controls.Add(this.lbl_address);
            this.groupBox1.Controls.Add(this.lbl_position);
            this.groupBox1.Controls.Add(this.lbl_last_name);
            this.groupBox1.Controls.Add(this.lbl_first_name);
            this.groupBox1.Controls.Add(this.lbl_company_name);
            this.groupBox1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(163, 316);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Contact Details";
            // 
            // lbl_customer_profile_position_title
            // 
            this.lbl_customer_profile_position_title.AutoSize = true;
            this.lbl_customer_profile_position_title.Location = new System.Drawing.Point(72, 81);
            this.lbl_customer_profile_position_title.Name = "lbl_customer_profile_position_title";
            this.lbl_customer_profile_position_title.Size = new System.Drawing.Size(35, 13);
            this.lbl_customer_profile_position_title.TabIndex = 18;
            this.lbl_customer_profile_position_title.Text = "label3";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "Position Title:";
            // 
            // lbl_customer_profile_fax
            // 
            this.lbl_customer_profile_fax.AutoSize = true;
            this.lbl_customer_profile_fax.Location = new System.Drawing.Point(72, 285);
            this.lbl_customer_profile_fax.Name = "lbl_customer_profile_fax";
            this.lbl_customer_profile_fax.Size = new System.Drawing.Size(41, 13);
            this.lbl_customer_profile_fax.TabIndex = 16;
            this.lbl_customer_profile_fax.Text = "label13";
            // 
            // lbl_customer_profile_mobile
            // 
            this.lbl_customer_profile_mobile.AutoSize = true;
            this.lbl_customer_profile_mobile.Location = new System.Drawing.Point(72, 262);
            this.lbl_customer_profile_mobile.Name = "lbl_customer_profile_mobile";
            this.lbl_customer_profile_mobile.Size = new System.Drawing.Size(41, 13);
            this.lbl_customer_profile_mobile.TabIndex = 15;
            this.lbl_customer_profile_mobile.Text = "label12";
            // 
            // lbl_customer_profile_phone
            // 
            this.lbl_customer_profile_phone.AutoSize = true;
            this.lbl_customer_profile_phone.Location = new System.Drawing.Point(72, 236);
            this.lbl_customer_profile_phone.Name = "lbl_customer_profile_phone";
            this.lbl_customer_profile_phone.Size = new System.Drawing.Size(41, 13);
            this.lbl_customer_profile_phone.TabIndex = 14;
            this.lbl_customer_profile_phone.Text = "label11";
            // 
            // lbl_customer_profile_email
            // 
            this.lbl_customer_profile_email.AutoSize = true;
            this.lbl_customer_profile_email.Location = new System.Drawing.Point(72, 213);
            this.lbl_customer_profile_email.Name = "lbl_customer_profile_email";
            this.lbl_customer_profile_email.Size = new System.Drawing.Size(41, 13);
            this.lbl_customer_profile_email.TabIndex = 13;
            this.lbl_customer_profile_email.Text = "label10";
            // 
            // lbl_customer_profile_postcode
            // 
            this.lbl_customer_profile_postcode.AutoSize = true;
            this.lbl_customer_profile_postcode.Location = new System.Drawing.Point(72, 193);
            this.lbl_customer_profile_postcode.Name = "lbl_customer_profile_postcode";
            this.lbl_customer_profile_postcode.Size = new System.Drawing.Size(35, 13);
            this.lbl_customer_profile_postcode.TabIndex = 12;
            this.lbl_customer_profile_postcode.Text = "label9";
            // 
            // lbl_customer_profile_state
            // 
            this.lbl_customer_profile_state.AutoSize = true;
            this.lbl_customer_profile_state.Location = new System.Drawing.Point(72, 172);
            this.lbl_customer_profile_state.Name = "lbl_customer_profile_state";
            this.lbl_customer_profile_state.Size = new System.Drawing.Size(35, 13);
            this.lbl_customer_profile_state.TabIndex = 11;
            this.lbl_customer_profile_state.Text = "label8";
            // 
            // lbl_customer_profile_suburb
            // 
            this.lbl_customer_profile_suburb.AutoSize = true;
            this.lbl_customer_profile_suburb.Location = new System.Drawing.Point(72, 148);
            this.lbl_customer_profile_suburb.Name = "lbl_customer_profile_suburb";
            this.lbl_customer_profile_suburb.Size = new System.Drawing.Size(35, 13);
            this.lbl_customer_profile_suburb.TabIndex = 10;
            this.lbl_customer_profile_suburb.Text = "label7";
            // 
            // lbl_customer_profile_address2
            // 
            this.lbl_customer_profile_address2.AutoSize = true;
            this.lbl_customer_profile_address2.Location = new System.Drawing.Point(72, 124);
            this.lbl_customer_profile_address2.Name = "lbl_customer_profile_address2";
            this.lbl_customer_profile_address2.Size = new System.Drawing.Size(35, 13);
            this.lbl_customer_profile_address2.TabIndex = 9;
            this.lbl_customer_profile_address2.Text = "label6";
            // 
            // lbl_customer_profile_address
            // 
            this.lbl_customer_profile_address.AutoSize = true;
            this.lbl_customer_profile_address.Location = new System.Drawing.Point(72, 101);
            this.lbl_customer_profile_address.Name = "lbl_customer_profile_address";
            this.lbl_customer_profile_address.Size = new System.Drawing.Size(35, 13);
            this.lbl_customer_profile_address.TabIndex = 8;
            this.lbl_customer_profile_address.Text = "label5";
            // 
            // lbl_customer_profile_lastname
            // 
            this.lbl_customer_profile_lastname.AutoSize = true;
            this.lbl_customer_profile_lastname.Location = new System.Drawing.Point(72, 61);
            this.lbl_customer_profile_lastname.Name = "lbl_customer_profile_lastname";
            this.lbl_customer_profile_lastname.Size = new System.Drawing.Size(35, 13);
            this.lbl_customer_profile_lastname.TabIndex = 7;
            this.lbl_customer_profile_lastname.Text = "label4";
            // 
            // lbl_customer_profile_firstname
            // 
            this.lbl_customer_profile_firstname.AutoSize = true;
            this.lbl_customer_profile_firstname.Location = new System.Drawing.Point(72, 38);
            this.lbl_customer_profile_firstname.Name = "lbl_customer_profile_firstname";
            this.lbl_customer_profile_firstname.Size = new System.Drawing.Size(35, 13);
            this.lbl_customer_profile_firstname.TabIndex = 6;
            this.lbl_customer_profile_firstname.Text = "label3";
            // 
            // lbl_customer_profile_company
            // 
            this.lbl_customer_profile_company.AutoSize = true;
            this.lbl_customer_profile_company.Location = new System.Drawing.Point(72, 16);
            this.lbl_customer_profile_company.Name = "lbl_customer_profile_company";
            this.lbl_customer_profile_company.Size = new System.Drawing.Size(35, 13);
            this.lbl_customer_profile_company.TabIndex = 5;
            this.lbl_customer_profile_company.Text = "label2";
            // 
            // lbl_address2
            // 
            this.lbl_address2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lbl_address2.AutoSize = true;
            this.lbl_address2.Location = new System.Drawing.Point(6, 124);
            this.lbl_address2.Name = "lbl_address2";
            this.lbl_address2.Size = new System.Drawing.Size(57, 13);
            this.lbl_address2.TabIndex = 0;
            this.lbl_address2.Text = "Address 2:";
            // 
            // lbl_email
            // 
            this.lbl_email.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbl_email.AutoSize = true;
            this.lbl_email.Location = new System.Drawing.Point(8, 213);
            this.lbl_email.Name = "lbl_email";
            this.lbl_email.Size = new System.Drawing.Size(35, 13);
            this.lbl_email.TabIndex = 0;
            this.lbl_email.Text = "eMail:";
            // 
            // lbl_fax
            // 
            this.lbl_fax.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbl_fax.AutoSize = true;
            this.lbl_fax.Location = new System.Drawing.Point(7, 285);
            this.lbl_fax.Name = "lbl_fax";
            this.lbl_fax.Size = new System.Drawing.Size(27, 13);
            this.lbl_fax.TabIndex = 0;
            this.lbl_fax.Text = "Fax:";
            // 
            // lbl_mobile
            // 
            this.lbl_mobile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbl_mobile.AutoSize = true;
            this.lbl_mobile.Location = new System.Drawing.Point(7, 262);
            this.lbl_mobile.Name = "lbl_mobile";
            this.lbl_mobile.Size = new System.Drawing.Size(41, 13);
            this.lbl_mobile.TabIndex = 0;
            this.lbl_mobile.Text = "Mobile:";
            // 
            // lbl_phone
            // 
            this.lbl_phone.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbl_phone.AutoSize = true;
            this.lbl_phone.Location = new System.Drawing.Point(7, 236);
            this.lbl_phone.Name = "lbl_phone";
            this.lbl_phone.Size = new System.Drawing.Size(41, 13);
            this.lbl_phone.TabIndex = 0;
            this.lbl_phone.Text = "Phone:";
            // 
            // lbl_postcode
            // 
            this.lbl_postcode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lbl_postcode.AutoSize = true;
            this.lbl_postcode.Location = new System.Drawing.Point(6, 193);
            this.lbl_postcode.Name = "lbl_postcode";
            this.lbl_postcode.Size = new System.Drawing.Size(59, 13);
            this.lbl_postcode.TabIndex = 0;
            this.lbl_postcode.Text = "Post Code:";
            // 
            // lbl_state
            // 
            this.lbl_state.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lbl_state.AutoSize = true;
            this.lbl_state.Location = new System.Drawing.Point(6, 172);
            this.lbl_state.Name = "lbl_state";
            this.lbl_state.Size = new System.Drawing.Size(35, 13);
            this.lbl_state.TabIndex = 0;
            this.lbl_state.Text = "State:";
            // 
            // lbl_suburb
            // 
            this.lbl_suburb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lbl_suburb.AutoSize = true;
            this.lbl_suburb.Location = new System.Drawing.Point(6, 148);
            this.lbl_suburb.Name = "lbl_suburb";
            this.lbl_suburb.Size = new System.Drawing.Size(44, 13);
            this.lbl_suburb.TabIndex = 0;
            this.lbl_suburb.Text = "Suburb:";
            // 
            // lbl_address
            // 
            this.lbl_address.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lbl_address.AutoSize = true;
            this.lbl_address.Location = new System.Drawing.Point(6, 101);
            this.lbl_address.Name = "lbl_address";
            this.lbl_address.Size = new System.Drawing.Size(48, 13);
            this.lbl_address.TabIndex = 0;
            this.lbl_address.Text = "Address:";
            // 
            // lbl_position
            // 
            this.lbl_position.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_position.AutoSize = true;
            this.lbl_position.Location = new System.Drawing.Point(-489, 28);
            this.lbl_position.Name = "lbl_position";
            this.lbl_position.Size = new System.Drawing.Size(47, 13);
            this.lbl_position.TabIndex = 0;
            this.lbl_position.Text = "Position:";
            // 
            // lbl_last_name
            // 
            this.lbl_last_name.AutoSize = true;
            this.lbl_last_name.Location = new System.Drawing.Point(6, 61);
            this.lbl_last_name.Name = "lbl_last_name";
            this.lbl_last_name.Size = new System.Drawing.Size(61, 13);
            this.lbl_last_name.TabIndex = 0;
            this.lbl_last_name.Text = "Last Name:";
            // 
            // lbl_first_name
            // 
            this.lbl_first_name.AutoSize = true;
            this.lbl_first_name.Location = new System.Drawing.Point(6, 38);
            this.lbl_first_name.Name = "lbl_first_name";
            this.lbl_first_name.Size = new System.Drawing.Size(60, 13);
            this.lbl_first_name.TabIndex = 0;
            this.lbl_first_name.Text = "First Name:";
            // 
            // lbl_company_name
            // 
            this.lbl_company_name.AutoSize = true;
            this.lbl_company_name.Location = new System.Drawing.Point(6, 16);
            this.lbl_company_name.Name = "lbl_company_name";
            this.lbl_company_name.Size = new System.Drawing.Size(54, 13);
            this.lbl_company_name.TabIndex = 0;
            this.lbl_company_name.Text = "Company:";
            // 
            // btn_edit_purchase_order
            // 
            this.btn_edit_purchase_order.Location = new System.Drawing.Point(11, 395);
            this.btn_edit_purchase_order.Name = "btn_edit_purchase_order";
            this.btn_edit_purchase_order.Size = new System.Drawing.Size(124, 23);
            this.btn_edit_purchase_order.TabIndex = 13;
            this.btn_edit_purchase_order.Text = "Edit Purchase Order";
            this.btn_edit_purchase_order.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dg_purchase_order_list);
            this.groupBox2.Location = new System.Drawing.Point(181, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(691, 388);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Purchase Order List";
            // 
            // dg_purchase_order_list
            // 
            this.dg_purchase_order_list.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dg_purchase_order_list.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_purchase_order_list.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6});
            this.dg_purchase_order_list.Location = new System.Drawing.Point(7, 15);
            this.dg_purchase_order_list.Name = "dg_purchase_order_list";
            this.dg_purchase_order_list.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.dg_purchase_order_list.Size = new System.Drawing.Size(678, 367);
            this.dg_purchase_order_list.TabIndex = 0;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Number";
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Version";
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Date Created";
            this.Column3.Name = "Column3";
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Description";
            this.Column4.Name = "Column4";
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Price";
            this.Column5.Name = "Column5";
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Status";
            this.Column6.Name = "Column6";
            // 
            // btn_new_purchase_order
            // 
            this.btn_new_purchase_order.Location = new System.Drawing.Point(12, 366);
            this.btn_new_purchase_order.Name = "btn_new_purchase_order";
            this.btn_new_purchase_order.Size = new System.Drawing.Size(123, 23);
            this.btn_new_purchase_order.TabIndex = 12;
            this.btn_new_purchase_order.Text = "New Purchase Order";
            this.btn_new_purchase_order.UseVisualStyleBackColor = true;
            // 
            // btn_close
            // 
            this.btn_close.Location = new System.Drawing.Point(797, 406);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(75, 25);
            this.btn_close.TabIndex = 15;
            this.btn_close.Text = "Close";
            this.btn_close.UseVisualStyleBackColor = true;
            // 
            // Supplier_Profile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 441);
            this.Controls.Add(this.btn_close);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btn_edit_purchase_order);
            this.Controls.Add(this.btn_new_purchase_order);
            this.Controls.Add(this.groupBox1);
            this.Name = "Supplier_Profile";
            this.Text = "Supplier_Profile";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dg_purchase_order_list)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lbl_customer_profile_position_title;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbl_customer_profile_fax;
        private System.Windows.Forms.Label lbl_customer_profile_mobile;
        private System.Windows.Forms.Label lbl_customer_profile_phone;
        private System.Windows.Forms.Label lbl_customer_profile_email;
        private System.Windows.Forms.Label lbl_customer_profile_postcode;
        private System.Windows.Forms.Label lbl_customer_profile_state;
        private System.Windows.Forms.Label lbl_customer_profile_suburb;
        private System.Windows.Forms.Label lbl_customer_profile_address2;
        private System.Windows.Forms.Label lbl_customer_profile_address;
        private System.Windows.Forms.Label lbl_customer_profile_lastname;
        private System.Windows.Forms.Label lbl_customer_profile_firstname;
        private System.Windows.Forms.Label lbl_customer_profile_company;
        private System.Windows.Forms.Label lbl_address2;
        private System.Windows.Forms.Label lbl_email;
        private System.Windows.Forms.Label lbl_fax;
        private System.Windows.Forms.Label lbl_mobile;
        private System.Windows.Forms.Label lbl_phone;
        private System.Windows.Forms.Label lbl_postcode;
        private System.Windows.Forms.Label lbl_state;
        private System.Windows.Forms.Label lbl_suburb;
        private System.Windows.Forms.Label lbl_address;
        private System.Windows.Forms.Label lbl_position;
        private System.Windows.Forms.Label lbl_last_name;
        private System.Windows.Forms.Label lbl_first_name;
        private System.Windows.Forms.Label lbl_company_name;
        private System.Windows.Forms.Button btn_edit_purchase_order;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dg_purchase_order_list;
        private System.Windows.Forms.Button btn_new_purchase_order;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
    }
}