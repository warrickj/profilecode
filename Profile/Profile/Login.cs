﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace ProfileGlassHome
{
    public partial class Login : Form
    {
        private DBQueries dbquery;
        private dbConnect db;

        public Login()
        {

            InitializeComponent();
            txtUsername.Text = "Admin";
            txtPwd.Text = "123";

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_Login_Click(object sender, EventArgs e)
        {
            dbquery = new DBQueries();
            db = new dbConnect();

            if ((string.IsNullOrEmpty(txtUsername.Text) || string.IsNullOrEmpty(txtPwd.Text)))
            {
                txtUsername.Clear();
                txtPwd.Clear();
                this.ActiveControl = txtUsername;
            }

            else
            {
                //string result = dbquery.login_query(txtUsername.Text, txtPwd.Text);
                int result_int = db.Login(txtUsername.Text, txtPwd.Text); ;

                //int result_int;
                //int.TryParse(result, out result_int);
                switch (result_int)
                {
                    case 0:
                        this.Hide();
                        frm_Home pmt = new frm_Home();
                        pmt.Show();
                        break;

                    case 5:
                        txtUsername.Clear();
                        txtPwd.Clear();
                        this.ActiveControl = txtUsername;
                        break;

                }
            }
        }
    }
}
