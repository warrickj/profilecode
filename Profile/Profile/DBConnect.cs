﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace ProfileGlassHome
{
    public class dbConnect
    {
        // Specify a connection string, replace the below connection string with your required connection string
        SqlConnection connection;
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter adp = new SqlDataAdapter();
        SqlTransaction transaction;

        DataTable tb = new DataTable();

        private string final_query;
        //private string var;

        private string connectionString = "Data Source=DESKTOP-EBGED3R;Initial Catalog=Profile;Integrated Security=True"; //Warrick's Laptop
        //private string connectionString = "Data Source=DESKTOP-49C5CC5;Initial Catalog=Profile;Integrated Security=True"; //Chris's Laptop


        public dbConnect()
        {
            Initialize();
        }


        private void Initialize()
        {
            connection = new SqlConnection();
            connection.ConnectionString = connectionString;            
        }


        //open connection to database
        public bool OpenConnection()
        {
            try
            {
                connection.Open();

                return true;
            }
            catch (SqlException ex)
            {
                //When handling errors, you can your application's response based 
                //on the error number.
                //The two most common error numbers when connecting are as follows:
                //0: Cannot connect to server.
                //1045: Invalid user name and/or password.
                switch (ex.Number)
                {
                    case 0:
                        //MessageBox.Show("Cannot connect to server.  Contact administrator");
                        Console.WriteLine("Cannot connect to server.  Contact administrator");
                        break;

                    case 1045:
                        Console.WriteLine("Invalid username/password, please try again");
                        break;
                }
                return false;
            }
        }

        //Close connection
        public bool CloseConnection()
        {
            try
            {
                connection.Close();
                return true;
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        public int Login(string username, string pwd)
        {
            int result = 0;

            if (this.OpenConnection() == true)
            {

                try
                {
                    //Create command
                    int resultStatus = 99;
                    SqlCommand cmd = connection.CreateCommand();
                    //cmd = connection.CreateCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "dbo.uspLogin";
                    cmd.Parameters.Add("@pLoginName", SqlDbType.NChar, 15).Value = username;
                    cmd.Parameters.Add("@pPassword", SqlDbType.NVarChar, 20).Value = pwd;
                    cmd.Parameters.Add("@responseMessage", SqlDbType.Int).Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    resultStatus = (int)cmd.Parameters["@responseMessage"].Value;

                    return resultStatus;

                }
                catch (SqlException ex)
                {
                    //When handling errors, you can use your application's response based 
                    //on the error number.
                    //The two most common error numbers when connecting are as follows:
                    //0: Cannot connect to server.
                    //1045: Invalid user name and/or password.
                    Console.WriteLine("Error code: " + ex.Number + "| Message: " + ex.Message);
                    result = ex.Number;
                }

            }

            this.CloseConnection();

            return result;
        }


        public int Select(string query)
        {
            int result = 0;
            
            //Open connection
            if (this.OpenConnection() == true)
            {

                try
                {
                    //Create command
                    adp.SelectCommand = new SqlCommand(query, connection);
                    result = 0;
                }
                catch (SqlException ex)
                {
                    //When handling errors, you can use your application's response based 
                    //on the error number.
                    //The two most common error numbers when connecting are as follows:
                    //0: Cannot connect to server.
                    //1045: Invalid user name and/or password.
                    Console.WriteLine("Error code: " + ex.Number + "| Message: " + ex.Message);
                    //result = false;
                    /*switch (ex.Number)
                    {
                        case 0:
                            MessageBox.Show("Cannot connect to server.  Contact administrator");
                            break;

                        case 1045:
                            MessageBox.Show("Invalid username/password, please try again");
                            break;
                    }*/
                    result = ex.Number;
                }

                
            }

            this.CloseConnection();

            return result;
        }

        //enum table_id {contact_id,details_id }
        string[] table_id = new string[2]; //table_id[0] - contact_id //table_id[1] - details_id

        public string get_contact_id
        {
            get
            {
                return table_id[0];
            }
            set
            {
                table_id[0] = value;
            }
        }

        public string get_detail_id
        {
            get
            {
                return table_id[1];
            }
            set
            {
                table_id[1] = value;
            }
        }

        //Inserts 3 new records into 3 tables and acquire the contact_id and details_id
        public int Save_New_Customer (List<string> query_list)
        {

            int result = 0;
            int i = 0;
            //Open connection
            if (this.OpenConnection() == true)
            {

                try
                {
                    //The ExecuteNonQuery Method returns the number of row(s) affected by either an INSERT, an UPDATE or a DELETE. 
                    //This method is to be used to perform DML (data manipulation language) statements as stated previously.

                    SqlCommand cmd = new SqlCommand();
                    // Start a local transaction.
                    transaction = connection.BeginTransaction("SampleTransaction");

                    //foreach (DataGridViewRow row in dg_ViewContactContactList.Rows)
                    foreach(String query in query_list)
                    {

                        //Check if the command is a SELECT query, if true then it must be
                        if (query.IndexOf("SELECT", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
                        {

                            //MessageBox.Show("SELECT Query: " + query);
                            adp.SelectCommand = new SqlCommand(query, connection,transaction);

                            //Acquire the values from the sql datadapter
                            get_SqlDataAdapter.Fill(tb);

                            //Assign either contact_id or details_id to the string array
                            table_id[i] = tb.Rows[0][0].ToString();

                            //MessageBox.Show("table_id: " + table_id[i]);

                            //Clear the data table so it can be reused again
                            tb.Clear();
                            
                            //Increment through table_id array
                            i++;    

                        }
                        else
                        {
                            //MessageBox.Show("OTHER Query: " + query);
                            //Execute Insert or Update query
                            cmd.Connection = connection;
                            cmd.Transaction = transaction;

                            //Flag used to determine whether @contact_id or @detail_id is present in the query
                            int flag = 0;

                            //Determine if "@contact_id" exists in the string, if so replace it with a new value
                            if (query.IndexOf("@contact_id", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
                            {
                                
                                final_query = query.Replace("@contact_id","'" + get_contact_id + "'");
                                Console.WriteLine("Query Edit @contact_id: " + final_query + "@contact_id: " +get_contact_id);
                                flag = 1;
                            }
                            

                            //Determine if "@detail_id" exists in the string, if so replace it with a new value
                            if (query.IndexOf("@detail_id", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
                            {
                                //If @contact_id was found in the query, then edit it, if else replace just @detail_id
                                if(flag == 1)
                                {
                                    final_query = final_query.Replace("@detail_id", "'" + get_detail_id + "'");
                                } else
                                {
                                    
                                    final_query = query.Replace("@detail_id", "'" + get_detail_id + "'");
                                }
                                Console.WriteLine("Query Edit @detail_id: " + final_query + "@detail_id: " + get_detail_id);
                                flag = 2;
                            }
                            
                            
                            if(flag == 0)
                            {
                                final_query = query;
                            }
                            

                            cmd.CommandText = final_query;
                            result = cmd.ExecuteNonQuery();

                            //MessageBox.Show("contact_id: " + get_contact_id + "details_id: " + get_details_id);


                            if (result == 0)
                            {
                                //MessageBox.Show("Error in query: " + query);
                                break;
                            }

                        }                      
                    }

                    // Attempt to commit the transaction.
                    transaction.Commit();
                    Console.WriteLine("Both records are written to database.");
                }
                catch (SqlException ex)
                {
                    transaction.Rollback();

                    Console.WriteLine("Error code: " + ex.Number + "  |LineNumber: " + ex.LineNumber + "| Message: " + ex.Message);

                    result = ex.Number;
                }

                //close Connection
                CloseConnection();
            }
            return result;
        }





        //
        public int Edit_Existing_Customer(List<string> query_list)
        {

            int result = 0;
            int i = 0;
            //Open connection
            if (this.OpenConnection() == true)
            {

                try
                {
                    //The ExecuteNonQuery Method returns the number of row(s) affected by either an INSERT, an UPDATE or a DELETE. 
                    //This method is to be used to perform DML (data manipulation language) statements as stated previously.

                    SqlCommand cmd = new SqlCommand();
                    // Start a local transaction.
                    transaction = connection.BeginTransaction("SampleTransaction");

                    //foreach (DataGridViewRow row in dg_ViewContactContactList.Rows)
                    foreach (String query in query_list)
                    {

                        //Check if the command is a SELECT query, if true then it must 
                        if (query.IndexOf("SELECT", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
                        {

                            //MessageBox.Show("SELECT Query: " + query);
                            adp.SelectCommand = new SqlCommand(query, connection, transaction);

                            //Acquire the values from the sql datadapter
                            get_SqlDataAdapter.Fill(tb);

                            //Assign either contact_id or details_id to the string array
                            table_id[i] = tb.Rows[0][0].ToString();

                            //MessageBox.Show("table_id: " + table_id[i]);

                            //Clear the data table so it can be reused again
                            tb.Clear();

                            //Increment through table_id array
                            i++;

                        }
                        else
                        {
                            //MessageBox.Show("OTHER Query: " + query);
                            //Execute Insert or Update query
                            cmd.Connection = connection;
                            cmd.Transaction = transaction;

                            //Flag used to determine whether @contact_id or @detail_id is present in the query
                            int flag = 0;

                            //Determine if "@contact_id" exists in the string, if so replace it with a new value
                            if (query.IndexOf("@contact_id", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
                            {

                                final_query = query.Replace("@contact_id", "'" + get_contact_id + "'");
                                Console.WriteLine("Query Edit @contact_id: " + final_query + "@contact_id: " + get_contact_id);
                                flag = 1;
                            }


                            //Determine if "@detail_id" exists in the string, if so replace it with a new value
                            if (query.IndexOf("@detail_id", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
                            {
                                //If @contact_id was found in the query, then edit it, if else replace just @detail_id
                                if (flag == 1)
                                {
                                    final_query = final_query.Replace("@detail_id", "'" + get_detail_id + "'");
                                }
                                else
                                {

                                    final_query = query.Replace("@detail_id", "'" + get_detail_id + "'");
                                }
                                Console.WriteLine("Query Edit @detail_id: " + final_query + "@detail_id: " + get_detail_id);
                                flag = 2;
                            }


                            if (flag == 0)
                            {
                                final_query = query;
                            }


                            cmd.CommandText = final_query;
                            result = cmd.ExecuteNonQuery();

                            //MessageBox.Show("contact_id: " + get_contact_id + "details_id: " + get_details_id);


                            if (result == 0)
                            {
                                //MessageBox.Show("Error in query: " + query);
                                break;
                            }

                        }


                    }

                    // Attempt to commit the transaction.
                    transaction.Commit();
                    Console.WriteLine("Both records are written to database.");

                }
                catch (SqlException ex)
                {
                    transaction.Rollback();

                    Console.WriteLine("Error code: " + ex.Number + "  |LineNumber: " + ex.LineNumber + "| Message: " + ex.Message);

                    result = ex.Number;

                }

                //close Connection
                CloseConnection();


            }
            return result;



        }






        public int Insert(string query1)
        {
            
            int result = 0;
            //Open connection
            if (this.OpenConnection() == true)
            {

                try
                {
                    //The ExecuteNonQuery Method returns the number of row(s) affected by either an INSERT, an UPDATE or a DELETE. 
                    //This method is to be used to perform DML (data manipulation language) statements as stated previously.
                    
                     SqlCommand cmd = new SqlCommand();
                    // Start a local transaction.
                    transaction = connection.BeginTransaction("SampleTransaction");

                    // Must assign both transaction object and connection
                    // to Command object for a pending local transaction
                    cmd.Connection = connection;
                    cmd.Transaction = transaction;
                    cmd.CommandText = query1;
                    result = cmd.ExecuteNonQuery();
                    MessageBox.Show("Query1: " + result);
                    
                    /*cmd.CommandText = query2;
                    result = cmd.ExecuteNonQuery();
                    MessageBox.Show("Query2: " + result);

                    cmd.CommandText = query3;
                    result = cmd.ExecuteNonQuery();
                    MessageBox.Show("Query3: " + result);
                    */
                    // Attempt to commit the transaction.
                    transaction.Commit();
                    Console.WriteLine("Both records are written to database.");

                    }
                catch (SqlException ex)
                {
                    transaction.Rollback();

                    //When handling errors, you can use your application's response based 
                    //on the error number.
                    //The two most common error numbers when connecting are as follows:
                    //0: Cannot connect to server.
                    //1045: Invalid user name and/or password.
                    Console.WriteLine("Error code: " + ex.Number + "  |LineNumber: "  + ex.LineNumber + "| Message: " + ex.Message);
                    
                    switch (ex.Number)
                    {
                        
                        case 0:
                            Console.WriteLine("Cannot connect to server.  Contact administrator");

                            break;

                        case 1045:
                            Console.WriteLine("Invalid username/password, please try again");
                            break;
                         
                         
                    }
                    result = ex.Number;
                    
                }

                //close Connection
                CloseConnection();

                
            }
            return result;
        }

        public int Update(string query1)
        {

            int result = 0;
            //Open connection
            if (this.OpenConnection() == true)
            {

                try
                {
                    //The ExecuteNonQuery Method returns the number of row(s) affected by either an INSERT, an UPDATE or a DELETE. 
                    //This method is to be used to perform DML (data manipulation language) statements as stated previously.


                    SqlCommand cmd = new SqlCommand();
                    // Start a local transaction.
                    transaction = connection.BeginTransaction("SampleTransaction");

                    // Must assign both transaction object and connection
                    // to Command object for a pending local transaction
                    cmd.Connection = connection;
                    cmd.Transaction = transaction;
                    cmd.CommandText = query1;
                    result = cmd.ExecuteNonQuery();
                    MessageBox.Show("Query1: " + result);

                    // Attempt to commit the transaction.
                    transaction.Commit();
                    Console.WriteLine("Both records are written to database.");

                }
                catch (SqlException ex)
                {
                    transaction.Rollback();

                    //When handling errors, you can use your application's response based 
                    //on the error number.
                    //The two most common error numbers when connecting are as follows:
                    //0: Cannot connect to server.
                    //1045: Invalid user name and/or password.
                    Console.WriteLine("Error code: " + ex.Number + "  |LineNumber: " + ex.LineNumber + "| Message: " + ex.Message);

                    /*switch (ex.Number)
                    {
                        
                        case 0:
                            MessageBox.Show("Cannot connect to server.  Contact administrator");

                            break;

                        case 1045:
                            MessageBox.Show("Invalid username/password, please try again");
                            break;
                         
                         
                    }*/
                    result = ex.Number;

                }

                //close Connection
                CloseConnection();


            }
            return result;
        }


        public SqlDataAdapter get_SqlDataAdapter
        {
            get
            {
                return adp;
            }
            
        }
        


    }
}
